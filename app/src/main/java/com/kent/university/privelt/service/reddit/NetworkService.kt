package com.kent.university.privelt.service.reddit

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {

    @GET("v1/me")
    fun requestProfile(
        @Header("Authorization") authorize: String
    ): Call<Profile>
}

class RetrofitRedditInstance {
    companion object {
        private const val OAUTH_API_URL: String = "https://auth.reddit.com/api/"

        fun getAuthRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(OAUTH_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class Subreddit(val user_is_contributor: Boolean,
                     val previous_names: List<String>,
                     val over_18: Boolean,
                     val restrict_commenting: Boolean,
                     val user_is_moderator: Boolean,
                     val accept_followers: Boolean,
                     val user_is_subscriber: String)

data class Features(val mod_service_mute_writes: Boolean,
                     val promoted_trend_blanks: Boolean,
                     val show_amp_link: Boolean,
                     val is_email_permission_required: Boolean,
                     val mod_awards: Boolean,
                     val expensive_coins_package: Boolean,
                     val awards_on_streams: Boolean,
                     val mweb_xpromo_modal_listing_click_daily_dismissible_ios: Boolean,
                     val chat_subreddit: Boolean,
                     val cookie_consent_banner: Boolean,
                     val modlog_copyright_removal: Boolean,
                     val show_nps_survey: Boolean,
                     val do_not_track: Boolean,
                     val mod_service_mute_reads: Boolean,
                     val chat_user_settings: Boolean,
                     val use_pref_account_deployment: Boolean,
                     val mweb_xpromo_interstitial_comments_ios: Boolean,
                     val mweb_xpromo_modal_listing_click_daily_dismissible_android: Boolean,
                     val premium_subscriptions_table: Boolean,
                     val mweb_xpromo_interstitial_comments_android: Boolean,
                     val crowd_control_for_post: Boolean,
                     val noreferrer_to_noopener: Boolean,
                     val chat_group_rollout: Boolean,
                     val resized_styles_images: Boolean,
                     val spez_modal: Boolean)

data class Profile(val is_employee: Boolean,
                   val seen_layout_switch: Boolean,
                   val has_visited_new_profile: Boolean,
                   val pref_no_profanity: Boolean,
                   val has_external_account: Boolean,
                   val pref_geopopular: String,
                   val seen_redesign_modal: Boolean,
                   val pref_show_trending: Boolean,
                   val subreddit: Subreddit,
                   val pref_show_presence: Boolean,
                   val snoovatar_img: String,
                   val has_gold_subscription: Boolean,
                   val is_sponsor: Boolean,
                   val num_friends: Int,
                   val features: Features,
                   val can_edit_name: Boolean,
                   val verified: Boolean,
                   val pref_autoplay: Boolean,
                   val coins: Int,
                   val has_paypal_subscription: Boolean,
                   val has_subscribed_to_premium: Boolean,
                   val id: String,
                   val has_stripe_subscription: Boolean,
                   val oauth_client_id: String,
                   val can_create_subreddit: Boolean,
                   val over_18: Boolean,
                   val is_gold: Boolean,
                   val is_mod: Boolean,
                   val awarder_karma: Int,
                   val has_verified_email: Boolean,
                   val is_suspended: Boolean,
                   val pref_video_autoplay: Boolean,
                   val has_android_subscription: Boolean,
                   val in_redesign_beta: Boolean,
                   val icon_img: String,
                   val has_mod_mail: Boolean,
                   val pref_nightmode: Boolean,
                   val awardee_karma: Int,
                   val hide_from_robots: String,
                   val password_set: Boolean,
                   val link_karma: Int,
                   val force_password_reset: Boolean,
                   val total_karma: Int,
                   val seen_give_award_tooltip: Boolean,
                   val inbox_count: Int,
                   val seen_premium_adblock_modal: Boolean,
                   val pref_top_karma_subreddits: Boolean,
                   val has_mail: Boolean,
                   val pref_show_snoovatar: Boolean,
                   val name: String,
                   val pref_clickgadget: Int,
                   val created: Float,
                   val gold_creddits: Int,
                   val created_utc: Float,
                   val has_ios_subscription: Boolean,
                   val pref_show_twitter: Boolean,
                   val in_beta: Boolean,
                   val comment_karma: Int,
                   val accept_followers: Boolean,
                   val has_subscribed: Boolean,
                   val seen_subreddit_chat_ftux: Boolean) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Reddit $title",
                type = DataTypeList.PROFILE.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date)
        }
        return userDataList
    }
}