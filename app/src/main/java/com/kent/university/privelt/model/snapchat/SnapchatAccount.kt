package com.kent.university.privelt.model.snapchat

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class SnapchatAccountActivity(
    @SerializedName("Display Name Change")
    val displayNameChange: List<SnapchatDisplayNameChange>?,
    @SerializedName("Email Change")
    val emailChange: List<SnapchatEmailChange>?,
    @SerializedName("Mobile Number Change")
    val mobileNumberChange: List<SnapchatMobileNumberChange>?,
    @SerializedName("Password Change")
    val passwordChange: List<SnapchatPasswordChange>?,
    @SerializedName("Two-Factor Authentication")
    val twoFactorAuthentication: List<SnapchatTwoFactorAuthentication>?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Account History",
            type = DataTypeList.PROFILE.title,
            subtype = "Snapchat Account History",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val displayNameChange: List<UserData> = displayNameChange?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) } ?: emptyList()
        val emailChange: List<UserData> = emailChange?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) }  ?: emptyList()
        val mobileNumberChange: List<UserData> = mobileNumberChange?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) }  ?: emptyList()
        val passwordChange: List<UserData> = passwordChange?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) }  ?: emptyList()
        val twoFactorAuthentication: List<UserData> = twoFactorAuthentication?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) }  ?: emptyList()
        allData.addAll(displayNameChange)
        allData.addAll(emailChange)
        allData.addAll(mobileNumberChange)
        allData.addAll(passwordChange)
        allData.addAll(twoFactorAuthentication)

        return allData.toList()
    }
}

data class SnapchatDisplayNameChange(
    @SerializedName("Date")
    val date: String?,
    @SerializedName("Display Name")
    val displayName: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val dateUserData = UserData(title = "Snapchat Display Name Change Date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Date changed: ${this.date ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(dateUserData)

        val displayNameUserData = UserData(title = "Snapchat Display Name Changed",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = displayName ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(displayNameUserData)

        return allData
    }
}

data class SnapchatEmailChange(
    @SerializedName("Date")
    val date: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val dateUserData = UserData(title = "Snapchat Email Change Date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Date changed: ${this.date ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(dateUserData)
        return allData
    }
}

data class SnapchatMobileNumberChange(
    @SerializedName("Date")
    val date: String?,
    @SerializedName("Mobile Number")
    val mobileNumber: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val dateUserData = UserData(title = "Snapchat Mobile Number Change Date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Date changed: ${this.date ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(dateUserData)

        val mobileNumberUserData = UserData(title = "Snapchat Mobile Number Changed",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.MOBILE.value,
            value = mobileNumber ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(mobileNumberUserData)
        return allData
    }
}

data class SnapchatPasswordChange(
    @SerializedName("Date")
    val date: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val dateUserData = UserData(title = "Snapchat Password Change Date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Date changed: ${this.date ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(dateUserData)
        return allData
    }
}

data class SnapchatTwoFactorAuthentication(
    @SerializedName("Date")
    val date: String?,
    @SerializedName("Event")
    val event: String?
)  {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val dateUserData = UserData(title = "Snapchat Event Change Date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Date changed: ${this.date ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(dateUserData)

        val eventUserData = UserData(title = "Snapchat Event Changed",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Number changed to: ${this.event ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(eventUserData)
        return allData
    }
}

data class SnapchatBasicInformation(
    @SerializedName("Username")
    val username: String?,
    @SerializedName("Name")
    val name: String?,
    @SerializedName("Creation Date")
    val creationDate: String?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Account Info",
            type = DataTypeList.PROFILE.title,
            subtype = "Snapchat Account Info",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val usernameUserData = UserData(title = "Snapchat Basic Account Info Username",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.USERNAME.value,
            value = username ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(usernameUserData)

        val nameUserData = UserData(title = "Snapchat Basic Account Info name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = name ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(nameUserData)

        val creationDateUserData = UserData(title = "Snapchat Basic Account Info creation date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Creation date: ${creationDate ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(creationDateUserData)
        return allData
    }
}

data class SnapchatDeviceInformation(
    @SerializedName("Make")
    val make: String?,
    @SerializedName("Model ID")
    val modelId: String?,
    @SerializedName("Model Name")
    val modelName: String?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Account Device",
            type = DataTypeList.DEVICE.title,
            subtype = "Snapchat Account Device",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val makeUserData = UserData(title = "Snapchat Account Device Make",
            type = DataTypeList.DEVICE.title,
            subtype = DataSubTypeListDevice.DEVICE_TYPE.value,
            value = make ?: "",
            serviceId = serviceId,
            date = date)
        allData.add(makeUserData)

        val modelUserData = UserData(title = "Snapchat Account Device Model",
            type = DataTypeList.DEVICE.title,
            subtype = DataSubTypeListDevice.DEVICE_TYPE.value,
            value = modelId ?: "",
            serviceId = serviceId,
            date = date)
        allData.add(modelUserData)

        val modelNameUserData = UserData(title = "Snapchat Account Device Model Name",
            type = DataTypeList.DEVICE.title,
            subtype = DataSubTypeListDevice.DEVICE_TYPE.value,
            value = modelName ?: "",
            serviceId = serviceId,
            date = date)
        allData.add(modelNameUserData)
        return allData
    }
}

data class SnapchatData(
    @SerializedName("Basic Information")
    val basicInformation: SnapchatBasicInformation?,
    @SerializedName("Device Information")
    val deviceInformation: SnapchatDeviceInformation?,
    @SerializedName("Login History")
    val loginHistory: List<SnapchatLoginHistory>?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val basicInformation: List<UserData> = basicInformation?.toUserData(serviceId, date) ?: emptyList()
        val deviceInformation: List<UserData> = deviceInformation?.toUserData(serviceId, date) ?: emptyList()
        val loginHistory: List<UserData> = loginHistory?.flatMap { it.toUserData(serviceId, date) } ?: emptyList()

        val userDataList: MutableList<List<UserData>> = mutableListOf<List<UserData>>()
        userDataList.add(basicInformation)
        userDataList.add(deviceInformation)
        userDataList.add(loginHistory)

        return userDataList.flatten().toList()
    }
}

data class SnapchatLoginHistory(
    @SerializedName("IP")
    val ip: String?,
    @SerializedName("Country")
    val country: String?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val initialLocationUserData = UserData(title = "Snapchat Login History",
            type = DataTypeList.LOCATION.title,
            subtype = "Snapchat Login History",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialLocationUserData)

        val initialDeviceUserData = UserData(title = "Snapchat Login Device",
            type = DataTypeList.DEVICE.title,
            subtype = "Snapchat Login Device",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialDeviceUserData)

        val countryUserData = UserData(title = "Snapchat Login History",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(countryUserData)

        val ipUserData = UserData(title = "Snapchat Login History",
            type = DataTypeList.DEVICE.title,
            subtype = DataSubTypeListDevice.IP.value,
            value = ip ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(ipUserData)
        return allData
    }
}


data class SnapchatProfile(
    @SerializedName("Profile")
    val profile: Profile? = null
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        return profile?.toUserData(serviceId, date) ?: emptyList()
    }
}

data class Profile(
    @SerializedName("Created")
    val created: String? = null,
    @SerializedName("Profile Title")
    val profileTitle: String? = null,
    @SerializedName("Location")
    val location: String? = null,
    @SerializedName("Profile Website")
    val profileWebsite: String? = null
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Snapchat Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val createdUserData = UserData(title = "Snapchat Profile Date Created",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Created date: ${created ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(createdUserData)

        val profileTitleUserData = UserData(title = "Snapchat Profile Title",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.TITLE.value,
            value = profileTitle ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(profileTitleUserData)

        val websiteUserData = UserData(title = "Snapchat Profile Website",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.WEBSITE.value,
            value = profileWebsite ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(websiteUserData)

        val initialLocationUserData = UserData(title = "Snapchat Profile Location",
            type = DataTypeList.LOCATION.title,
            subtype = "Snapchat Profile Location",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialLocationUserData)

        val initialLocationDataUserData = UserData(title = "Snapchat Profile Location Data",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = location ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(initialLocationDataUserData)
        return allData
    }
}

data class SnapchatUserProfile(
    @SerializedName("App Profile")
    val appProfile: SnapchatAppProfile?,
    @SerializedName("Demographics")
    val demographics: SnapchatDemographics?,
    @SerializedName("Interest Categories")
    val interestCategories: List<String>?,
    @SerializedName("Mobile Ad Id")
    val mobileAdId: String?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat User Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Snapchat User Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val appProfile: List<UserData> = appProfile?.toUserData(serviceId, date, initialUserData.id) ?: emptyList()
        val demographics: List<UserData> = demographics?.toUserData(serviceId, date, initialUserData.id)  ?: emptyList()
        allData.addAll(appProfile)
        allData.addAll(demographics)

        if (interestCategories != null) {
            val interestCategories = interestCategories.map {
                UserData(title = "Snapchat Interest Categories",
                    type = DataTypeList.PROFILE.title,
                    subtype = it,
                    value = "",
                    serviceId = serviceId,
                    date = date,
                    parentId = initialUserData.id)
            }
            allData.addAll(interestCategories)
        }
        if (mobileAdId != null) {
            val mobileAdId = UserData(title = "Snapchat Mobile Ad ID",
                type = DataTypeList.PROFILE.title,
                subtype = mobileAdId,
                value = "",
                serviceId = serviceId,
                date = date,
                parentId = initialUserData.id)
            val tempList: MutableList<UserData> = mutableListOf<UserData>()
            tempList.add(mobileAdId)
            allData.addAll(tempList)
        }
        return allData
    }
}

data class SnapchatAppProfile(
    @SerializedName("Country")
    val country: String? = null,
    @SerializedName("Creation Time")
    val creationTime: String? = null,
    @SerializedName("Account Creation Country")
    val accountCreationCountry: String? = null,
    @SerializedName("Platform Version")
    val platformVersion: String? = null
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()

        val initialLocationUserData = UserData(title = "Snapchat Profile Location",
            type = DataTypeList.LOCATION.title,
            subtype = "Snapchat Profile Location",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialLocationUserData)

        val initialLocationDataUserData = UserData(title = "Snapchat Profile Location Data",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(initialLocationDataUserData)

        val createdUserData = UserData(title = "Snapchat Profile Creation Time",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Created date: ${creationTime ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(createdUserData)

        val accountCreatedUserData = UserData(title = "Snapchat Profile Account Creation Time",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Account Created date: ${accountCreationCountry ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(accountCreatedUserData)

        val initialAccountLocationDataUserData = UserData(title = "Snapchat Account Location Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = accountCreationCountry ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(initialAccountLocationDataUserData)

        return allData
    }
}

data class SnapchatDemographics(
    @SerializedName("Cohort Age")
    val cohortAge: String? = null,
    @SerializedName("Derived Ad Demographic")
    val derivedAdDemographic: String? = null
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()

        val initialLocationUserData = UserData(title = "Snapchat Profile Demographics Cohort Age",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Cohort age: ${cohortAge ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(initialLocationUserData)

        val adDemographicUserData = UserData(title = "Snapchat Profile Demographics derived ad demographic",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Derived ad demographic: ${derivedAdDemographic ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(adDemographicUserData)
        return  allData
    }
}
