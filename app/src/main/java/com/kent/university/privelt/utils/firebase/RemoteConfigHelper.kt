package com.kent.university.privelt.utils.firebase

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.utils.PriVELTLog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.nio.charset.Charset

object RemoteConfigHelper {

    private var currentActivityScreen: String = ""
    private var currentFragmentScreen: String = ""
    private var helpContent: HelpContent? = null
    private const val SERVICES_OPTIONS = "service_options"

    fun initRemoteConfig() {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(1800) // 30 min
            .build()

        //Set config with max fetch between 2 fetch
        remoteConfig.setConfigSettingsAsync(configSettings)
        FirebaseRemoteConfig.getInstance().fetchAndActivate()
    }

    fun get(): FirebaseRemoteConfig {
        return FirebaseRemoteConfig.getInstance()
    }

    fun init() = CoroutineScope(Dispatchers.IO).launch {
            helpContent = getHelpContentObject()
        }

    fun getHelpContent(): String {
        return helpContent?.getContentForScreen(if (currentActivityScreen == "2.0.0" || currentActivityScreen == "1.0.1") currentFragmentScreen else currentActivityScreen) ?: ""
    }

    fun getHelpType(): Int {
        return helpContent?.getHelpTypeForScreen(if (currentActivityScreen == "2.0.0" || currentActivityScreen == "1.0.1") currentFragmentScreen else currentActivityScreen) ?: 1
    }

    fun nextScreenActivity(screen: String, label: String) {
        currentActivityScreen = screen
        PriVELTLog.SCREEN_NAV("New screen opened $screen ($label)")
    }

    fun nextScreenFragment(screen: String, label: String) {
        currentFragmentScreen = screen
        PriVELTLog.SCREEN_NAV("New screen opened $screen ($label)")
    }

    private fun getHelpContentObject(): HelpContent {
        val ins: InputStream = PriVELTApplication.instance.applicationContext.resources.openRawResource(R.raw.app_help)
        val text = ins.bufferedReader(Charset.defaultCharset()).use { it.readText() }
        return Gson().fromJson(text, HelpContent::class.java)
    }

    fun getServicesOptions(): ServicesOptions? {
        val config = get().getString(SERVICES_OPTIONS)
        if (config == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_STRING) return null
        return Gson().fromJson(config, ServicesOptions::class.java)
    }
}