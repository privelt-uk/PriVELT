package com.kent.university.privelt.service.youtube

import android.content.Context
import android.util.Base64
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class YoutubeService: Service("Youtube", false, "", "", "") {
    lateinit var APIServiceListener: APIServiceListener
    private val redirectUrl = "com.kent.university.privelt:/gcal"
    val clientId = "715125507310-25rc9iqe1afnnjvh47cigoikc8iqt0a6.apps.googleusercontent.com"
    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(token: String, context: Context) = CoroutineScope(
        Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }
       systemTime = System.currentTimeMillis()

       val id = insertOrGetService(context)
       getPlaylistsList(token, id, context)
    }

    fun connection(context: Context, webView: WebView)= CoroutineScope(
        Dispatchers.IO).launch {
        val token = getServiceRefreshToken(context)
        if (token != null && token.isNotEmpty()) {
            CoroutineScope(Dispatchers.Main).launch {
                APIServiceListener.loading()
            }
            val retGcalendar = RetrofitYoutubeInstance.getAuthRetrofitInstance()
            retGcalendar.refreshToken( grantType = "refresh_token", clientId = clientId, refreshToken = token)
                .enqueue(object: Callback<AuthRequestTokenResponse> {
                    override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                        val message = t.message.toString()
                        Log.e("youtube", t.message.toString())
                    }
                    override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                        if (response.code() == 200) {

                            val token = response.body()?.access_token!!
                            val bearerToken = "Bearer $token"

                            insertServiceAndFetchData(bearerToken, context)
                        } else {
                            Log.d("youtube","Token exchange failed! from refresh")
                            Log.d("youtube",response.body().toString())
                        }
                    }
                })
        } else {
            CoroutineScope(Dispatchers.Main).launch{
                openBrowser(webView, context)
            }
        }
    }

    private fun openBrowser(webView: WebView, context: Context) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        val codeVerifier = "01234567890123456789012345678901234567890123456789"//getRandomString(64)
        Log.d("OAuth code verifier", codeVerifier)
        val base64 = Base64.encodeToString(codeVerifier.encodeToByteArray(), Base64.NO_WRAP).replace("==", "")
        val codeChallenge = "-4cf-Mzo_qg9-uq0F4QwWhRh4AjcAqNx7SbYVsdmyQM"//encodeHeader(base64)
        Log.d("OAuth code challenge", codeChallenge)
        val redirectScheme = "com.kent.university.privelt"
        val scope = "https://www.googleapis.com/auth/youtube"
        val url = "https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=$clientId&state=state&scope=$scope&redirect_uri=$redirectUrl&code_challenge=$codeChallenge&code_challenge_method=S256"
        webView.settings.userAgentString = "Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.69 Mobile Safari/537.36"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectScheme)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.getQueryParameter("code")?.let { code ->
                            // Got code!
                            Log.d("OAuth code", code)
                            getToken(code, codeVerifier, context)
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            val exception = ApiResponseException(
                                "Youtube auth code not received.",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                            APIServiceListener.error("Authorization code not received :(")
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    fun getToken(code: String, codeVerifier: String, context: Context) {
        val retGcalendar = RetrofitYoutubeInstance.getAuthRetrofitInstance()
        retGcalendar.requestToken( "authorization_code", clientId, code, redirectUrl, codeVerifier)
            .enqueue(object: Callback<AuthRequestTokenResponse> {
                override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                    Log.e("youtube", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                    if (response.code() == 200) {
                        Log.d("youtube","Generated token!")
                        response.body()?.let { responseBody ->
                            Log.d("Access token", responseBody.access_token)
                            val token = responseBody.access_token
                            val bearerToken = "Bearer $token"
                            this@YoutubeService.password = responseBody.refresh_token
                            insertServiceAndFetchData(bearerToken, context)
                        }
                    } else {
                        Log.d("youtube","Token exchange failed!")
                        val exception = ApiResponseException(
                            "Token exchange failed",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        Log.d("youtube",response.body().toString())
                        APIServiceListener.error("Token exchange failed!")
                    }
                }
            })
    }

    private fun getPlaylistsList(token: String, serviceId: Long, context: Context) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val retGoogleCalendar = RetrofitYoutubeInstance.getApiRetrofitInstance()
        retGoogleCalendar.requestPlaylists(token,
            "snippet,contentDetails,id,localizations,player,status",
             true, "50")
            .enqueue(object: Callback<YoutubePlaylistList> {
                override fun onFailure(call: Call<YoutubePlaylistList>, t: Throwable) {
                    Log.e("youtube", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<YoutubePlaylistList>, response: Response<YoutubePlaylistList>) {
                    if (response.code() == 200) {
                        Log.d("youtube","Fetched youtube playlists list!")
                        response.body()?.let { it ->

                            val initialUserData = UserData(title = "Youtube Playlists",
                                type = DataTypeList.PROFILE.title,
                                subtype = "Youtube Playlists",
                                value = "",
                                serviceId = serviceId,
                                date = systemTime)
                            val allData = mutableListOf<UserData>()
                            allData.add(initialUserData)
                            val test = response.body()?.items?.flatMap{ it.toUserData(serviceId, systemTime, initialUserData.id) } ?: emptyList()
                            allData.addAll(test)
                            CoroutineScope(Dispatchers.IO).launch {
                                allData.forEach {
                                    if (it.isValid())
                                        userDataRepository?.insertUserData(it)
                                }
                            }
                            getSubscriptionList(token, serviceId, context)
                        }
                    } else if (response.code() == 404) {
                        Log.d("youtube","No playlists found!")
                        APIServiceListener.success()
                    } else {
                        Log.d("youtube", response.message())
                        val exception = ApiResponseException(
                            "Fetching youtube playlists list failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        Log.d("youtube","Fetching youtube playlists list failed!")
                        APIServiceListener.error("Fetching youtube playlists list failed!")
                    }
                }
            })
    }

    private fun getSubscriptionList(token: String, serviceId: Long, context: Context) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val retGoogleCalendar = RetrofitYoutubeInstance.getApiRetrofitInstance()
        retGoogleCalendar.requestSubscriptions(token, "snippet,contentDetails,id",
            true, "50")
            .enqueue(object: Callback<YoutubeSubscriptionList> {
                override fun onFailure(call: Call<YoutubeSubscriptionList>, t: Throwable) {
                    Log.e("youtube", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<YoutubeSubscriptionList>, response: Response<YoutubeSubscriptionList>) {
                    if (response.code() == 200) {
                        Log.d("youtube","Fetched youtube subscriptions list!")
                        response.body()?.let { it ->

                            val initialUserData = UserData(title = "Youtube Subscriptions",
                                type = DataTypeList.PROFILE.title,
                                subtype = "Youtube Subscriptions",
                                value = "",
                                serviceId = serviceId,
                                date = systemTime)
                            val allData = mutableListOf<UserData>()
                            allData.add(initialUserData)
                            val test = response.body()?.items?.flatMap{ it.toUserData(serviceId, systemTime, initialUserData.id) } ?: emptyList()
                            allData.addAll(test)
                            CoroutineScope(Dispatchers.IO).launch {
                                allData.forEach {
                                    if (it.isValid())
                                        userDataRepository?.insertUserData(it)
                                }
                            }
                            APIServiceListener.success()
                        }
                    }
                    else{
                        Log.d("youtube", response.message())
                        val exception = ApiResponseException(
                            "Fetching youtube subscriptions list failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        Log.d("youtube","Fetching youtube subscriptions list failed!")
                        APIServiceListener.error("Fetching youtube subscriptions list failed!")
                    }
                }
            })
    }
}