/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.card

import android.content.Intent
import android.graphics.Color
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.databinding.CellServiceBinding
import com.kent.university.privelt.events.ChangeWatchListStatusEvent
import com.kent.university.privelt.events.ClickedDateExtractionDateEvent
import com.kent.university.privelt.events.ClickedRefreshEvent
import com.kent.university.privelt.events.DetailedCardEvent
import com.kent.university.privelt.events.RadarClickedEvent
import com.kent.university.privelt.events.UpdateCredentialsEvent
import com.kent.university.privelt.model.Card
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.ServiceController
import com.kent.university.privelt.ui.risk_value.BarActivity
import com.kent.university.privelt.ui.risk_value.RiskValueActivity
import com.kent.university.privelt.utils.capitalize
import com.kent.university.privelt.utils.sentence.SentenceAdapter
import net.neferett.webviewsextractor.model.UserDataTypes
import org.greenrobot.eventbus.EventBus
import java.util.*

internal class CardViewHolder(private val binding: CellServiceBinding) :
    RecyclerView.ViewHolder(binding.root) {

    var card: Card? = null

    fun bind(card: Card) {
        this.card = card

        binding.serviceType.isVisible = card.isService

        binding.refresh.isVisible = card.isService
        binding.refresh.setOnClickListener {
            EventBus.getDefault().post(ClickedRefreshEvent(card.title))
        }
        binding.extraction.isVisible = card.isService
        binding.extraction.setOnClickListener {
            EventBus.getDefault().post(ClickedDateExtractionDateEvent(card))
        }
        binding.title.text = capitalize(card.title)
        if (card.isService) {
            val priVELTApplication = binding.title.context.applicationContext as PriVELTApplication
            val res = priVELTApplication.serviceHelper!!.getResIdWithName(card.title)
            res?.let {
                binding.imageService.setImageResource(res)
            }
            binding.radar.isVisible = false
            binding.settings.isVisible = card.title.contains("(R)") == true
            binding.settings.setOnClickListener {
                EventBus.getDefault().post(UpdateCredentialsEvent(card.title))
            }

            val serviceItem = ServiceController.getServiceItem(card.title)
            when {
                ServiceController.isAPIService(serviceItem) -> {
                    binding.serviceType.text = "API"
                    binding.serviceType.setBackgroundResource(R.drawable.api_circle)
                }
                ServiceController.isSARSService(serviceItem) -> {
                    binding.serviceType.text = "SAR"
                    binding.serviceType.setBackgroundResource(R.drawable.sar_circle)
                }
            }
        } else {
            val userDataType = UserDataTypes.getUserDataType(card.title.uppercase(Locale.ROOT))
            if (userDataType != null)
                binding.imageService.setImageResource(userDataType.res)
            else
                binding.imageService.setImageResource(DataTypeList.getResourceFromTitle(card.title))
            binding.settings.visibility = View.GONE
            binding.radar.isVisible = true
            binding.radar.setOnClickListener {
                EventBus.getDefault().post(RadarClickedEvent(card))
            }
        }

        binding.watchIcon.setOnClickListener {
            EventBus.getDefault().post(ChangeWatchListStatusEvent(card.title))
            card.isWatched = !card.isWatched
            binding.watchIcon.setColorFilter(
                if (card.isWatched) ContextCompat.getColor(
                    itemView.context,
                    R.color.colorAccent
                ) else ContextCompat.getColor(itemView.context, android.R.color.black)
            )

            if (card.isWatched)
                Toast.makeText(
                    itemView.context,
                    itemView.context.getString(R.string.added_watch_list),
                    Toast.LENGTH_LONG
                ).show()
            else
                Toast.makeText(
                    itemView.context,
                    itemView.context.getString(R.string.removed_watch_list),
                    Toast.LENGTH_LONG
                ).show()
        }

        binding.imageService.setOnClickListener {
            EventBus.getDefault().post(DetailedCardEvent(card))
        }
        binding.watchIcon.setColorFilter(
            if (card.isWatched) ContextCompat.getColor(
                itemView.context,
                R.color.colorAccent
            ) else ContextCompat.getColor(itemView.context, android.R.color.black)
        )

        var total = 0
        for (item in card.metrics)
            if (item.date == card.dateSelected)
                total += item.number
        if (card.metrics.size != 0) {
            //TODO: 200 HARDCODED (MAX DATA)
            binding.chart.setOnClickListener {
                val intent = Intent(binding.chart.context, BarActivity::class.java)
                if (card.isService) intent.putExtra(
                    RiskValueActivity.PARAM_SERVICE,
                    card.title
                ) else intent.putExtra(RiskValueActivity.PARAM_DATA, card.title)
                binding.chart.context.startActivity(intent)
            }
            binding.chart.visibility = View.VISIBLE
            binding.serviceValue.visibility = View.VISIBLE
            binding.serviceValue.text = total.toString()
        } else {
            binding.chart.visibility = View.GONE
            binding.chart.setOnClickListener(null)
            binding.serviceValue.visibility = View.GONE
        }
        var riskValue = total
        if (riskValue > 100) riskValue = 100
        when {
            riskValue == 0 -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "None"
                )
                binding.privacyValue.setTextColor(Color.GRAY)
            }
            riskValue < 20 -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "Low"
                )
                binding.privacyValue.setTextColor(Color.GREEN)
            }
            riskValue < 60 -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "Medium"
                )
                binding.privacyValue.setTextColor(Color.parseColor("#FFBF00"))
            }
            else -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "High"
                )
                binding.privacyValue.setTextColor(Color.RED)
            }
        }
    }

}
