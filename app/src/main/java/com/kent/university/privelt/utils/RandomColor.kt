package com.kent.university.privelt.utils

import android.graphics.Color

var index = 0

val colors = arrayOf(
        Color.parseColor("#FF007F"),
        Color.parseColor("#FF7F00"),
        Color.parseColor("#FFFF00"),
        Color.parseColor("#7FFF00"),
        Color.parseColor("#00FFFF"),
        Color.parseColor("#007FFF"),
        Color.parseColor("#7F00FF"),
        Color.parseColor("#FF00FF")
)

fun getRandomColor(): Int {
    return colors[index++ % colors.size]
}