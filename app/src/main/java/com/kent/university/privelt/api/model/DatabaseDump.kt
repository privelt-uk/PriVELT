package com.kent.university.privelt.api.model

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.CurrentUser
import com.kent.university.privelt.model.HistoryPermission
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.UserEvent

data class ServiceAnonymized(val id: Long, val name: String, val createdAt: Long, @SerializedName("user_data") var userData: List<UserDataAnonymized>? = emptyList())
data class UserDataAnonymized(val id: String, val dataDomain: String, val dataSubject: String, val date: Long, val parentId: String?, val serviceId: Long)

data class DatabaseDump(
    @SerializedName("services")
    val service: List<Service>?,
    @SerializedName("user_data")
    val userData: List<UserData>?,
    @SerializedName("history_permission")
    val historyPermissionList: List<HistoryPermission>?,
    @SerializedName("user_event")
    val userEvent: List<UserEvent>? = null
)

data class DatabaseDumpAnonymized(
    @SerializedName("services")
    val service: List<ServiceAnonymized>?,
    @SerializedName("history_permission")
    val historyPermissionList: List<HistoryPermission>?,
    @SerializedName("user_event")
    val userEvent: List<UserEvent>? = null
)

fun UserData.toUserDataAnonymized(): UserDataAnonymized {
    return UserDataAnonymized(id, type, subtype, date, parentId, serviceId)
}

fun Service.toServiceAnonymized(): ServiceAnonymized {
    return ServiceAnonymized(id, name, createdAt)
}
