/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.sensors

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.R
import com.kent.university.privelt.databinding.CellApplicationBinding
import com.kent.university.privelt.events.LaunchDetailedSensorEvent
import com.kent.university.privelt.model.Sensor
import com.kent.university.privelt.ui.dashboard.sensors.chart.sensor.SensorChartFragment
import com.kent.university.privelt.ui.dashboard.sensors.chart.sensor.SensorPagerActivity
import com.kent.university.privelt.utils.privacy_scoring.PermissionScoring.computeScoreForPermission
import com.kent.university.privelt.utils.sensors.SensorHelper.getNumberOfApplicationInstalled
import com.kent.university.privelt.utils.sentence.SentenceAdapter
import org.greenrobot.eventbus.EventBus

internal class SensorViewHolder(private val binding: CellApplicationBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(sensor: Sensor) {
        binding.imageSensor.setImageResource(sensor.resId)
        binding.sensorValue.text = sensor.getApplications().size.toString()
        binding.root.setOnClickListener {
            EventBus.getDefault().post(LaunchDetailedSensorEvent(sensor))
        }
        binding.title.text = sensor.title
        binding.chart.setOnClickListener {
            val intent = Intent(binding.chart.context, SensorPagerActivity::class.java)
            binding.chart.context.startActivity(intent)
        }

        var riskValue = computeScoreForPermission(
            sensor,
            getNumberOfApplicationInstalled(itemView.context)
        ).toInt()
        riskValue = riskValue * 100 / getNumberOfApplicationInstalled(itemView.context)
        when {
            riskValue == 0 -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "0%"
                )
                binding.privacyValue.setTextColor(Color.GRAY)
            }
            riskValue < 20 -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "$riskValue%"
                )
                binding.privacyValue.setTextColor(Color.GREEN)
            }
            riskValue < 60 -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "$riskValue%"
                )
                binding.privacyValue.setTextColor(Color.parseColor("#FFBF00"))
            }
            else -> {
                binding.privacyValue.text = SentenceAdapter.adapt(
                    itemView.context.resources.getString(R.string.global_privacy_value),
                    "$riskValue%"
                )
                binding.privacyValue.setTextColor(Color.RED)
            }
        }

        if (sensor.isSensor) {
            binding.sensorStatusImage.visibility = View.VISIBLE
            binding.sensorStatusName.visibility = View.VISIBLE
            val isEnabled = sensor.isEnabled(itemView.context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                binding.sensorStatusImage.backgroundTintList =
                    ColorStateList.valueOf(if (isEnabled) Color.RED else Color.GREEN)
            }
            binding.sensorStatusName.text = if (isEnabled) "Enabled" else "Disabled"
            binding.sensorStatusName.setTextColor(ColorStateList.valueOf(if (isEnabled) Color.RED else Color.GREEN))
        } else {
            binding.sensorStatusImage.visibility = View.GONE
            binding.sensorStatusName.visibility = View.GONE
        }
    }

}
