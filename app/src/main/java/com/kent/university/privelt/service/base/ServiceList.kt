package com.kent.university.privelt.service.base

import com.kent.university.privelt.model.Service

interface ServiceList {

    val service: Service
    val icon: Int
    val privacyURL: String?

}
