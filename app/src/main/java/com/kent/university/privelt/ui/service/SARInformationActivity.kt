package com.kent.university.privelt.ui.service

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kent.university.privelt.databinding.ActivitySarInformationBinding

class SARInformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySarInformationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.sarInformation.text = intent.getStringExtra(SARServiceActivity.SAR_INFORMATION)
    }

}
