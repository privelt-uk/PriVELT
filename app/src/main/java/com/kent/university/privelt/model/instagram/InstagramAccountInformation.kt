package com.kent.university.privelt.model.instagram

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class InstagramAccountInformationData(
    @SerializedName("Contact syncing")
    val contactSyncing: InstagramInnerGeneric?,
    @SerializedName("First country code")
    val firstCountryCode: InstagramInnerGeneric?,
    @SerializedName("Has shared live video")
    val hasSharedLiveVideo: InstagramInnerGeneric?,
    @SerializedName("Last login")
    val lastLogin: InstagramInnerGeneric?,
    @SerializedName("Last logout")
    val lastLogout: InstagramInnerGeneric?,
    @SerializedName("First story time")
    val firstStoryTime: InstagramInnerGeneric?,
    @SerializedName("Last story time")
    val lastStoryTime: InstagramInnerGeneric?,
    @SerializedName("First close friends story time")
    val firstCloseFriendsStoryTime: InstagramInnerGeneric?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val contactSyncingUserData = contactSyncing?.toUserData(serviceId, date, parentId, "Instagram Contact Syncing Data") ?: emptyList()
        val firstCountryCodeUserData = firstCountryCode?.toUserData(serviceId, date, parentId, "Instagram First Country Code Data") ?: emptyList()
        val hasSharedLiveVideoUserData = hasSharedLiveVideo?.toUserData(serviceId, date, parentId, "Instagram Has Shared Live Video Data") ?: emptyList()
        val lastLoginUserData = lastLogin?.toUserData(serviceId, date, parentId, "Instagram Last Login Data") ?: emptyList()
        val lastLogoutUserData = lastLogout?.toUserData(serviceId, date, parentId, "Instagram Last Logout Data") ?: emptyList()
        val firstStoryTimeUserData = firstStoryTime?.toUserData(serviceId, date, parentId, "Instagram First Story Time Data") ?: emptyList()
        val lastStoryTimeUserData = lastStoryTime?.toUserData(serviceId, date, parentId, "Instagram Last Story Time Data") ?: emptyList()
        val firstCloseFriendsStoryTimeUserData = firstCloseFriendsStoryTime?.toUserData(serviceId, date, parentId, "Instagram First Close Friends Story Data") ?: emptyList()

        allData.addAll(contactSyncingUserData)
        allData.addAll(firstCountryCodeUserData)
        allData.addAll(hasSharedLiveVideoUserData)
        allData.addAll(lastLoginUserData)
        allData.addAll(lastLogoutUserData)
        allData.addAll(firstStoryTimeUserData)
        allData.addAll(lastStoryTimeUserData)
        allData.addAll(firstCloseFriendsStoryTimeUserData)

        return allData
    }
}

data class InstagramInnerGeneric(
    @SerializedName("href")
    val href: String?,
    @SerializedName("value")
    val value: String?,
    @SerializedName("timestamp")
    val timestamp: Long?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String, userDataTitle: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "$userDataTitle $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}
