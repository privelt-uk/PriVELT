package com.kent.university.privelt.utils

import android.util.Log
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.UserEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object PriVELTLog {

    fun SCREEN_NAV(message: String) = logMessage("screen_nav", message)

    fun BUTTON_PRESSED(message: String) = logMessage("button_pressed", message)

    fun ACTION_RESULT(message: String) = logMessage("action_result", message)

    private fun logMessage(tag: String, message: String) {
        CoroutineScope(Dispatchers.IO).launch {
            Log.i("PRiVELTLog", "[$tag] $message")
            val userEvent = UserEvent(tag, message, System.currentTimeMillis())
            val userEventRepository = PriVELTDatabase.getInstance(PriVELTApplication.instance)?.userEventDao()
            userEventRepository?.insertUserEvent(userEvent)
        }
    }
}