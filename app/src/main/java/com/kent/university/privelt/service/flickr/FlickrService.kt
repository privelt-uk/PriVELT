package com.kent.university.privelt.service.flickr

import android.content.Context
import android.util.Base64
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

class FlickrService: Service("Flickr", false, "", "", "") {
    lateinit var APIServiceListener: APIServiceListener
    val redirectUrl = "privelt://flickr"
    val clientId = "79823a483117c7f0acc24f73d6ef1750"
    val clientSecret = "d324d6100ca34e1e"
    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(token: String, userId: String, context: Context, fullName: String, tokenSecret: String) = CoroutineScope(
        Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }

        systemTime = System.currentTimeMillis()

        this@FlickrService.user = fullName
        val id = insertOrGetService(context)
        getProfile(context, id, userId, token, tokenSecret)
    }

    fun connection(context: Context, webView: WebView) {
        getAuthCode(webView, context)
    }

    fun getRandomString(length: Int) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }

    fun getTimeStamp(): String {
        val tsLong = System.currentTimeMillis() / 1000
        return tsLong.toString()
    }

    fun getAuthCode(webView: WebView, context: Context) {
        val uniqueToken = getRandomString(16)
        Log.d("OAuth unique token", uniqueToken)
        val timestamp = getTimeStamp()
        val baseUrl =
            "${RetrofitFlickrInstance.BASE_URL}oauth/request_token"
        val encodedRedirectUrl = URLEncoder.encode(redirectUrl, "UTF-8")
        val parameters = "oauth_callback=$encodedRedirectUrl&oauth_consumer_key=$clientId" +
                "&oauth_nonce=$uniqueToken&oauth_signature_method=HMAC-SHA1" +
                "&oauth_timestamp=$timestamp&oauth_version=1.0"
        val encodedBaseUrl = URLEncoder.encode(baseUrl, "UTF-8")
        val encodedParameters = URLEncoder.encode(parameters, "UTF-8")
        val baseString = "GET&$encodedBaseUrl&$encodedParameters"
        Log.d("baseString", baseString)
        val signature = hmacSha1(baseString, "$clientSecret&")
        Log.d("signature", signature.toString())

        Log.d("url", "$baseUrl?$parameters&oauth_signature=$signature")

        RetrofitFlickrInstance
            .getStringRetrofitInstance()
            .requestToken(uniqueToken, timestamp, clientId, "HMAC-SHA1",
                "1.0", signature!!, redirectUrl)
            .enqueue(object: Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.e("flickr", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.code() == 200) {
                        Log.d("flickr","Fetched flickr auth code!")
                        response.body().let {
                            if (it != null) {
                                val array = it.split('&')
                                val callbackConfirmed = array[0].split('=').last()
                                val token = array[1].split('=').last()
                                val tokenSecret = array[2].split('=').last()
                                Log.d("flickr", token)

                                openBrowser(webView, context, token, tokenSecret)
                            }
                        }
                    }
                    else {
                        Log.d("flickr", response.body().toString())
                        Log.d("flickr", response.message())
                        Log.d("flickr","Fetching flickr auth code failed!")
                        val exception = ApiResponseException(
                            "Fetching flickr auth code failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching flickr auth code failed!")
                    }
                }
            })
    }

    @Throws(
        UnsupportedEncodingException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class
    )
    private fun hmacSha1(value: String, key: String): String? {
        val type = "HmacSHA1"
        val secret = SecretKeySpec(key.toByteArray(), type)
        val mac: Mac = Mac.getInstance(type)
        mac.init(secret)
        val bytes: ByteArray = mac.doFinal(value.toByteArray())
        return Base64.encodeToString(bytes, Base64.NO_WRAP)
    }

    fun openBrowser(webView: WebView, context: Context, token: String, tokenSecret: String) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        val baseUrl = RetrofitFlickrInstance.AUTH_URL
        val url = "$baseUrl?oauth_token=$token&perms=read"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView?.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith("privelt")) {
                        // This is our request. Parse the redirect URL query parameters to get the code

                        request.url.getQueryParameter("oauth_token")?.let { authToken ->
                            Log.d("OAuth code", authToken)

                            request.url.getQueryParameter("oauth_verifier")?.let { verifier ->
                                Log.d("OAuth verifier", verifier)
                                getAccessToken(context, authToken, verifier, tokenSecret)
                            }
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            APIServiceListener.error("Authorization code not received :(")

                            val exception = ApiResponseException(
                                "Fetching flickr auth code failed!",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    fun getAccessToken(context: Context, authToken: String, verifier: String, tokenSecret: String) {
        val uniqueToken = getRandomString(16)
        Log.d("OAuth unique token", uniqueToken)
        val timestamp = getTimeStamp()
        val baseUrl =
            "${RetrofitFlickrInstance.BASE_URL}oauth/access_token"
        val parameters = "oauth_consumer_key=$clientId&oauth_nonce=$uniqueToken" +
                "&oauth_signature_method=HMAC-SHA1&oauth_timestamp=$timestamp" +
                "&oauth_token=$authToken&oauth_verifier=$verifier&oauth_version=1.0"
        val encodedBaseUrl = URLEncoder.encode(baseUrl, "UTF-8")
        val encodedParameters = URLEncoder.encode(parameters, "UTF-8")
        val baseString = "GET&$encodedBaseUrl&$encodedParameters"
        Log.d("baseString", baseString)
        val signature = hmacSha1(baseString, "$clientSecret&$tokenSecret")
        Log.d("signature", signature.toString())

        Log.d("url", "$baseUrl?$parameters&oauth_signature=$signature")

        RetrofitFlickrInstance
            .getStringRetrofitInstance()
            .accessToken(authToken, clientId, uniqueToken, timestamp, signature!!,
                "HMAC-SHA1", verifier, "1.0")
            .enqueue(object: Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.e("flickr", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.code() == 200) {
                        Log.d("flickr","Fetched flickr access token!")
                        response.body().let {
                            if (it != null) {
                                val array = it.split('&')
                                val fullName = array[0].split('=').last()
                                val token = array[1].split('=').last()
                                val tokenSecret = array[2].split('=').last()
                                val userId = array[3].split('=').last()
                                val username = array[4].split('=').last()

                                Log.d("flickr", token)
                                insertServiceAndFetchData(token, userId, context, fullName, tokenSecret)
                            }
                        }
                    }
                    else {
                        Log.d("flickr", response.body().toString())
                        Log.d("flickr", response.message())
                        Log.d("flickr","Fetching flickr access token failed!")
                        val exception = ApiResponseException(
                            "Fetching flickr access token failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching flickr access token failed!")
                    }
                }
            })
    }

    fun getProfile(context: Context, serviceId: Long, userId: String, authToken: String, tokenSecret: String) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        RetrofitFlickrInstance
            .getRetrofitInstance()
            .getProfile(clientId, "flickr.profile.getProfile", userId, "json", "1")
            .enqueue(object: Callback<FlickrUser> {
                override fun onFailure(call: Call<FlickrUser>, t: Throwable) {
                    Log.e("flickr", t.message.toString())
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<FlickrUser>, response: Response<FlickrUser>) {
                    if (response.code() == 200) {
                        Log.d("flickr","Fetched flickr profile!")
                        response.body().let {
                            if (it != null) {
                                val allData = mutableListOf<UserData>()
                                val test = response.body()?.profile?.toUserData(serviceId,systemTime) ?: emptyList()
                                allData.addAll(test)
                                CoroutineScope(Dispatchers.IO).launch {
                                    allData.forEach {
                                        if (it.isValid())
                                            userDataRepository?.insertUserData(it)
                                    }
                                }
                            }
                            APIServiceListener.success()
                        }
                    }
                    else {
                        Log.d("flickr", response.message())
                        Log.d("flickr","Fetching flickr profile failed!")
                        val exception = ApiResponseException(
                            "Fetching flickr profile failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching flickr profile failed!")
                    }
                }
            })
    }

    fun getContacts(context: Context, serviceId: Long, authToken: String, tokenSecret: String) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val uniqueToken = getRandomString(16)
        Log.d("OAuth unique token", uniqueToken)
        val timestamp = getTimeStamp()
        val baseUrl =
            "${RetrofitFlickrInstance.BASE_URL}rest"
        val parameters = "format=json&method=flickr.contacts.getList&nojsoncallback=1" +
                "&oauth_consumer_key=$clientId&oauth_nonce=$uniqueToken" +
                "&oauth_signature_method=HMAC-SHA1&oauth_timestamp=$timestamp" +
                "&oauth_token=$authToken&oauth_version=1.0"
        val encodedBaseUrl = URLEncoder.encode(baseUrl, "UTF-8")
        val encodedParameters = URLEncoder.encode(parameters, "UTF-8")
        val baseString = "GET&$encodedBaseUrl&$encodedParameters"
        Log.d("baseString", baseString)
        val signature = hmacSha1(baseString, "$clientSecret&$tokenSecret")
        Log.d("signature", signature.toString())

        Log.d("url", "$baseUrl?$parameters&oauth_signature=$signature")

        RetrofitFlickrInstance
            .getRetrofitInstance()
            .getContacts("json", "flickr.contacts.getList", "1",
                clientId, uniqueToken, timestamp, authToken, signature!!, "HMAC-SHA1",
                "1.0")
            .enqueue(object: Callback<FlickrContacts> {
                override fun onFailure(call: Call<FlickrContacts>, t: Throwable) {
                    Firebase.crashlytics.recordException(t)
                    Log.e("flickr", t.message.toString())
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<FlickrContacts>, response: Response<FlickrContacts>) {
                    if (response.code() == 200) {
                        Log.d("flickr","Fetched flickr contacts!")
                        response.body().let {
                            if (it != null) {
                                val contact = response.body()?.contacts?.contact

                                val allData = mutableListOf<UserData>()
                                val initialUserData = UserData(title = "Flickr Contacts",
                                    type = DataTypeList.FEED.title,
                                    subtype = "Flickr Contacts",
                                    value = "",
                                    serviceId = id,
                                    date = systemTime)
                                allData.add(initialUserData)
                                val test = contact?.map { it ->
                                    it.toUserData(serviceId, systemTime, initialUserData.id)
                                }?.flatten() ?: emptyList()
                                allData.addAll(test)
                                CoroutineScope(Dispatchers.IO).launch {
                                    allData.forEach { item ->
                                        if (item.isValid()) {
                                            userDataRepository?.insertUserData(item)
                                        }
                                    }
                                }
                            }
                            APIServiceListener.success()
                        }
                    }
                    else {
                        Log.d("flickr", response.body().toString())
                        Log.d("flickr", response.message())
                        Log.d("flickr","Fetching flickr contacts failed!")
                        val exception = ApiResponseException(
                            "Fetching flickr contacts failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching flickr contacts failed!")
                    }
                }
            })
    }

}