package com.kent.university.privelt.service.spotify

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {
    @FormUrlEncoded
    @POST("token")
    fun requestToken(@Field("grant_type") grantType: String,
                     @Field("redirect_uri") redirectUri: String,
                     @Field("code") code: String,
                     @Header("Authorization") authorization: String): Call<AuthRequestTokenResponse>

    @GET("me")
    fun requestProfile(@Header("Authorization") authorization: String): Call<Profile>
}

class RetrofitSpotifyInstance {
    companion object {
        private const val BASE_AUTH_URL: String = "https://accounts.spotify.com/api/"
        private const val BASE_API_URL: String = "https://api.spotify.com/v1/"

        fun getAuthRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_AUTH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthRequestTokenResponse(val access_token: String,
                                    val token_type: String,
                                    val scope: String,
                                    val expires_in: Int,
                                    val refresh_token: String)

data class Profile(val country: String,
                   val display_name: String,
                   val email: String,
                   val href: String,
                   val type: String,
                   val uri: String) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Spotify Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Spotify Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val initialLocationUserData = UserData(title = "Spotify Profile Location",
            type = DataTypeList.LOCATION.title,
            subtype = "Spotify Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialLocationUserData)

        val countryUserData = UserData(title = "Spotify Profile Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country,
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(countryUserData)

        val displayNameUserData = UserData(title = "Spotify Profile Display Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = display_name,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(displayNameUserData)

        val emailUserData = UserData(title = "Spotify Profile Email",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = email,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(emailUserData)

        val hrefUserData = UserData(title = "Spotify Profile HREF",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.WEBSITE.value,
            value = href,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(hrefUserData)

        val uriUserData = UserData(title = "Spotify Profile URI",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.WEBSITE.value,
            value = uri,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(uriUserData)

        val typeUserData = UserData(title = "Spotify Profile Account Type",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Account type: $type",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(typeUserData)
        return allData
    }
}
