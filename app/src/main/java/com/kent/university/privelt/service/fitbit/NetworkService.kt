package com.kent.university.privelt.service.fitbit

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {
    @FormUrlEncoded
    @POST("oauth2/token")
    fun requestToken(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("code") code: String,
        @Field("redirect_uri") redirectUri: String,
        @Field("code_verifier") codeVerifier: String
    ): Call<AuthRequestTokenResponse>

    @GET("1/user/{id}/profile.json")
    fun requestProfile(
        @Path("id") userId: String,
        @Header("Authorization") authorize: String
    ): Call<FitbitUser>
}

class RetrofitFitbitInstance {
    companion object {
        private const val BASE_API_URL: String = "https://api.fitbit.com/"

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthRequestTokenResponse(val access_token: String,
                                    val token_type: String,
                                    val user_id: String,
                                    val scope: String,
                                    val expires_in: Int,
                                    val refresh_token: String)

data class FitbitUser(val user: FitbitUserDetails)

data class FitbitUserDetails(val aboutMe: String?,
                             val age: Int,
                             val ambassador: Boolean,
                             val autoStrideEnabled: Boolean?,
                             val averageDailySteps: Int,
                             val challengesBeta: Boolean,
                             val country: String,
                             val corporate: Boolean,
                             val corporateAdmin: Boolean,
                             val dateOfBirth: String,
                             val displayName: String,
                             val displayNameSetting: String,
                             val firstName: String,
                             val foodsLocale: Boolean,
                             val fullName: Boolean,
                             val gender: String,
                             val height: Int,
                             val heightUnit: String,
                             val isBugReportEnabled: Boolean,
                             val isChild: Boolean,
                             val isCoach: Boolean,
                             val languageLocale: String,
                             val lastName: String,
                             val legalTermsAcceptRequired: Boolean,
                             val locale: String,
                             val memberSince: String,
                             val mfaEnabled: Boolean,
                             val phoneNumber: String,
                             val sdkDeveloper: Boolean,
                             val sleepTracking : String,
                             val timezone: String,
                             val waterUnit: String,
                             val waterUnitName: String,
                             val weight: Float,
                             val weightUnit: String) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val profileUserData = UserData(title = "Fitbit Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Fitbit Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(profileUserData)

        val locationUserData = UserData(title = "Fitbit Locations",
            type = DataTypeList.LOCATION.title,
            subtype = "Fitbit Locations",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(locationUserData)

        val aboutMeData = UserData(title = "Fitbit About Me",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "About Me: $aboutMe",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(aboutMeData)

        val ageData = UserData(title = "Fitbit Age",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.AGE_INFO.value,
            value = age.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(ageData)

        val ambassadorData = UserData(title = "Fitbit Ambassador",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = ambassador.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(ambassadorData)

        val autoStrideEnabledData = UserData(title = "Fitbit Auto Stride Enabled",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = autoStrideEnabled.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(autoStrideEnabledData)

        val averageDailyStepsData = UserData(title = "Fitbit Average Daily Steps",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Average Daily Steps: $averageDailySteps",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(averageDailyStepsData)

        val challengesBetaData = UserData(title = "Fitbit Challenges Beta",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = challengesBeta.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(challengesBetaData)

        val countryData = UserData(title = "Fitbit Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(countryData)

        val corporateData = UserData(title = "Fitbit Corporate",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = corporate.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(corporateData)

        val corporateAdminData = UserData(title = "Fitbit Corporate Admin",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = corporateAdmin.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(corporateAdminData)

        val dateOfBirthData = UserData(title = "Fitbit Date Of Birth",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.AGE_INFO.value,
            value = dateOfBirth,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(dateOfBirthData)

        val displayNameData = UserData(title = "Fitbit Display Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = displayName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(displayNameData)

        val displayNameSettingData = UserData(title = "Fitbit Display Name Setting",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = displayNameSetting,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(displayNameSettingData)

        val firstNameData = UserData(title = "Fitbit First Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = firstName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(firstNameData)

        val foodsLocaleData = UserData(title = "Fitbit Foods Locale",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = foodsLocale.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(foodsLocaleData)

        val showFullNameData = UserData(title = "Fitbit Show Full Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = fullName.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(showFullNameData)

        val genderData = UserData(title = "Fitbit Gender",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.GENDER.value,
            value = gender,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(genderData)

        val heightData = UserData(title = "Fitbit Height",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.HEIGHT.value,
            value = height.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(heightData)

        val heightUnitData = UserData(title = "Fitbit Height Unit",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = heightUnit,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(heightUnitData)

        val isBugReportEnabledData = UserData(title = "Fitbit Is Bug Report Enabled",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = isBugReportEnabled.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(isBugReportEnabledData)

        val isChildData = UserData(title = "Fitbit Is Child",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = isChild.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(isChildData)

        val isCoachData = UserData(title = "Fitbit Is Coach",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = isCoach.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(isCoachData)

        val languageLocaleData = UserData(title = "Fitbit Language Locale",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = languageLocale,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(languageLocaleData)

        val lastNameData = UserData(title = "Fitbit Last Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = lastName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(lastNameData)

        val legalTermsAcceptRequiredData = UserData(title = "Fitbit Legal Terms Accept Required",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = legalTermsAcceptRequired.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(legalTermsAcceptRequiredData)

        val localeData = UserData(title = "Fitbit Locale",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = locale,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(localeData)

        val memberSinceData = UserData(title = "Fitbit Member Since",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "member since: $memberSince",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(memberSinceData)

        val mfaEnabledData = UserData(title = "Fitbit MFA Enabled",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = mfaEnabled.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(mfaEnabledData)

        val phoneNumberData = UserData(title = "Fitbit Phone Number",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.MOBILE.value,
            value = phoneNumber,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(phoneNumberData)

        val sdkDeveloperData = UserData(title = "Fitbit SDK Developer",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = sdkDeveloper.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(sdkDeveloperData)

        val sleepTrackingData = UserData(title = "Fitbit Sleep Tracking",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = sleepTracking,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(sleepTrackingData)

        val timezoneData = UserData(title = "Fitbit Timezone",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = timezone,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(timezoneData)

        val waterUnitData = UserData(title = "Fitbit Water Unit",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = waterUnit,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(waterUnitData)

        val waterUnitNameData = UserData(title = "Fitbit Water Unit Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = waterUnitName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(waterUnitNameData)

        val weightData = UserData(title = "Fitbit Weight",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.WEIGHT.value,
            value = weight.toString(),
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(weightData)

        val weightNameData = UserData(title = "Fitbit Weight Unit",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = weightUnit,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(weightNameData)
        return allData
    }
}
