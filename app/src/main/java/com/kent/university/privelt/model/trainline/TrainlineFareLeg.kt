package com.kent.university.privelt.model.trainline

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataSubTypeListFinancials
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TrainlineFareLeg(val orderCreatedDate: String?,
                            val fareLegOriginName: String?,
                            val fareLegDestinationName: String?,
                            val fare_discount_card_names: String?,
                            val fare_category_name: String?,
                            val fare_travel_class_name: String?,
                            val fare_passenger_number_senior: String?,
                            val fare_passenger_number_adult: String?,
                            val fare_passenger_number_youth: String?,
                            val fare_passenger_number_child: String?,
                            val fare_passenger_number_infant: String?,
                            val fare_passengers: String?) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Trainline Fare Legs",
            type = DataTypeList.ACTIVITY.title,
            subtype = "Trainline Fare Legs",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val orderCreatedDateUserData = UserData(
            title = "Trainline Fare Leg Order Created Date",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Order Created date: ${orderCreatedDate ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(orderCreatedDateUserData)
        val fareLegOriginNameUserData = UserData(
            title = "Trainline Fare Leg Origin Name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Leg Origin name: ${fareLegOriginName?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareLegOriginNameUserData)
        val fareLegDiscountCardNamesUserData = UserData(
            title = "Trainline Fare Leg Discount Card Names",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Leg Discount Card name: ${fare_discount_card_names ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareLegDiscountCardNamesUserData)
        val fareCategoryNameUserData = UserData(
            title = "Trainline Fare Category Name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare category name: ${fare_category_name ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareCategoryNameUserData)
        val fareTravelClassNameUserData = UserData(
            title = "Trainline Fare Travel Class Name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare travel class name: ${fare_travel_class_name ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareTravelClassNameUserData)
        val seniorPassengersNumberUserData = UserData(
            title = "Trainline Fare Senior Passengers Number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Senior Passengers Number: ${fare_passenger_number_senior ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(seniorPassengersNumberUserData)
        val adultPassengersNumberUserData = UserData(
            title = "Trainline Fare Adult Passengers Number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Adult Passengers Number: ${fare_passenger_number_adult ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(adultPassengersNumberUserData)
        val youthPassengersNumberUserData = UserData(
            title = "Trainline Fare Youth Passengers Number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Youth Passengers Number: ${fare_passenger_number_youth ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(youthPassengersNumberUserData)
        val childPassengersNumberUserData = UserData(
            title = "Trainline Fare Child Passengers Number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Child Passengers Number: ${fare_passenger_number_child ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(childPassengersNumberUserData)
        val infantPassengersNumberUserData = UserData(
            title = "Trainline Fare Infant Passengers Number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Infant Passengers Number: ${fare_passenger_number_infant ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(infantPassengersNumberUserData)
        val passengersNumberUserData = UserData(
            title = "Trainline Fare Passengers Number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Passengers Number: ${fare_passengers ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(passengersNumberUserData)
        return allData
    }
}
