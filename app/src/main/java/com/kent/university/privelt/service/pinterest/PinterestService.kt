package com.kent.university.privelt.service.pinterest

import android.content.Context
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.pinterest.PinterestProfile
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream

class PinterestService : SARService() {
    override var sarRequestURL: String = "https://policy.pinterest.com/en-gb/privacy-policy#section-transferring-your-information"
    override val service = Service("Pinterest", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) = CoroutineScope(Dispatchers.IO).launch {
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val unixTime = System.currentTimeMillis()
            val html = inputStream.bufferedReader().use { it.readText() }

            extractProfile(context, html, unixTime)
        }
    }

    fun extractProfile(context: Context, html: String, date: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            val username = regexFind(USERNAME_REGEX, html)
            val accountCreationTime = regexFind(ACCOUNT_CREATION_TIME_REGEX, html)
            val firstName = regexFind(FIRST_NAME_REGEX, html)
            val lastName = regexFind(LAST_NAME_REGEX, html)
            val about = regexFind(ABOUT_REGEX, html)
            val location = regexFind(LOCATION_REGEX, html)
            val website = regexFind(WEBSITE_REGEX, html)
            val email = regexFind(EMAIL_REGEX, html)
            val emailVerified = regexFind(EMAIL_VERIFIED_REGEX, html)
            val gender = regexFind(GENDER_REGEX, html)
            val country = regexFind(COUNTRY_REGEX, html)
            val birthday = regexFind(BIRTHDAY_REGEX, html)
            val hideProfileFromSearchEngines = regexFind(HIDE_PROFILE_FROM_SEARCH_ENGINES_REGEX, html)
            val shareActivity = regexFind(SHARE_ACTIVITY_REGEX, html)
            val improveRecommendations = regexFind(IMPROVE_RECOMMENDATIONS_REGEX, html)
            val improveRecommendationsPartners = regexFind(IMPROVE_RECOMMENDATIONS_PARTNERS_REGEX, html)
            val improveAds = regexFind(IMPROVE_ADS_REGEX, html)
            val emailEngagement = regexFind(EMAIL_ENGAGEMENT_REGEX, html)
            val hasPinterestExtension = regexFind(HAS_PINTEREST_EXTENSION_REGEX, html)
            val interestCount = regexFind(INTEREST_COUNT_REGEX, html)
            val clickthroughCount = regexFind(CLICKTHROUGH_COUNT_REGEX, html)
            val searchCount = regexFind(SEARCH_COUNT_REGEX, html)

            val profile = PinterestProfile(username, accountCreationTime, firstName,
                lastName, about, location, website, email, emailVerified, gender, country, birthday,
                hideProfileFromSearchEngines, shareActivity, improveRecommendations, improveRecommendationsPartners,
                improveAds, emailEngagement, hasPinterestExtension, interestCount, clickthroughCount, searchCount)

            val userdata = profile.toUserData(0L, date)
            saveUserDataToRepository(context, userdata, true)
        }
    }

    private fun regexFind(regex: String, html: String): String {
        val values = Regex(regex, RegexOption.MULTILINE).find(html)?.groupValues
        return values?.first() ?: "Not found"
    }

    companion object {
        private const val USERNAME_REGEX = "Username</h2>(.*?)<"
        private const val ACCOUNT_CREATION_TIME_REGEX = "Account creation time</h2>(.*?)<"
        private const val FIRST_NAME_REGEX = "First Name</h2>(.*?)<"
        private const val LAST_NAME_REGEX = "Last Name</h2>(.*?)<"
        private const val ABOUT_REGEX = "About</h2>(.*?)<"
        private const val LOCATION_REGEX = "Location</h2>(.*?)<"
        private const val WEBSITE_REGEX = "Website</h2>(.*?)<"
        private const val EMAIL_REGEX = "Email</h2>(.*?)<"
        private const val EMAIL_VERIFIED_REGEX = "Email verified</h2>(.*?)<"
        private const val GENDER_REGEX = "Gender</h2>(.*?)<"
        private const val COUNTRY_REGEX = "Country</h2>(.*?)<"
        private const val BIRTHDAY_REGEX = "Birthday</h2>(.*?)<"
        private const val HIDE_PROFILE_FROM_SEARCH_ENGINES_REGEX = "Hide your profile from search engines: </b>(.*?)<"
        private const val SHARE_ACTIVITY_REGEX = "Share activity for ads performance reporting: </b>(.*?)<"
        private const val IMPROVE_RECOMMENDATIONS_REGEX = "Use sites you visit to improve which recommendations and ads you see: </b>(.*?)<"
        private const val IMPROVE_RECOMMENDATIONS_PARTNERS_REGEX = "Use information from our partners to improve which recommendations and ads you see: </b>(.*?)<"
        private const val IMPROVE_ADS_REGEX = "Use your activity to improve the ads you see about Pinterest on other sites or apps you may visit: </b>(.*?)<"
        private const val EMAIL_ENGAGEMENT_REGEX = "Last engagement with email:</b>(.*?)<"
        private const val HAS_PINTEREST_EXTENSION_REGEX = "Has installed the Pinterest extension:</b>(.*?)<"
        private const val INTEREST_COUNT_REGEX = "Interest count:</b>(.*?)<"
        private const val CLICKTHROUGH_COUNT_REGEX = "Clickthrough count:</b>(.*?)<"
        private const val SEARCH_COUNT_REGEX = "Search count:</b>(.*?)<"
    }
}