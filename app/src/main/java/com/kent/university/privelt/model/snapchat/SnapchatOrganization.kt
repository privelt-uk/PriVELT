package com.kent.university.privelt.model.snapchat

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class SnapchatOrganization(
    @SerializedName("Organization Members")
    val organizationMembers: List<SnapchatOrganizationMember>?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val userDataList = organizationMembers?.flatMap { it.toUserData(serviceId, date, parentId) }
        return userDataList ?: emptyList<UserData>()
    }
}

data class SnapchatOrganizationMember(
    @SerializedName("Display Name")
    val displayName: String?,
    @SerializedName("Invitation E-mail Address")
    val invitationEmailAddress: String?,
    @SerializedName("Organization Name")
    val organizationName: String?,
    @SerializedName("Contact Name")
    val contactName: String?,
    @SerializedName("Contact Phone Number")
    val contactPhoneNumber: String?,
    @SerializedName("Contact E-mail Address")
    val contactEmailAddress: String?,
    @SerializedName("Active Roles")
    val activeRoles: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Snapchat Organization $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}