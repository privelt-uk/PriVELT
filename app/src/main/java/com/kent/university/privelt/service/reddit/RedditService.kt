package com.kent.university.privelt.service.reddit

import android.content.Context
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RedditService: Service("Reddit", false, "", "", "") {
    lateinit var APIServiceListener: APIServiceListener
    private val redirectUrl = "privelt://reddit"
    val clientId = "93itdsuWsaKto5orZ-QOgg"

    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(token: String, context: Context) = CoroutineScope(
        Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }

        systemTime = System.currentTimeMillis()
        val id = insertOrGetService(context)
        getProfile(token, id, context)
    }

    fun connection(context: Context, webView: WebView) {
        openBrowser(webView, context)
    }

    private fun openBrowser(webView: WebView, context: Context) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        val redirectScheme = "privelt"
        val scope = "identity%20privatemessages%20read"
        val url = "https://www.reddit.com/api/v1/authorize?client_id=$clientId&response_type=token&state=state&redirect_uri=$redirectUrl&scope=$scope"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectScheme)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.fragment.let { fragment ->
                            val splitString = fragment?.split("&")?.filter { it -> it.contains("access_token") }
                            val accessToken = splitString?.first()?.split("=")?.last()!!
                            // Got code!
                            Log.d("OAuth code", accessToken)
                            val bearerToken = "Bearer $accessToken"
                            insertServiceAndFetchData(bearerToken, context)
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    private fun getProfile(token: String, serviceId: Long, context: Context) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val retFitbit = RetrofitRedditInstance.getAuthRetrofitInstance()
        retFitbit.requestProfile(token)
            .enqueue(object: Callback<Profile> {
                override fun onFailure(call: Call<Profile>, t: Throwable) {
                    Log.e("reddit", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    val exception = ApiResponseException(
                        "Fetching profile failed!",
                        "",
                        500)
                    Firebase.crashlytics.recordException(exception)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                    if (response.code() == 200) {
                        Log.d("reddit","Fetched profile!")
                        response.body().let { it ->
                            if (it != null) {

                                val test = response.body()?.toUserData(serviceId, systemTime)

                                //save username
                                CoroutineScope(Dispatchers.IO).launch {
                                    val serviceDataRepository =
                                        PriVELTDatabase.getInstance(context)?.serviceDao()
                                    val service =
                                        serviceDataRepository?.getServiceWithName(this@RedditService.name)
                                    service?.user = response.body()?.name ?: ""
                                    serviceDataRepository?.updateServices(service)
                                }

                                CoroutineScope(Dispatchers.IO).launch {
                                    test?.forEach {
                                        if (it.isValid())
                                            userDataRepository?.insertUserData(it)
                                    }
                                }
                            }
                            APIServiceListener.success()
                        }
                    }
                    else{
                        Log.d("reddit", response.message())
                        Log.d("reddit","Fetching profile failed!")
                        val exception = ApiResponseException(
                            "Fetching profile failed failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching profile failed!")
                    }
                }
            })
    }

}