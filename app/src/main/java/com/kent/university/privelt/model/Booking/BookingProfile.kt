package com.kent.university.privelt.model.Booking

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList

data class BookingProfile (
    var currencyCode : String,
    var language : String,
    var title : String,
    var address : String,
    var gender : String,
    var userFirstName : String,
    var userLastName : String,
    var userEmail : String,
    var phoneNumber : String,
    var smokePreference : String,
    var companyName : String,
    var ageGroup : String,
    var countryCode : String
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val profileUserData = UserData(title = "Booking Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Booking Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(profileUserData)

        val locationUserData = UserData(title = "Booking Locations",
            type = DataTypeList.LOCATION.title,
            subtype = "Booking Locations",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(locationUserData)

        val currencyCodeEntry = UserData(title = "Booking profile Data user currency code",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "currency code: $currencyCode",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(currencyCodeEntry)

        val titleEntry = UserData(title = "Booking profile Data user title",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.TITLE.value,
            value = title,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(titleEntry)

        val addressEntry = UserData(title = "Booking profile Data user address",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = address,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(addressEntry)

        val genderEntry = UserData(title = "Booking profile Data user gender",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.GENDER.value,
            value = gender,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(genderEntry)

        val firstNameEntry = UserData(title = "Booking profile Data user first name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = userFirstName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(firstNameEntry)

        val lastNameEntry = UserData(title = "Booking profile Data user last name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = userLastName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(lastNameEntry)

        val emailEntry = UserData(title = "Booking profile Data user email",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = userEmail,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(emailEntry)

        val smokePreferenceEntry = UserData(title = "Booking profile Data user smoke preference",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "smoke preference: $smokePreference",
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(smokePreferenceEntry)

        val companyNameEntry = UserData(title = "Booking profile Data user company name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = companyName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(companyNameEntry)

        val ageGroupEntry = UserData(title = "Booking profile Data user age group",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.AGE_INFO.value,
            value = ageGroup,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(ageGroupEntry)

        val countryCodeEntry = UserData(title = "Booking profile Data user country code",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = countryCode,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(countryCodeEntry)

        val phoneNumberEntry = UserData(title = "Booking profile Data user phone number",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.MOBILE.value,
            value = phoneNumber,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(phoneNumberEntry)
        return allData
    }
}