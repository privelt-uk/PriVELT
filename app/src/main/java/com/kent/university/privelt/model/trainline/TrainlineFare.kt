package com.kent.university.privelt.model.trainline

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TrainlineFare(val fareTypeName : String?,
                         val fareTypeConditionsSummary: String?,
                         val fareLegLocalDepartAt: String?,
                         val fareLegLocalArriveAt: String?,
                         val fareLegTransportMode: String?,
                         val fareLegTravelClassName: String?,
                         val fareLegDirection: String?,
                         val fareLegCarrierName: String?,
                         val fareLegRoute: String?,
                         val feeType: String?,
                         val feeCurrencyCode: String?,
                         val feeAmount: String?) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Trainline Fares",
            type = DataTypeList.ACTIVITY.title,
            subtype = "Trainline Fares",
            value = "",
            serviceId = serviceId,
            date = date)

        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val fareTypeNameUserData = UserData(
            title = "Trainline Fare Type Name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Type Name: ${fareTypeName ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareTypeNameUserData)
        val fareTypeConditionSummaryUserData = UserData(
            title = "Trainline Fare Type Condition Summary",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Type Condition Summary: ${fareTypeConditionsSummary?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareTypeConditionSummaryUserData)
        val fareLegDepartureTimeUserData = UserData(
            title = "Trainline Fare Departure time",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Departure time: ${fareLegLocalDepartAt ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareLegDepartureTimeUserData)
        val fareArrivalTimeUserData = UserData(
            title = "Trainline Fare Arrival Time",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Arrival Time: ${fareLegLocalArriveAt ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareArrivalTimeUserData)
        val fareTransportModeUserData = UserData(
            title = "Trainline Fare Transport Mode",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare transport mode: ${fareLegTransportMode ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareTransportModeUserData)
        val travelClassNameUserData = UserData(
            title = "Trainline Fare Travel class name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Travel Class Name: ${fareLegTravelClassName ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(travelClassNameUserData)
        val fareDirectionUserData = UserData(
            title = "Trainline Fare Direction",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Direction: ${fareLegDirection ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareDirectionUserData)
        val fareCarrierNameUserData = UserData(
            title = "Trainline Fare Carrier Name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare carrier name: ${fareLegCarrierName ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareCarrierNameUserData)
        val fareRouteUserData = UserData(
            title = "Trainline Fare Route",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare route: ${fareLegRoute ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareRouteUserData)
        val fareFeeTypeUserData = UserData(
            title = "Trainline Fare Fee Type",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare fee type: ${feeType ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareFeeTypeUserData)
        val fareFeeCurrencyCodeUserData = UserData(
            title = "Trainline Fare Fee Currency Code",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare Fee Currency Code: ${feeCurrencyCode ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareFeeCurrencyCodeUserData)
        val fareFeeAmountUserData = UserData(
            title = "Trainline Fare Fee Amount",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.EVENTS.value,
            value = "Fare fee amount: ${feeAmount ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(fareFeeAmountUserData)
        return allData
    }
}
