/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.dashboard.sensors.chart

import android.content.res.ColorStateList
import android.widget.CompoundButton
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.CompoundButtonCompat
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.databinding.CellSensorCheckboxBinding
import com.kent.university.privelt.events.CheckedSensorEvent
import com.kent.university.privelt.model.Sensor
import org.greenrobot.eventbus.EventBus

class SensorChartViewHolder internal constructor(private val binding: CellSensorCheckboxBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(sensor: Sensor?, scripts: LinkedHashMap<String?, Boolean>) {
        binding.sensorCheckbox.text = sensor?.title
        binding.sensorCheckbox.isChecked = scripts[sensor?.title]!!
        binding.sensorCheckbox.setTextColor(
            ResourcesCompat.getColor(
                itemView.context.resources,
                sensor?.color!!,
                null
            )
        )
        CompoundButtonCompat.setButtonTintList(
            binding.sensorCheckbox,
            ColorStateList.valueOf(
                ResourcesCompat.getColor(
                    itemView.context.resources,
                    sensor.color,
                    null
                )
            )
        )
        binding.sensorCheckbox.setOnCheckedChangeListener { _: CompoundButton?, b: Boolean ->
            scripts[sensor.title] = b
            if (scripts.containsValue(true)) {
                EventBus.getDefault().post(CheckedSensorEvent())
            } else {
                scripts[sensor.title] = true
                binding.sensorCheckbox.isChecked = true
            }
        }
        //itemView.logo.setImageResource(sensor.resId)
    }

}
