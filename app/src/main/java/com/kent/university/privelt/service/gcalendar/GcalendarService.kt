package com.kent.university.privelt.service.gcalendar

import android.content.Context
import android.util.Base64
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GcalendarService: Service("Google Calendar", false, "", "", "") {
    lateinit var APIServiceListener: APIServiceListener
    private val redirectUrl = "com.kent.university.privelt:/gcal"
    val clientId = "715125507310-25rc9iqe1afnnjvh47cigoikc8iqt0a6.apps.googleusercontent.com"

    var systemTime: Long = System.currentTimeMillis()

    fun insertServiceAndFetchData(token: String, context: Context) = CoroutineScope(
        Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }

        systemTime = System.currentTimeMillis()
        val id = insertOrGetService(context)
        getGoogleCalendarList(token, id, context)
    }

    fun connection(context: Context, webView: WebView) = CoroutineScope(
        Dispatchers.IO).launch {
        val token = getServiceRefreshToken(context)
        if (token != null && token.isNotEmpty()) {
            CoroutineScope(Dispatchers.Main).launch {
                APIServiceListener.loading()
            }
            val retGcalendar = RetrofitGcalendarInstance.getAuthRetrofitInstance()
            retGcalendar.refreshToken( grantType = "refresh_token", clientId = clientId, refreshToken = token)
                .enqueue(object: Callback<AuthRequestTokenResponse> {
                    override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                        val message = t.message.toString()
                        Log.e("googleCalendar", t.message.toString())
                    }
                    override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                        if (response.code() == 200) {

                            val token = response.body()?.access_token!!
                            val bearerToken = "Bearer $token"

                            insertServiceAndFetchData(bearerToken, context)
                        } else {
                            Log.d("googleCalendar","Token exchange failed! from refresh")
                            Log.d("googleCalendar",response.body().toString())
                        }
                    }
                })
        } else {
            CoroutineScope(Dispatchers.Main).launch{
                openBrowser(webView, context)
            }
        }
    }

    private fun openBrowser(webView: WebView, context: Context) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        val codeVerifier = "01234567890123456789012345678901234567890123456789"//getRandomString(64)
        Log.d("OAuth code verifier", codeVerifier)
        val base64 = Base64.encodeToString(codeVerifier.encodeToByteArray(), Base64.NO_WRAP).replace("==", "")
        val codeChallenge = "-4cf-Mzo_qg9-uq0F4QwWhRh4AjcAqNx7SbYVsdmyQM"//encodeHeader(base64)
        Log.d("OAuth code challenge", codeChallenge)
        val redirectScheme = "com.kent.university.privelt"
        val scope = "profile email https://www.googleapis.com/auth/calendar"
        val url = "https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=$clientId&state=state&scope=$scope&redirect_uri=$redirectUrl&code_challenge=$codeChallenge&code_challenge_method=S256"
        webView.settings.userAgentString = "Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.69 Mobile Safari/537.36"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectScheme)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.getQueryParameter("code")?.let { code ->
                            // Got code!
                            Log.d("OAuth code", code)
                            getToken(code, codeVerifier, context)
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            APIServiceListener.error("Authorization code not received :(")

                            val exception = ApiResponseException(
                                "Fetching events list failed!",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    fun getToken(code: String, codeVerifier: String, context: Context) {
        val retGcalendar = RetrofitGcalendarInstance.getAuthRetrofitInstance()
        retGcalendar.requestToken( "authorization_code", clientId, code, redirectUrl, codeVerifier)
            .enqueue(object: Callback<AuthRequestTokenResponse> {
                override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                    Log.e("googleCalendar", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                    if (response.code() == 200) {
                        Log.d("googleCalendar","Generated token!")
                        response.body()?.let { responseBody ->
                            Log.d("Access token", responseBody.access_token)
                            val token = responseBody.access_token
                            val bearerToken = "Bearer $token"
                            this@GcalendarService.password = responseBody.refresh_token
                            insertServiceAndFetchData(bearerToken, context)
                        }
                    } else {
                        Log.d("googleCalendar","Token exchange failed!")
                        Log.d("googleCalendar",response.body().toString())
                        val exception = ApiResponseException(
                            "Token exchange failed",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Token exchange failed!")
                    }
                }
            })
    }

    private fun getGoogleCalendarList(token: String, serviceId: Long, context: Context) {
        val retGoogleCalendar = RetrofitGcalendarInstance.getApiRetrofitInstance()
        retGoogleCalendar.requestCalendarList(token)
            .enqueue(object: Callback<GoogleCalendarList> {
                override fun onFailure(call: Call<GoogleCalendarList>, t: Throwable) {
                    Log.e("googleCalendar", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<GoogleCalendarList>, response: Response<GoogleCalendarList>) {
                    if (response.code() == 200) {
                        Log.d("googleCalendar","Fetched calendar list!")
                        response.body()?.let { it ->
                            it.items.forEach { calendar ->
                                getEventsList(token, calendar.id, serviceId, context)
                            }
                            APIServiceListener.success()
                        }
                    }
                    else{
                        Log.d("googleCalendar", response.message())
                        Log.d("googleCalendar","Fetching calendar list failed!")
                        val exception = ApiResponseException(
                            "Fetching calendar list failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching calendar list failed!")
                    }
                }
            })
    }

    fun getEventsList(token: String, calendarId: String, serviceId: Long, context: Context) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val retGoogleCalendar = RetrofitGcalendarInstance.getApiRetrofitInstance()
        retGoogleCalendar.requestEventsList(calendarId, token)
            .enqueue(object: Callback<GoogleEventsList> {
                override fun onFailure(call: Call<GoogleEventsList>, t: Throwable) {
                    Log.e("googleCalendar", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<GoogleEventsList>, response: Response<GoogleEventsList>) {
                    if (response.code() == 200) {
                        Log.d("googleCalendar","Fetched events list!")
                        response.body()?.let { it ->
                            val allData = mutableListOf<UserData>()
                            val initialUserData = UserData(title = "Google Events List",
                                type = DataTypeList.ACTIVITY.title,
                                subtype = "Google Events List",
                                value = "",
                                serviceId = serviceId,
                                date = systemTime)
                            val initialUserDataProfile = UserData(title = "Google Events profile data",
                                type = DataTypeList.PROFILE.title,
                                subtype = "Google Events profile data",
                                value = "",
                                serviceId = serviceId,
                                date = systemTime)
                            val initialUserDataLocation = UserData(title = "Google Events locations",
                                type = DataTypeList.LOCATION.title,
                                subtype = "Google Events locations",
                                value = "",
                                serviceId = serviceId,
                                date = systemTime)
                            mInitialUserDataProfile = initialUserDataProfile.id
                            mInitialUserDataLocation = initialUserDataLocation.id
                            allData.add(initialUserData)
                            allData.add(initialUserDataProfile)
                            //allData.add(initialUserDataLocation)
                            Log.d("googleCalendar", "$serviceId, $systemTime, ${initialUserData.id}")
                            val test = response.body()?.items?.flatMap{ it.toUserData(serviceId, systemTime, initialUserData.id) } ?: emptyList()
                            allData.addAll(test)

                            CoroutineScope(Dispatchers.IO).launch {
                                allData.forEach {
                                    if (it.isValid())
                                        userDataRepository?.insertUserData(it)
                                }
                            }
                        }
                    }
                    else{
                        Log.d("googleCalendar", response.message())
                        Log.d("googleCalendar","Fetching events list failed!")
                        val exception = ApiResponseException(
                            "Fetching events list failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching events list failed!")
                    }
                }
            })
    }

    companion object {
        var mInitialUserDataProfile: String? = null
        var mInitialUserDataLocation: String? = null
    }
}