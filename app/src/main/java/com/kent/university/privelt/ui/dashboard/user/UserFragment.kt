/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.user

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.kent.university.privelt.R
import com.kent.university.privelt.databinding.FragmentUserBinding
import com.kent.university.privelt.di.DaggerPriVELTComponent
import com.kent.university.privelt.di.RoomModule
import com.kent.university.privelt.model.CurrentUser
import com.kent.university.privelt.utils.firebase.RemoteConfigHelper
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class UserFragment : Fragment() {

    private var _binding: FragmentUserBinding? = null
    private val binding get() = _binding!!

    private var userViewModel: UserViewModel? = null
    private var currentUser: CurrentUser? = null

    private var edit: Button? = null
    private var save: Button? = null
    private var cancel: Button? = null

    private fun onBirthdayClick() {
        val listener =
            OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                var month = (monthOfYear + 1).toString()
                month = if (month.length == 1) "0$month" else month

                var day = (dayOfMonth).toString()
                day = if (day.length == 1) "0$day" else day

                binding.userEdit.birthDate.setText(
                    StringBuilder()
                        .append(day).append("/").append(month).append("/").append(year).append(" ")
                )
            }
        val calendar = Calendar.getInstance(TimeZone.getDefault())
        val dialog = DatePickerDialog(
            context!!, listener,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        dialog.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserBinding.inflate(inflater, container, false)
        val view = binding.root

        // Component
        val component = DaggerPriVELTComponent.builder().roomModule(RoomModule(context!!)).build()

        // ViewModelFactory
        val factory = component.viewModelFactory
        userViewModel = ViewModelProvider(this, factory!!)[UserViewModel::class.java]
        userViewModel?.init()

        getCurrentUser()

        userViewModel!!.getIsEditMode().observe(viewLifecycleOwner) { bool: Boolean ->
            val display = view.findViewById<View>(R.id.user_display)
            val edit = view.findViewById<View>(R.id.user_edit)

            if (bool) {
                RemoteConfigHelper.nextScreenFragment("4.0.1", "fragment_user")

                display?.visibility = View.GONE
                edit?.visibility = View.VISIBLE
                save?.visibility = View.VISIBLE
                this.edit?.visibility = View.GONE
                this.cancel?.visibility = View.VISIBLE
            } else {
                RemoteConfigHelper.nextScreenFragment("4.0.2", "fragment_user")

                display?.visibility = View.VISIBLE
                edit?.visibility = View.GONE
                save?.visibility = View.GONE
                this.edit?.visibility = View.VISIBLE
                this.cancel?.visibility = View.GONE
            }
        }

        binding.userEdit.birthDate.setOnClickListener { onBirthdayClick() }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edit = view.findViewById(R.id.edit)
        save = view.findViewById(R.id.save)
        cancel = view.findViewById(R.id.cancel)

        save?.setOnClickListener {
            if (isEmailValid(binding.userEdit.email.text.toString())) {
                userViewModel?.changeEditMode(false)
                saveUser()
            } else
                Toast.makeText(context, "The email is invalid", Toast.LENGTH_LONG).show()
        }

        edit?.setOnClickListener {
            userViewModel?.changeEditMode(true)
            updateUserData(currentUser)
        }

        cancel?.setOnClickListener {
            userViewModel?.changeEditMode(false)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getCurrentUser() {
        userViewModel?.currentUser?.observe(viewLifecycleOwner) { userData: CurrentUser? ->
            updateUserData(
                userData
            )
        }
    }

    private fun updateUserData(currentUser: CurrentUser?) {
        if (currentUser == null) {
            this.currentUser = CurrentUser("", "", "", "", "", "")
        } else
            this.currentUser = currentUser
        binding.userEdit.firstName.setText(currentUser?.firstName)
        binding.userEdit.lastName.setText(currentUser?.lastName)
        binding.userEdit.birthDate.setText(currentUser?.birthday)
        binding.userEdit.address.setText(currentUser?.address)
        binding.userEdit.email.setText(currentUser?.mail)
        binding.userEdit.phoneNumber.setText(currentUser?.phoneNumber)

        binding.userDisplay.firstNameText.text = currentUser?.firstName
        binding.userDisplay.lastNameText.text = currentUser?.lastName
        binding.userDisplay.birthdayText.text = currentUser?.birthday
        binding.userDisplay.addressText.text = currentUser?.address
        binding.userDisplay.emailText.text = currentUser?.mail
        binding.userDisplay.phoneText.text = currentUser?.phoneNumber
    }

    private fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern: Pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches() || email.isEmpty()
    }

    private fun saveUser() {
        val user = CurrentUser(
            binding.userEdit.firstName.text.toString(),
            binding.userEdit.lastName.text.toString(),
            binding.userEdit.birthDate.text.toString(),
            binding.userEdit.address.text.toString(),
            binding.userEdit.phoneNumber.text.toString(),
            binding.userEdit.email.text.toString()
        )
        userViewModel?.updateCurrentUser(user)
    }

}
