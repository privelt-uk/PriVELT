package com.kent.university.privelt.service.trainline

import android.content.Context
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.trainline.TrainlineCustomerDetails
import com.kent.university.privelt.model.trainline.TrainlineFare
import com.kent.university.privelt.model.trainline.TrainlineFareLeg
import com.kent.university.privelt.model.trainline.TrainlinePayments
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.InputStream
import java.util.zip.ZipInputStream

class TrainlineService : SARService() {

    override var sarRequestURL: String = "https://ico.org.uk/for-the-public/getting-copies-of-your-information-subject-access-request/"
    override val service = Service("Trainline", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val workbook = ZipInputStream(inputStream).use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .mapNotNull { entry ->
                        if (!entry.isDirectory && entry.name.endsWith(".xlsx")) {
                            XSSFWorkbook(zipStream)
                        } else {
                            null
                        }
                    }
                    .firstOrNull()
            }

            if(workbook != null) {
                CoroutineScope(Dispatchers.IO).launch {
                    val unixTime = System.currentTimeMillis()
                    val allData = mutableListOf<UserData>()
                    val customerUserData = extractCustomerDetails(workbook, unixTime)
                    allData.addAll(customerUserData)
                    val paymentsUserData = extractPayments(workbook, unixTime)
                    allData.addAll(paymentsUserData)
                    val faresUserData = extractFares(workbook, unixTime)
                    allData.addAll(faresUserData)
                    val fareLegsUserData = extractFareLegs(workbook, unixTime)
                    allData.addAll(fareLegsUserData)
                    saveUserDataToRepository(context, allData)
                    _stateFlow.emit(State.Success)
                }
            } else {
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
            }
        }

    private fun getCellValue(row: Row, column: Int): String {
        val cell = row.getCell(column)
        if (cell != null) {
            return when (cell.cellType) {
                CellType.NUMERIC -> {
                    cell.numericCellValue.toString()
                }

                CellType.STRING -> {
                    cell.stringCellValue
                }

                else -> {
                    ""
                }
            }
        } else {
            return ""
        }
    }

    private fun extractCustomerDetails(workbook: Workbook, date: Long): List<UserData>  {
        val customerDetails: MutableList<TrainlineCustomerDetails> =
            mutableListOf<TrainlineCustomerDetails>()
        val allData = mutableListOf<UserData>()
        val customerDetailsSheet = workbook.getSheet("Customer Details")
        if (customerDetailsSheet != null) {
            val rowIterator = customerDetailsSheet.rowIterator()
            while (rowIterator.hasNext()) {
                val next = rowIterator.next()
                val header = getCellValue(next, 0)
                if (header != "customer_id") {
                    val customerDetail = TrainlineCustomerDetails(
                        getCellValue(next, 2),
                        getCellValue(next, 6),
                        getCellValue(next, 11),
                        getCellValue(next, 12),
                        getCellValue(next, 13),
                        getCellValue(next, 14),
                        getCellValue(next, 15),
                        getCellValue(next, 16),
                        getCellValue(next, 17),
                        getCellValue(next, 18)
                    )

                    customerDetails.add(customerDetail)
                }
            }
            val userDataList = customerDetails
                .flatMap {
                    it.toUserData(0L, date)
                }
            allData.addAll(userDataList)
        }
        return allData
    }

    private fun extractPayments(workbook: Workbook, date: Long): List<UserData> {
        val payments: MutableList<TrainlinePayments> = mutableListOf<TrainlinePayments>()
        val allData = mutableListOf<UserData>()
        val paymentsSheet = workbook.getSheet("Payments")
        if (paymentsSheet != null) {
            val rowIterator = paymentsSheet.rowIterator()
            while (rowIterator.hasNext()) {
                val next = rowIterator.next()
                val header = getCellValue(next, 0)
                if(header != "amount") {
                    val payment = TrainlinePayments(
                        getCellValue(next, 0),
                        getCellValue(next, 1),
                        getCellValue(next, 2),
                        getCellValue(next, 3),
                        getCellValue(next, 4),
                        getCellValue(next, 5),
                        getCellValue(next, 6)
                    )
                    payments.add(payment)
                }
            }
            val userDataList = payments
                .flatMap {
                    it.toUserData(0L, date)
                }
            allData.addAll(userDataList)
        }
        return allData
    }

    private fun extractFareLegs(workbook: Workbook, date: Long): List<UserData>  {
        val fareLegs: MutableList<TrainlineFareLeg> = mutableListOf<TrainlineFareLeg>()
        val allData = mutableListOf<UserData>()
        val fareLegsSheet = workbook.getSheet("Fare Legs")
        if (fareLegsSheet != null) {
            val rowIterator = fareLegsSheet.rowIterator()
            while (rowIterator.hasNext()) {
                val next = rowIterator.next()
                val header = getCellValue(next, 0)
                if(header != "order_created_date") {
                    val fareLeg = TrainlineFareLeg(
                        getCellValue(next, 0),
                        getCellValue(next, 1),
                        getCellValue(next, 2),
                        getCellValue(next, 3),
                        getCellValue(next, 4),
                        getCellValue(next, 5),
                        getCellValue(next, 6),
                        getCellValue(next, 7),
                        getCellValue(next, 8),
                        getCellValue(next, 9),
                        getCellValue(next, 10),
                        getCellValue(next, 11)
                    )

                    fareLegs.add(fareLeg)
                }
            }
            val userDataList = fareLegs
                .flatMap {
                    it.toUserData(0L, date)
                }
            allData.addAll(userDataList)
        }
        return allData
    }

    private fun extractFares(workbook: Workbook, date: Long): List<UserData> {
        val fares: MutableList<TrainlineFare> = mutableListOf<TrainlineFare>()
        val allData = mutableListOf<UserData>()
        val fareLegsSheet = workbook.getSheet("Fares")
        if (fareLegsSheet != null) {
            val rowIterator = fareLegsSheet.rowIterator()
            while (rowIterator.hasNext()) {
                val next = rowIterator.next()
                val header = getCellValue(next, 0)
                if(header != "fare_setting_toc") {
                    val fare = TrainlineFare(
                        getCellValue(next, 1),
                        getCellValue(next, 2),
                        getCellValue(next, 3),
                        getCellValue(next, 4),
                        getCellValue(next, 5),
                        getCellValue(next, 6),
                        getCellValue(next, 7),
                        getCellValue(next, 8),
                        getCellValue(next, 9),
                        getCellValue(next, 10),
                        getCellValue(next, 11),
                        getCellValue(next, 12)
                    )

                    fares.add(fare)
                }
            }
            val userDataList = fares
                .flatMap {
                    it.toUserData(0L, date)
                }
            allData.addAll(userDataList)
        }
        return allData
    }
}
