/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.card.detailed

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.databinding.ActivityDetailedCardBinding
import com.kent.university.privelt.events.LaunchListDataEvent
import com.kent.university.privelt.model.Card
import com.kent.university.privelt.repositories.UserDataRepository
import com.kent.university.privelt.service.base.APIServiceEnum
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARServiceEnum
import com.kent.university.privelt.service.base.ServiceController
import com.kent.university.privelt.ui.data.DataActivity
import com.kent.university.privelt.ui.data.DataTreeActivity
import com.kent.university.privelt.ui.login.LoginActivity
import com.kent.university.privelt.ui.privacy.PrivacyActivity
import com.kent.university.privelt.ui.privacy.PrivacyActivity.Companion.LINK_PARAM
import com.kent.university.privelt.ui.service.APIServiceActivity
import com.kent.university.privelt.ui.service.SARServiceActivity
import com.kent.university.privelt.utils.capitalize
import com.kent.university.privelt.utils.getSerializableIntent
import com.kent.university.privelt.utils.sentence.SentenceAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.neferett.webviewsextractor.model.UserDataTypes
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.text.SimpleDateFormat
import java.util.*


class DetailedCardActivity : BaseActivity() {

    private lateinit var binding: ActivityDetailedCardBinding

    private var card: Card? = null
    private var date: Long? = null

    override val activityLayout: Int
        get() = R.layout.activity_detailed_card

    override fun configureViewModel() {
    }

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityDetailedCardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            card = savedInstanceState.getSerializable(PARAM_CARD, Card::class.java)
        } else if (intent != null) {
            card = intent.getSerializableIntent(PARAM_CARD, Card::class.java)
        }

        if (card!!.loggedAs.isNullOrEmpty())
            binding.loggedAs.isVisible = false
        else
            binding.loggedAs.text = "You are logged as ${card!!.loggedAs}"

        binding.titleTV.text = capitalize(card!!.title)
        title = capitalize(card!!.title)
        val priVELTApplication = binding.logo.context.applicationContext as PriVELTApplication
        if (!card!!.isService) {
            val userDataType = UserDataTypes.getUserDataType(card!!.title.uppercase(Locale.ROOT))
            if (userDataType != null)
                binding.logo.setImageResource(userDataType.res)
            else
                binding.logo.setImageResource(DataTypeList.getResourceFromTitle(card!!.title))
        } else {
            val res = priVELTApplication.serviceHelper!!.getResIdWithName(card!!.title)
            res?.let {
                binding.logo.setImageResource(res)
            }
        }
        binding.privacy.setOnClickListener {
            val intent = Intent(this, PrivacyActivity::class.java)

            val service = ServiceController.getServiceItemOrNull(card!!.title)

            if (service != null)
                intent.putExtra(LINK_PARAM, service.privacyURL)
            else
                intent.putExtra(
                    LINK_PARAM,
                    priVELTApplication.serviceHelper!!.getServiceWithName(card!!.title)?.privacyURL
                )
            startActivity(intent)
        }

        if (!(card?.isService!!))
            binding.privacy.visibility = View.GONE

        binding.refreshData.setOnClickListener {
            showRefreshDialog {
                val serviceName = card?.title
                if (serviceName != null && card?.isService == true) {
                    val serviceItem = ServiceController.getServiceItem(serviceName)
                    if (ServiceController.isAPIService(serviceItem)) {
                        val intent = Intent(this, APIServiceActivity::class.java)
                        intent.putExtra(ServiceController.PARAM_SERVICE, serviceItem as APIServiceEnum)
                        startActivityForResult(intent, 489)
                    } else if (ServiceController.isSARSService(serviceItem)) {
                        val intent = Intent(this, SARServiceActivity::class.java)
                        intent.putExtra(ServiceController.PARAM_SERVICE, serviceItem as SARServiceEnum)
                        startActivityForResult(intent, 489)
                    }
                }
            }
        }

        binding.deleteData.setOnClickListener {
            if (binding.dateSpinner.adapter.count > 1) {
                showRadioButtonDialog {
                    deleteData(it)
                }
            } else {
                showConfirmDialog {
                    deleteData(0)
                }
            }
        }

        populateSpinner()
        configureRecyclerView(-1)
        refreshPrivacyScore(-1)
    }

    private fun deleteData(option: Int) {
        CoroutineScope(Dispatchers.IO).launch {

            val userDataRepository = PriVELTDatabase.getInstance(this@DetailedCardActivity)?.userDataDao()
            val serviceDataRepository = PriVELTDatabase.getInstance(this@DetailedCardActivity)?.serviceDao()

            val serviceId = serviceDataRepository?.getServiceWithName(card!!.title)?.id!!

            val date = date
            if (option == 0 || date == null)
                userDataRepository?.deleteUserDataForAService(serviceId)
            else if (option == 1)
                userDataRepository?.deleteUserDataForAServiceAndDate(serviceId, date)

            card?.metrics?.clear()

            CoroutineScope(Dispatchers.Main).launch {
                Toast.makeText(this@DetailedCardActivity, "The data has been deleted", Toast.LENGTH_LONG).show()
                finish()
                //configureRecyclerView(-1)
                //refreshPrivacyScore(-1)
            }

        }
    }

    private fun showRadioButtonDialog(onItemSelected: (Int) -> Unit) {

        val options = arrayOf("All service data", "Current snapshot")
        var checkedItem = 0

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Please choose what you want to delete:")
        builder.setSingleChoiceItems(options, checkedItem) { dialog, which ->
            checkedItem = which
        }
        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }
        builder.setPositiveButton("Confirm") { dialog, _ ->
            dialog.dismiss()
            onItemSelected(checkedItem)
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun showConfirmDialog(onItemSelected: (Int) -> Unit) {

        var checkedItem = 0

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Do you want to delete the service data?")
        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }
        builder.setPositiveButton("Confirm") { dialog, _ ->
            dialog.dismiss()
            onItemSelected(checkedItem)
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun showRefreshDialog(onItemSelected: () -> Unit) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Do you want to re-fetch a new snapshot of this service?")
        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }
        builder.setPositiveButton("Confirm") { dialog, _ ->
            dialog.dismiss()
            onItemSelected()
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun refreshPrivacyScore(date: Long) {
        var progress = 0
        for ((_, number, mDate) in card!!.metrics)
            if (date == mDate)
                progress += number
        binding.riskProgress.progress = progress * 100 / 200
        var riskValue = progress
        if (riskValue > 100) riskValue = 100
        when {
            riskValue == 0 -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "None")
            riskValue < 20 -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "Low")
            riskValue < 60 -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "Medium")
            else -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "High")
        }
    }

    private fun populateSpinner() {
        val spinnerArray: MutableList<String> = ArrayList()
        val dateChoices : MutableList<Long> = ArrayList()

        card?.metrics?.map { it.date }?.distinct()?.forEach {
            val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            val dateString = formatter.format(Date(it))
            if (!spinnerArray.contains(dateString)) {
                spinnerArray.add(dateString)
                dateChoices.add(it)
            }
        }

        val adapter = ArrayAdapter(
            this, android.R.layout.simple_spinner_item, spinnerArray
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.dateSpinner.adapter = adapter

        binding.dateSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                configureRecyclerView(dateChoices[position])
                refreshPrivacyScore(dateChoices[position])
                date = dateChoices[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(PARAM_CARD, card)
    }

    private fun configureRecyclerView(date: Long) {
        if (card!!.metrics.size != 0) {
            binding.progressCircular.visibility = View.GONE
            binding.deleteData.isVisible = card?.isService == true
        }
        else {
            binding.clickInstruction.visibility = View.GONE
            binding.deleteData.visibility = View.GONE
        }
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.recyclerViewMetrics.layoutManager = layoutManager

        val sublist = card!!.metrics.filter { it.date == date }
        val detailedCardAdapter = DetailedCardAdapter(sublist, !card!!.isService)
        binding.recyclerViewMetrics.adapter = detailedCardAdapter
    }

    public override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    public override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onLaunchListData(event: LaunchListDataEvent) {
        val intent = Intent(this, DataTreeActivity::class.java)
        if (card!!.isService) {
            intent.putExtra(LoginActivity.PARAM_SERVICE, card!!.title)
            intent.putExtra(DataActivity.PARAM_TYPE, event.card)
        } else {
            intent.putExtra(LoginActivity.PARAM_SERVICE, event.card)
            intent.putExtra(DataActivity.PARAM_TYPE, card!!.title)
        }
        startActivity(intent)
    }

    companion object {
        const val PARAM_CARD = "PARAM_CARD"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 489) {
            finish()
        }
    }

}
