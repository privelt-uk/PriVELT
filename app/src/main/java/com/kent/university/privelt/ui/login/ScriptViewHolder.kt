/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.login

import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.databinding.CellScriptBinding

class ScriptViewHolder internal constructor(private val binding: CellScriptBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(name: String?, scripts: LinkedHashMap<String?, Boolean>) {
        binding.script.text = name
        binding.script.isChecked = scripts[name]!!
        binding.script.setOnCheckedChangeListener { _: CompoundButton?, b: Boolean ->
            scripts[name] = b
        }
    }

}
