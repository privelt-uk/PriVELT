package com.kent.university.privelt.utils.firebase

import com.google.gson.annotations.SerializedName

data class ServicesOptions(@SerializedName("service_options") val servicesOptions: List<ServiceOption>)

data class ServiceOption(@SerializedName("name") val name: String,
                         @SerializedName("type") val type: String,
                         @SerializedName("is_visible") val visibility: Boolean)