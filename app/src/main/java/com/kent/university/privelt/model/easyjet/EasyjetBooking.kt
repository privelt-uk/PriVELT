package com.kent.university.privelt.model.easyjet

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class EasyjetBooking(val bookedOn: String,
                          val dateOfBooking: String,
                          val bookingReference: String,
                          val bookedBy: String,
                          val passengerNames: List<String>,
                          val flightDetails: List<String>,
                          val payments: List<String>,
                          val bookerName: String,
                          val bookerAddressLine: String,
                          val bookerPostalCode: String,
                          val bookerCity: String,
                          val bookerCountry: String,
                          val bookerEmail: String,
                          val checkInDetails: List<String>
){
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "EasyJet Booking Data $title",
                type = DataTypeList.ACTIVITY.title,
                subtype = DataSubTypeListActivity.BOOKING.value,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}