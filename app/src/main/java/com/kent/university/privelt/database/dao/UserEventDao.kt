package com.kent.university.privelt.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kent.university.privelt.model.UserEvent

@Dao
interface UserEventDao {
    @get:Query("SELECT * FROM user_event")
    val allUserEvent: List<UserEvent>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserEvent(vararg userEvent: UserEvent?)
}