package com.kent.university.privelt.model.twitter

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TwitterAccount(
    val account: TwitterAccountDetails?) {

    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        if (account != null) {
            return account.toUserData(serviceId, date, parentId)
        }

        return emptyList()
    }
}

data class TwitterAccountDetails(val email: String?,
                                 val createdVia: String?,
                                 val username: String?,
                                 val accountId: String?,
                                 val createdAt: String?,
                                 val accountDisplayName: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter $title",
                type = DataTypeList.PROFILE.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}