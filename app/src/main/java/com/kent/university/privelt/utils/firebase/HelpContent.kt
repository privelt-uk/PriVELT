package com.kent.university.privelt.utils.firebase

import com.google.gson.annotations.SerializedName

data class HelpContent(@SerializedName("version") val version: String,
                        @SerializedName("screens") val screens: List<Screen>,
                       @SerializedName("help_content") val helpContents: List<HelpContent>) {
    data class Screen(@SerializedName("s_id") val id: String)
    data class HelpContent(@SerializedName("t_id") val helpType: Int, @SerializedName("s_id") val screenId: String, @SerializedName("Content") val content: String)

    fun getContentForScreen(screenId: String): String {
        for (helpContent in helpContents)
            if (helpContent.screenId == screenId)
                return helpContent.content
        return ""
    }

    fun getHelpTypeForScreen(screenId: String): Int {
        for (helpContent in helpContents)
            if (helpContent.screenId == screenId)
                return helpContent.helpType
        return -1
    }
}
