package com.kent.university.privelt.model.stagecoach

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList

data class StagecoachProfile(
    val email: String,
    val id: String,
    val uuid: String,
    val title: String,
    val firstName: String,
    val lastName: String,
    val mobileNumber: String,
    val dateCreated: String,
    val dateUpdated: String) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val profileUserData = UserData(title = "Stagecoach Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Stagecoach Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(profileUserData)

        val emailEntry = UserData(title = "Stagecoach profile Data user email",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = email,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(emailEntry)

        val idEntry = UserData(title = "Stagecoach profile Data user id",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = id,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(idEntry)

        val uuidEntry = UserData(title = "Stagecoach profile Data user uuid",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = uuid,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(uuidEntry)

        val titleEntry = UserData(title = "Stagecoach profile Data user title",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.TITLE.value,
            value = title,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(titleEntry)

        val createdEntry = UserData(title = "Stagecoach profile Data user created date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user created at: $dateCreated",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(createdEntry)

        val updatedEntry = UserData(title = "Stagecoach profile Data user updated date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user updated at: $dateUpdated",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(updatedEntry)

        val firstNameEntry = UserData(title = "Stagecoach profile Data user first name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = firstName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(firstNameEntry)

        val lastNameEntry = UserData(title = "Stagecoach profile Data user last name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = lastName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(lastNameEntry)

        val phoneNumberEntry = UserData(title = "Stagecoach profile Data user phone number",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.MOBILE.value,
            value = mobileNumber,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(phoneNumberEntry)
        return allData
    }
}
