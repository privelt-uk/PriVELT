package com.kent.university.privelt.model.Airbnb

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class AirbnbReservationsData(
    var guestCurrency : String,
    var numberOfGuests : String,
    var numberOrInfants : String,
    var hostingUrl : String,
    var guestVatCountry : String,
    var numberOfChildren : String,
    var numberOfAdults : String,
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = splitCamelCase(field.name)
            val value = field.getter.call(this).toString()
            UserData(title = "Airbnb Reservations $title",
                type = DataTypeList.ACTIVITY.title,
                subtype = DataSubTypeListActivity.BOOKING.value,
                value = "$title: $value",
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }

    private fun splitCamelCase(text: String): String {
        return text.replace("(?=[A-Z]+)".toRegex(), " ").trim { it <= ' ' }
    }
}