package com.kent.university.privelt.service.base

import com.kent.university.privelt.R
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.service.facebook.FacebookService
import com.kent.university.privelt.service.fitbit.FitbitService
import com.kent.university.privelt.service.flickr.FlickrService
import com.kent.university.privelt.service.gcalendar.GcalendarService
import com.kent.university.privelt.service.instagram.InstagramService
import com.kent.university.privelt.service.snapchat.SnapchatService
import com.kent.university.privelt.service.spotify.SpotifyService

enum class APIServiceEnum(
    override val service: Service,
    override val icon: Int,
    override val privacyURL: String
) : ServiceList {

    INSTAGRAM(InstagramService(), R.drawable.instagram, "https://help.instagram.com/519522125107875/"),
    FACEBOOK(FacebookService(), R.drawable.ic_facebook, "https://www.facebook.com/about/privacy"),
    SNAPCHAT(SnapchatService(), R.drawable.snapchat, "https://snap.com/en-GB/privacy/privacy-policy/"),
    SPOTIFY(SpotifyService(), R.drawable.spotify, "https://www.spotify.com/us/legal/privacy-policy/"),
    FITBIT(FitbitService(), R.drawable.fitbit, "https://www.fitbit.com/global/us/legal/privacy-policy"),
    GOOGLECALENDAR(GcalendarService(), R.drawable.gcalendar, "https://policies.google.com/privacy"),
    FLICKR(FlickrService(), R.drawable.flickr, "https://www.flickr.com/help/privacy");
}
