/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.settings

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.api.services.drive.DriveScopes
import com.kent.university.privelt.BuildConfig
import com.kent.university.privelt.R
import com.kent.university.privelt.api.GoogleDriveRepository.saveDatabaseAsJsonToGoogleDrive
import com.kent.university.privelt.api.GoogleDriveRepository.saveToDatabase
import com.kent.university.privelt.base.PDAFragment
import com.kent.university.privelt.base.PDAListener
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.databinding.FragmentSettingsBinding
import com.kent.university.privelt.model.ServicePDA
import com.kent.university.privelt.model.Settings
import com.kent.university.privelt.service.FirebaseSavingHelper.pushToFirebaseStorage
import com.kent.university.privelt.ui.master_password.ImportDataAdapter
import com.kent.university.privelt.ui.master_password.ImportDataDialog
import com.kent.university.privelt.ui.master_password.MasterPasswordActivity
import com.kent.university.privelt.utils.PriVELTLog
import com.kent.university.privelt.utils.biometric.BiometricPromptTinkManager
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class SettingsFragment : PDAFragment(), ImportDataAdapter.ImportDataListener {

    private var settingsViewModel: SettingsViewModel? = null
    private var settings: Settings? = null
    private var editTextParam: String? = null

    val exceptionHandler = CoroutineExceptionHandler { _, ex ->
        Log.e("CoroutineScope", "Caught ${Log.getStackTraceString(ex)}")
    }

    private lateinit var binding: FragmentSettingsBinding

    //TODO Here we need a recyclerview with each service and a ServiceManager
    override fun configureDesign(): View? {
        binding = FragmentSettingsBinding.inflate(layoutInflater)

        binding.changePassword.setOnClickListener {
            val intent = Intent(activity, MasterPasswordActivity::class.java)
            intent.putExtra(ARG_CHANGE_PASSWORD, true)
            PriVELTLog.BUTTON_PRESSED("User ask for change password")
            startActivity(intent)
        }
        binding.logout.setOnClickListener {
            AlertDialog.Builder(activity, R.style.AlertThemeMaterial)
                .setTitle(R.string.log_out)
                .setMessage(R.string.log_out_confirmation)
                .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                    Toast.makeText(binding.logout.context, "You are currently logged out", Toast.LENGTH_LONG).show()
                    PriVELTLog.BUTTON_PRESSED("User logged out")
                    logout()
                }
                .setNegativeButton(R.string.no, null)
                .show()
        }
        binding.downloadDrive.setOnClickListener {
            AlertDialog.Builder(activity, R.style.AlertThemeMaterial)
                .setTitle("Google Drive")
                .setMessage("The API will download the last backup in the following path from your drive: PriVELT App/user_data.zip" +
                        "\nAre you sure you want to import your backup from Google Drive?")
                .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                    binding.loader.isVisible = true
                    PriVELTLog.BUTTON_PRESSED("User ask for import data with Google Drive")
                    downloadFile()
                }
                .setNegativeButton(R.string.no, null)
                .show()
        }

        binding.deleteDataDrive.setOnClickListener {
            AlertDialog.Builder(activity, R.style.AlertThemeMaterial)
                .setTitle("Google Drive")
                .setMessage("Are you sure you want to delete your backup on Google Drive?")
                .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                    binding.loader.isVisible = true
                    PriVELTLog.BUTTON_PRESSED("User ask for delete data with Google Drive")
                    deleteFileFromGoogleDrive()
                }
                .setNegativeButton(R.string.no, null)
                .show()
        }


        listener = object : PDAListener {
            override fun onDownloadSuccess() {
                CoroutineScope(Dispatchers.IO).launch {
                    saveToDatabase(activity!!.applicationContext)
                    CoroutineScope(Dispatchers.Main).launch {
                        binding.loader.isVisible = false
                        Toast.makeText(activity, R.string.data_imported_correctly, Toast.LENGTH_LONG).show()
                        PriVELTLog.ACTION_RESULT("Google Drive backup imported")
                    }
                }
            }

            override fun onDownloadFailure() {
                PriVELTLog.ACTION_RESULT("Google Drive importation failed")
                Toast.makeText(activity, getString(R.string.not_found), Toast.LENGTH_LONG).show()
                binding.loader.isVisible = false
            }
            override fun onConnectionSuccess() {
                val account = GoogleSignIn.getLastSignedInAccount(activity!!)
                binding.googleId.text = resources.getString(R.string.logged_with, account!!.displayName)
                binding.googleLayout.visibility = View.VISIBLE
                binding.googleId.visibility = View.VISIBLE
                binding.downloadDrive.visibility = View.VISIBLE
                binding.driveConnection.visibility = View.GONE
                binding.driveSave.visibility = View.VISIBLE
                binding.deleteDataDrive.visibility = View.VISIBLE
            }

            override fun onHatUploadSuccess(fileId: String) {
                if (settings?.hatFileID2 != null && settings!!.hatFileID2?.isNotEmpty() == true)
                    deleteFileOnHAT(settings!!.hatFileID2!!)
                settings!!.hatFileID2 = fileId
                binding.fileIdEditTextHat.setText(fileId)
                binding.fileIdEditTextHat.visibility = View.VISIBLE
                settingsViewModel!!.updateSettings(settings)
            }

            override fun onHatUploadFailure(error: String) {
            }
        }
        binding.driveConnection.setOnClickListener {
            googleDriveConnection()
            PriVELTLog.BUTTON_PRESSED("User tries to connect to Google Drive")
        }
        binding.driveSave.setOnClickListener {
            AlertDialog.Builder(activity, R.style.AlertThemeMaterial)
                .setTitle("Google Drive")
                .setMessage("The API will save a backup in the following path to your drive: PriVELT App/user_data.zip" +
                        "\nAre you sure you want to save your backup to Google Drive?")
                .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                    binding.loader.isVisible = true
                    CoroutineScope(Dispatchers.IO).launch {
                        saveDatabaseAsJsonToGoogleDrive(activity!!.applicationContext)
                        CoroutineScope(Dispatchers.Main).launch {
                            binding.loader.isVisible = false
                            Toast.makeText(activity, "A backup of your data was saved to your Google Drive", Toast.LENGTH_LONG).show()
                            PriVELTLog.ACTION_RESULT("Google Drive backup saved")
                        }
                    }
                }
                .setNegativeButton(R.string.no, null)
                .show()
        }
        binding.fileIdEditTextHat.setOnClickListener {
            val clipboard = activity!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Copied text", binding.fileIdEditTextHat.text.toString())
            clipboard.setPrimaryClip(clip)
            Toast.makeText(activity, "Text copied to clipboard", Toast.LENGTH_LONG).show()
        }
        binding.disconnect.setOnClickListener {
            AlertDialog.Builder(activity, R.style.AlertThemeMaterial)
                .setTitle("Google Drive")
                .setMessage("Are you sure you want to disconnect from Google Drive?")
                .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                    val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(Scope(DriveScopes.DRIVE_FILE))
                        .build()
                    val googleSignInClient = GoogleSignIn.getClient(activity!!, signInOptions)
                    googleSignInClient.signOut()
                    settings!!.isGoogleDriveAutoSave = false
                    settingsViewModel!!.updateSettings(settings)
                    PriVELTLog.BUTTON_PRESSED("User disconnection")
                }
                .setNegativeButton(R.string.no, null)
                .show()
        }
        configureViewModel()
        getSettings()
        configureHat()

        val manager = BiometricPromptTinkManager(activity!!)
        binding.fingerprintSettings.isChecked = manager.isFingerPrintAvailable() && manager.checkIfPreviousEncryptedData()

        binding.fingerprintSettings.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                manager.encryptPrompt("".toByteArray(), failedAction = {
                    binding.fingerprintSettings.isChecked = false
                    Toast.makeText(activity, "Failure", Toast.LENGTH_LONG).show()
                }, {
                    binding.fingerprintSettings.isChecked = true
                    Toast.makeText(activity, "Success", Toast.LENGTH_LONG).show()
                })
            } else {
                manager.clearMasterPassword()
            }
            PriVELTLog.BUTTON_PRESSED("User changed fingerprint settings $isChecked")
        }

        binding.exportData.setOnClickListener {
            alertDialogBuilder("Enter your Prolific ID", "Prolific ID") { input ->
                if (input.isEmpty()) {
                    Toast.makeText(activity, "Prolific ID can not be empty", Toast.LENGTH_LONG).show()
                    return@alertDialogBuilder
                }
                pushToFirebaseStorage(activity!!, binding.loader, input)
            }
        }

        resetMasterPassword()
        binding.version.text = "Version " + BuildConfig.VERSION_NAME

        return binding.root
    }

    private fun logout() {
        startActivity(Intent(activity, MasterPasswordActivity::class.java).putExtra(PARAM_DISABLE_FINGERPRINT, true))
        activity!!.setResult(Activity.RESULT_OK)
        activity!!.finish()
    }

    @SuppressLint("StaticFieldLeak")
    private fun resetMasterPassword() {
        binding.reset.setOnClickListener {
            AlertDialog.Builder(activity)
                .setTitle(R.string.reset_confirmation)
                .setMessage(R.string.process_confirmation)
                .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                    binding.loader.visibility = View.VISIBLE
                    CoroutineScope(Dispatchers.IO).launch {
                        PriVELTDatabase.getInstance(binding.reset.context)?.clearAllTables()
                        CoroutineScope(Dispatchers.Main).launch {
                            PriVELTLog.BUTTON_PRESSED("User reset master password")
                            logout()
                            Toast.makeText(
                                activity,
                                R.string.reset_done,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
                .setNegativeButton(R.string.no, null)
                .show()
        }
    }

    override fun googleLogged() {
        Toast.makeText(context, "Google Drive is connected successfully", Toast.LENGTH_LONG).show()
    }

    override val fragmentLayout: Int
        get() = R.layout.fragment_settings

    private fun getSettings() {
        settingsViewModel!!.settings?.observe(this, Observer { settings: Settings? -> updateSettings(settings) })
    }

    private fun updateSettings(settings: Settings?) {
        val account = GoogleSignIn.getLastSignedInAccount(activity!!)
        this.settings = settings
        if (settings == null) this.settings = Settings(false, null, null)
        if (account != null) {
            binding.googleId.text = resources.getString(R.string.logged_with, account.displayName)
            binding.googleLayout.visibility = View.VISIBLE
            binding.downloadDrive.visibility = View.VISIBLE
            binding.deleteDataDrive.visibility = View.VISIBLE
            binding.googleId.visibility = View.VISIBLE
            binding.driveConnection.visibility = View.GONE
            binding.driveSave.visibility = View.VISIBLE
        } else {
            this.settings?.isGoogleDriveAutoSave = false
            binding.googleLayout.visibility = View.GONE
            binding.downloadDrive.visibility = View.GONE
            binding.googleId.visibility = View.GONE
            binding.deleteDataDrive.visibility = View.GONE
            binding.driveConnection.visibility = View.VISIBLE
            binding.driveSave.visibility = View.GONE
        }
        if (settings?.hatFileID2 != null && settings.hatFileID2?.isNotEmpty() == true)
            binding.fileIdEditTextHat.setText(settings.hatFileID2)
    }

    override fun configureViewModel() {
        settingsViewModel = getViewModel(SettingsViewModel::class.java)
        settingsViewModel?.init()
    }

    companion object {
        const val ARG_CHANGE_PASSWORD = "ARG_CHANGE_PASSWORD"
        const val PARAM_DISABLE_FINGERPRINT = "param disable fingerprint"
    }

    private fun configureHat() {
        initHatLoginButton()
    }

    private fun initHatLoginButton() {
        binding.hatSwitch.setOnClickListener {
            loginHAT(binding.hatEmail.text.toString())
        }
    }

    override fun hatLogged() {
        uploadDatabaseWithHAT()
    }

    override fun onPDAClick(servicePDA: ServicePDA) {
        when (servicePDA.title) {
            "HAT" -> {}//processHatClick()
            "Google" -> processGoogleClick()
        }
    }

    private fun processGoogleClick() {
        downloadFile()
    }

    private fun alertDialogBuilder(title: String, hint: String, onPositiveClick: (input: String) -> Unit) {
        val alert = AlertDialog.Builder(activity)
        val edittext = EditText(activity)
        var dialog: AlertDialog? = null
        edittext.setTextColor(Color.parseColor("#000000"))
        edittext.hint = hint
        edittext.setSingleLine()
        val container = FrameLayout(activity!!)
        val params: FrameLayout.LayoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        params.leftMargin = resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
        params.topMargin = resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
        params.rightMargin = resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
        params.bottomMargin = resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
        edittext.layoutParams = params
        container.addView(edittext)
        alert.setView(container)
        alert.setTitle(title)
        alert.setPositiveButton(R.string.continue_string) { _: DialogInterface?, _: Int ->
            onPositiveClick(edittext.text.toString())
        }
        alert.setNegativeButton("Cancel") { _: DialogInterface?, _: Int ->
            dialog?.dismiss()
        }
        dialog = alert.show()
    }

    private fun deleteFileFromGoogleDrive() = CoroutineScope(Dispatchers.Main).launch {

        val fileNameToDelete = "user_data.zip"
        googleDriveConnection()

        val deletionSuccessful = mDriveServiceHelper!!.deleteFileFromDrive(fileNameToDelete).await()
        binding.loader.isVisible = false
        if (deletionSuccessful) {
            Log.i("Privelt", "deletion successful")
            Toast.makeText(context, "File deleted successfully", Toast.LENGTH_LONG).show()
        } else {
            Log.i("Privelt", "deletion failed")
            Toast.makeText(context, "Failed to delete file", Toast.LENGTH_LONG).show()
        }
    }
}