package com.kent.university.privelt.service.twitter

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {
    @FormUrlEncoded
    @POST("oauth2/token")
    fun requestToken(@Field("client_id") clientId: String,
                     @Field("grant_type") grantType: String,
                     @Field("redirect_uri") redirectUri: String,
                     @Field("code_verifier") codeVerifier: String,
                     @Field("code") code: String): Call<AuthRequestTokenResponse>

    @GET("users/me")
    fun profile(@Query("user.fields") fields: String,
                @Header("Authorization") accessToken: String): Call<ProfileData>

    @GET("users/{id}/timelines/reverse_chronological")
    fun timeline(@Path("id") userId: String,
                 @Query("tweet.fields") fields: String,
                 @Header("Authorization") accessToken: String): Call<TweetsData>
}

class RetrofitTwitterInstance {
    companion object {
        private const val BASE_API_URL: String = "https://api.twitter.com/2/"

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthRequestTokenResponse(val access_token: String,
                                    val token_type: String,
                                    val scope: String,
                                    val expires_in: Int)

data class ProfileData(val data: Profile) {

    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        return this.data.toUserData(serviceId, date)
    }
}

data class Profile(val id: String,
                   val name: String,
                   val username: String,
                   val created_at: String,
                   val protected: Boolean,
                   val withheld: Boolean,
                   val location: String,
                   val url: String,
                   val description: String,
                   val verified: Boolean) {

    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter $title",
                type = DataTypeList.PROFILE.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date)
        }
        return userDataList
    }
}

data class TweetsData(val data: List<Tweets>) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        return this.data.map {
            it.toUserData(serviceId, date)
        }
    }
}

data class Tweets(val text: String,
                  val created_at: String,
                  val source: String,
                  val lang: String,
                  val geo: TweetsGeo,
                  val public_metrics: TweetsPublicMetrics,
                  val possibly_sensitive: Boolean) {
    fun toUserData(serviceId: Long, date: Long): UserData {
        return UserData(title = "Twitter Feed",
            type = DataTypeList.FEED.title,
            subtype = "Twitter Feed",
            value = this.toString(),
            serviceId = serviceId,
            date = date)
    }
}

data class TweetsPublicMetrics(val retweet_count: Int,
                               val reply_count: Int,
                               val like_count: Int,
                               val quote_count: Int) {
    fun toUserData(serviceId: Long): List<UserData> {
        val unixTime = System.currentTimeMillis()
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = unixTime)
        }
        return userDataList
    }
}

data class TweetsGeo(val coordinates: TweetsGeoCoordinates) {
    fun toUserData(serviceId: Long): List<UserData> {
        val unixTime = System.currentTimeMillis()
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = unixTime)
        }
        return userDataList
    }
}

data class TweetsGeoCoordinates(val type: String,
                                val coordinates: List<Float>) {
    fun toUserData(serviceId: Long): List<UserData> {
        val unixTime = System.currentTimeMillis()
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = unixTime)
        }
        return userDataList
    }
}