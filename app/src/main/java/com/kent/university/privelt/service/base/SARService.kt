package com.kent.university.privelt.service.base

import android.content.Context
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import java.io.InputStream

abstract class SARService {

    protected val _stateFlow = MutableSharedFlow<State>()
    val stateFlow: SharedFlow<State> = _stateFlow

    val name: String
        get() = service.name
    abstract val sarRequestURL: String
    abstract val service: Service

    abstract fun parseSAR(context: Context, inputStream: InputStream): Job

    fun insertOrGetService(context: Context): Long {

        val serviceDataRepository = PriVELTDatabase.getInstance(context)?.serviceDao()

        service.createdAt = System.currentTimeMillis()

        //used to get id
        val service = serviceDataRepository?.getServiceWithName(this.name)
        if (service != null) {
            return service.id
        } else {
            serviceDataRepository?.insertServices(this.service)
            val service = serviceDataRepository?.getServiceWithName(this.name)!!
            return service.id
        }
    }

    protected suspend fun saveUserDataToRepository(context: Context, userDataList: List<UserData>, isLast: Boolean = false) {
        if (isLast && userDataList.isEmpty()) {
            _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
            return
        }
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()
        val serviceId = insertOrGetService(context)
        if (isLast && serviceId == null) {
            _stateFlow.emit(State.Error("Service error"))
            return
        }
        userDataList.forEach {
            if (it.isValid()) {
                it.serviceId = serviceId
                userDataRepository?.insertUserData(it)
            }
        }
        if (isLast)
            _stateFlow.emit(State.Success)
    }

}

sealed class State {
    object Success : State()
    object Loading : State()
    class Error(val error: String) : State()
}
