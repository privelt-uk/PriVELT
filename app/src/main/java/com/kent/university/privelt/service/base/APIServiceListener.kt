package com.kent.university.privelt.service.base

interface APIServiceListener {

    fun loading()

    fun success()

    fun error(error: String)

}
