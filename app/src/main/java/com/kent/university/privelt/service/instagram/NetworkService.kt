package com.kent.university.privelt.service.instagram

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {
    @FormUrlEncoded
    @POST("oauth/access_token")
    fun exchangeCodeForToken(@Field("client_secret") clientSecret: String,
                             @Field("client_id") clientId: String,
                             @Field("grant_type") grantType: String,
                             @Field("redirect_uri") redirectUri: String,
                             @Field("code") code: String): Call<AuthTokenExchangeResponse>

    @GET("v13.0/me")
    fun profile(@Query("fields") fields: String,
                @Query("access_token") accessToken: String): Call<Profile>

    @GET("v13.0/me/media")
    fun media(@Query("fields") fields: String,
              @Query("access_token") accessToken: String): Call<Media>
}

class RetrofitInstance {
    companion object {
        private const val BASE_URL: String = "https://graph.instagram.com"
        private const val BASE_API_URL: String = "https://api.instagram.com"

        fun getGraphRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthTokenExchangeResponse(val access_token: String,
                                     val token_type: String,
                                     val expires_in: Int,
                                     val refresh_token: String,
                                     val scope: String)

data class Profile(val username: String) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val userData = UserData(title = "Instagram Username",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.USERNAME.value,
            value = username,
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        return listOf(userData)
    }
}

data class Media(val username: String) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val userData = UserData(title = "Instagram Media Username",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.USERNAME.value,
            value = username,
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        return listOf(userData)
    }
}
