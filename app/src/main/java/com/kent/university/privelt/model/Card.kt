/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.model

import java.io.Serializable

data class Card(var title: String, var isWatched: Boolean, var isService: Boolean, var metrics: MutableList<CardItem>, var loggedAs: String? = null, val date: Long, val dateSelected: Long) : Serializable {

    fun getCardItemFromTitleAndDate(title: String, date: Long): CardItem? {
        for (i in metrics.indices) {
            if (metrics[i].name == title && date == metrics[i].date)
                return metrics[i]
        }
        return null
    }

}