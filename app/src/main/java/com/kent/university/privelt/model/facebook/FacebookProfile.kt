package com.kent.university.privelt.model.facebook

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class FacebookProfile (
    @SerializedName("id") val id : Long,
    @SerializedName("age_range") val age_range : FacebookAgeRange?,
    @SerializedName("email") val email : String?,
    @SerializedName("first_name") val first_name : String?,
    @SerializedName("last_name") val last_name : String?,
    @SerializedName("location") val location : FacebookLocation?,
    @SerializedName("name") val name : String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "age_range" && it.name == "location"
        }
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Facebook Profile $title",
                type = DataTypeList.PROFILE.title,
                subtype = getSubTypeFromField(field.name),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        val allData = mutableListOf<UserData>()
        allData.addAll(userDataList)
        val ageRangeData = age_range?.toUserData(serviceId, date, parentId)
        val locationData = location?.toUserData(serviceId, date)
        ageRangeData?.let {
            allData.addAll(ageRangeData)
        }
        locationData?.let {
            allData.addAll(locationData)
        }
        return allData
    }
}

private fun getSubTypeFromField(field: String): String {
    return when (field) {
        "first_name" -> DataSubTypeListProfile.NAME.value
        "last_name" -> DataSubTypeListProfile.NAME.value
        "name" -> DataSubTypeListProfile.NAME.value
        "location" -> DataSubTypeListLocation.LOCATION.value
        "age_range" -> DataSubTypeListProfile.AGE_INFO.value
        "email" -> DataSubTypeListProfile.EMAIL.value
        else -> DataSubTypeListProfile.OTHER.value
    }
}