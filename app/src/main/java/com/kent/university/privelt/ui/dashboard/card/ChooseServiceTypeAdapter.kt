/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.dashboard.card

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.databinding.CellPdaBinding
import com.kent.university.privelt.service.base.ServiceController

class ChooseServiceTypeAdapter(
    private val list: List<String>,
    private val listener: (Int) -> Unit
) : RecyclerView.Adapter<ChooseServiceTypeAdapter.AddCardViewHolder>() {

    class AddCardViewHolder(private val binding: CellPdaBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(service: String) {
            binding.title.text = service
            binding.logo.isVisible = false
            if (adapterPosition == 0) {
                binding.serviceType.text = "API"
                binding.serviceType.setBackgroundResource(R.drawable.api_circle)
            }
            else if (adapterPosition == 1) {
                binding.serviceType.text = "SAR"
                binding.serviceType.setBackgroundResource(R.drawable.sar_circle)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddCardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CellPdaBinding.inflate(inflater, parent, false)
        return AddCardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AddCardViewHolder, position: Int) {
        holder.bind(list[position])
        holder.itemView.setOnClickListener { listener(position) }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
