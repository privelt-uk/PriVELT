package com.kent.university.privelt.model.uber

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class RiderEaterSavedLocations(
    val label: String,
    val houseNumber: String,
    val streetName: String,
    val city: String,
    val state: String,
    val postalCode: String,
    val countryCode: String,
    val latitude: String,
    val longitude: String
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Uber rider eater saved locations Data $title",
                type = DataTypeList.LOCATION.title,
                subtype = DataSubTypeListLocation.LOCATION.value,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}