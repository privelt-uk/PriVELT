package com.kent.university.privelt.model.trainline

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TrainlineCustomerDetails(val registrationDate: String?,
                                    val retailerName: String?,
                                    val emailAddress: String?,
                                    val firstName: String?,
                                    val dateOfBirth: String?,
                                    val addressLine1: String?,
                                    val addressLine2: String?,
                                    val addressPostcode: String?,
                                    val addressCountryCode: String?,
                                    val addressCountry: String?) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()
        val initialUserData = UserData(
            title = "Trainline Customer Details",
            type = DataTypeList.PROFILE.title,
            subtype = "Trainline Customer Details",
            value = "",
            serviceId = serviceId,
            date = date
        )
        allData.add(initialUserData)
        val initialLocationUserData = UserData(
            title = "Trainline Customer Location Details",
            type = DataTypeList.LOCATION.title,
            subtype = "Trainline Customer Location Details",
            value = "",
            serviceId = serviceId,
            date = date
        )
        allData.add(initialLocationUserData)

        val registrationDateUserData = UserData(
            title = "Trainline Customer registration date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Registration date: ${registrationDate ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(registrationDateUserData)
        val retailerNameUserData = UserData(
            title = "Trainline Customer retailer name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Registration date: ${retailerName ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(retailerNameUserData)
        val emailAddressUserData = UserData(
            title = "Trainline Customer email",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = emailAddress ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(emailAddressUserData)
        val firstNameUserData = UserData(
            title = "Trainline Customer First Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = firstName ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(firstNameUserData)
        val dateOfBirthUserData = UserData(
            title = "Trainline Customer Date of Birth",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.AGE_INFO.value,
            value = dateOfBirth ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(dateOfBirthUserData)
        val address1UserData = UserData(
            title = "Trainline Customer Address line 1",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = addressLine1 ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id
        )
        allData.add(address1UserData)
        val address2UserData = UserData(
            title = "Trainline Customer Address line 2",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = addressLine2 ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id
        )
        allData.add(address2UserData)
        val postCodeUserData = UserData(
            title = "Trainline Customer Post Code",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = addressPostcode ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id
        )
        allData.add(postCodeUserData)
        val countryUserData = UserData(
            title = "Trainline Customer Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = addressCountry ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id
        )
        allData.add(countryUserData)
        val countryCodeUserData = UserData(
            title = "Trainline Customer Country Code",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = addressCountryCode ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id
        )
        allData.add(countryCodeUserData)
        return allData
    }
}