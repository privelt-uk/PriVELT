package com.kent.university.privelt.model.uber

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListFinancials
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class PaymentMethods(
    val profileType: String,
    val bankOrIssuerName: String,
    val paymentMethodBrand: String,
    val paymentMethodType: String,
    val usedForCollection: String,
    val usedForDisbursements: String,
    val isTheLastVersion: String,
    val notPayable: String,
    val country: String,
    val region: String,
    val deletedAt: String
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Uber payment methods Data $title",
                type = DataTypeList.FINANCIALS.title,
                subtype = DataSubTypeListFinancials.PAYMENT_METHODS.value,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}