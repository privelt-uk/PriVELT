/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.model

import android.content.Context
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.kent.university.privelt.database.PriVELTDatabase
import java.io.Serializable

@Entity(tableName = "service")
open class Service(var name: String, var isPasswordSaved: Boolean, var concatenatedScripts: String, var user: String, var password: String, var createdAt: Long = 0) : Serializable, Cloneable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    val unConcatenatedScripts: Array<String>
        get() = concatenatedScripts.split(DELIMITER).toTypedArray()

    public override fun clone(): Any {
        val service = Service(name, isPasswordSaved, concatenatedScripts, user, password, createdAt)
        service.id = id
        return service
    }

    companion object {
        const val DELIMITER = "@/:-"
    }

    fun insertOrGetService(context: Context): Long {

        val serviceDataRepository = PriVELTDatabase.getInstance(context)?.serviceDao()

        this.createdAt = System.currentTimeMillis()

        //used to get id
        val service = serviceDataRepository?.getServiceWithName(this.name)
        if (service != null) {
            return service.id
        } else {
            serviceDataRepository?.insertServices(this)
            val service = serviceDataRepository?.getServiceWithName(this.name)!!
            return service.id
        }
    }

    fun getServiceRefreshToken(context: Context): String? {
        val serviceDataRepository = PriVELTDatabase.getInstance(context)?.serviceDao()
        val service = serviceDataRepository?.getServiceWithName(this.name)
        return service?.password
    }

    @Ignore
    var dateSelected: Long = 0
}