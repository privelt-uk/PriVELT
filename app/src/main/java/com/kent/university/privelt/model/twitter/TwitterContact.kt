package com.kent.university.privelt.model.twitter

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TwitterContact(val contact: TwitterContactDetails?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        if (contact != null) {
            return contact.toUserData(serviceId, date, parentId)
        }

        return emptyList()
    }
}

data class TwitterContactDetails(val emails: List<String>?,
                                 val phoneNumbers: List<String>?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter Contact Details $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}