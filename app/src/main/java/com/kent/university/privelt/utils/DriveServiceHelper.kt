/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.utils

import android.content.Context
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.api.client.http.FileContent
import com.google.api.services.drive.Drive
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.api.GoogleDriveRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.util.concurrent.Callable
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class DriveServiceHelper(private val mDriveService: Drive) {
    private val mExecutor: Executor = Executors.newSingleThreadExecutor()
    fun getFolderId(path: String): String? {
        var currentFolder: com.google.api.services.drive.model.File? = null
        val pathElements = path.split("/")
        for (pathElement in pathElements) {
            if (pathElement.isNotEmpty()) {
                val query = "mimeType='application/vnd.google-apps.folder' and trashed = false and name='$pathElement'" +
                        if (currentFolder != null) " and '${currentFolder.id}' in parents" else " and 'root' in parents"
                val results = mDriveService.files().list().setQ(query).execute().files
                if (results.isNotEmpty()) {
                    currentFolder = results[0]
                } else {
                    val folder = com.google.api.services.drive.model.File()
                    folder.name = pathElement
                    folder.mimeType = "application/vnd.google-apps.folder"
                    if (currentFolder != null) {
                        folder.parents = listOf(currentFolder.id)
                    }
                    currentFolder = mDriveService.files().create(folder).execute()
                }
            }
        }
        return currentFolder?.id
    }
    fun uploadFile(localFile: File, folderName: String = "PriVELT App"): String {
        val folderId = getFolderId(folderName)

        val fileMetadata = com.google.api.services.drive.model.File()
        fileMetadata.name = localFile.name

        val mediaContent = FileContent("image/jpeg", localFile)

        val query = "'$folderId' in parents and name = '${localFile.name}' and trashed = false"
        val existingFiles = mDriveService.files().list().setQ(query).execute().files

        return if (existingFiles != null && existingFiles.isNotEmpty()) {
            // If the file exists, update it with the new content and set new parent
            val existingFileId = existingFiles[0].id
            fileMetadata.parents = listOf(folderId)
            mDriveService.files().update(existingFileId, null, mediaContent)
                .setFields("id")
                .execute().id
        } else {
            // If the file does not exist, create a new file with the specified parent
            if (folderId != null && folderId.isNotEmpty()) {
                fileMetadata.parents = listOf(folderId)
            }
            mDriveService.files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute().id
        }
    }


    fun downloadFile(path: String): Task<Nothing?> {
        return Tasks.call(mExecutor, Callable {
            val query = "name = 'PriVELT App' and mimeType = 'application/vnd.google-apps.folder' and trashed = false"
            val folderResult = mDriveService.files().list().setQ(query).execute()
            val folderId = folderResult.files[0].id // ID of folder "Privelt App"

            val query2 = "name = 'user_data.zip' and mimeType != 'application/vnd.google-apps.folder' and trashed = false and '$folderId' in parents"
            val fileResult = mDriveService.files().list().setQ(query2).execute()
            val fileDrive = fileResult.files.first() // last one

            if (fileDrive == null) {
                throw FileNotFoundException("File user_data.zip not found in folder Privelt App.")
            }

            val file = File(path + "/" + GoogleDriveRepository.FILE_NAME)
            if (!file.exists()) {
                file.createNewFile() // create if does not exist
            }
            val outputStream = FileOutputStream(file)
            mDriveService.files().get(fileDrive.id).executeMediaAndDownloadTo(outputStream)
            null
        })
    }

    fun deleteFileFromDrive(fileName: String) = CoroutineScope(Dispatchers.IO).async {
        val query = "name = 'PriVELT App' and mimeType = 'application/vnd.google-apps.folder' and trashed = false"
        val folderResult = mDriveService.files().list().setQ(query).execute()
        val folderId = folderResult.files[0].id

        val query2 = "name = '$fileName' and mimeType != 'application/vnd.google-apps.folder' and trashed = false and '$folderId' in parents"
        val fileResult = mDriveService.files().list().setQ(query2).execute()
        val fileDrive = fileResult.files.firstOrNull()
            ?: return@async false

        return@async try {
            mDriveService.files().delete(fileDrive.id).execute()
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

}