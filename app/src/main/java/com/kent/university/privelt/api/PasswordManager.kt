/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.api

import android.content.Context
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import kotlin.experimental.and

class PasswordManager(applicationContext: Context) {

    private val sharedPreferences = applicationContext.getSharedPreferences(
        SHARED_PREFERENCES,
        Context.MODE_PRIVATE
    )

    fun isInvitationCodeAlreadyGiven(): Boolean {
        return sharedPreferences.getBoolean(INVITATION_CODE, false)
    }

    fun setInvitationCode() {
        sharedPreferences.edit().putBoolean(INVITATION_CODE, true).apply()
    }

    fun isPasswordAlreadySet(): Boolean {
        return sharedPreferences.getString(PASSWORD, null) != null
    }

    fun deletePassword() {
        sharedPreferences.edit().remove(PASSWORD).apply()
    }

    fun checkPassword(password: String): Boolean {
        val hash = hashPassword(password)
        val storedHash = sharedPreferences.getString(PASSWORD, null)
        return hash == storedHash
    }

    fun setPassword(password: String) {
        val hash = hashPassword(password)
        sharedPreferences.edit().putString(PASSWORD, hash).apply()
    }

    @Throws(NoSuchAlgorithmException::class)
    fun hashPassword(password: String): String {
        val md: MessageDigest = MessageDigest.getInstance("SHA-512")
        md.reset()
        md.update(password.toByteArray())
        val mdArray: ByteArray = md.digest()
        val sb = StringBuilder(mdArray.size * 2)
        for (b in mdArray) {
            val v: Int = b.toInt() and 0xff
            if (v < 16) sb.append('0')
            sb.append(Integer.toHexString(v))
        }
        return sb.toString()
    }

    companion object {
        const val SHARED_PREFERENCES = "shared_preferences"
        const val PASSWORD = "password"
        const val INVITATION_CODE = "invitation_code"
    }
}