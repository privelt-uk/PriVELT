package com.kent.university.privelt.service.spotify

import android.content.Context
import android.util.Base64
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SpotifyService: Service("Spotify", false, "", "", "") {

    lateinit var APIServiceListener: APIServiceListener
    private val redirectUrl = "privelt://spotify"
    val clientId = "a30b1fb4a39d4cf7b2631e563c561aa7"
    private val clientSecret = "73e93318db0b4b2dbac15eb0b52c3437"
    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(context: Context, token: String) = CoroutineScope(Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }
        systemTime = System.currentTimeMillis()

        val id = insertOrGetService(context)
        getProfile(context, id, token)
    }

    fun connection(context: Context, webView: WebView) {
        openBrowser(webView, context)
    }

    private fun openBrowser(webView: WebView, context: Context) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        val redirectScheme = "privelt"
        val scope = "user-read-private user-read-email playlist-read-private user-library-read user-follow-read"
        val url = "https://accounts.spotify.com/authorize?response_type=code&client_id=$clientId&redirect_uri=$redirectUrl&scope=$scope&state=state"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectScheme)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.getQueryParameter("code")?.let { code ->
                            // Got code!
                            Log.d("OAuth code", code)
                            getToken(code, context)
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            APIServiceListener.error("Authorization code not received :(")

                            val exception = ApiResponseException(
                                "Authorization code not received :(",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    private fun encodeHeader(header: String): String {
        // encode a string using Base64 encoder
        return Base64.encodeToString(header.encodeToByteArray(), Base64.NO_WRAP)
    }

    fun getToken(code: String, context: Context) {
        val retroSpotify = RetrofitSpotifyInstance.getAuthRetrofitInstance()
        val header = encodeHeader("$clientId:$clientSecret")
        retroSpotify.requestToken( "authorization_code", "privelt://spotify", code, "Basic $header")
            .enqueue(object: Callback<AuthRequestTokenResponse> {
                override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                    Log.e("spotify", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                    if (response.code() == 200) {
                        Log.d("spotify","Generated token!")
                        response.body()?.access_token?.let { token ->
                            Log.d("Access token", token)
                            insertServiceAndFetchData(context, token)
                        }
                    } else {
                        Log.d("spotify","Token exchange failed!")
                        val exception = ApiResponseException(
                            "Token exchange failed",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Token exchange failed!")
                    }
                }
            })
    }

    private fun getProfile(context: Context, serviceId: Long, token: String) {
        val retroSpotify = RetrofitSpotifyInstance.getApiRetrofitInstance()
        val bearerToken = "Bearer $token"
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        retroSpotify.requestProfile(bearerToken)
            .enqueue(object: Callback<Profile> {
                override fun onFailure(call: Call<Profile>, t: Throwable) {
                    Log.e("spotify", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                    if (response.code() == 200) {
                        Log.d("spotify","Fetched profile!")
                        response.body()?.let { it ->
                            if (it != null) {
                                val userData = response.body()?.toUserData(serviceId, systemTime) ?: emptyList()
                                //save username
                                CoroutineScope(Dispatchers.IO).launch {
                                    val serviceDataRepository =
                                        PriVELTDatabase.getInstance(context)?.serviceDao()
                                    val service =
                                        serviceDataRepository?.getServiceWithName(this@SpotifyService.name)
                                    service?.user = response.body()?.display_name ?: ""
                                    serviceDataRepository?.updateServices(service)
                                }

                                CoroutineScope(Dispatchers.IO).launch {
                                    userData.forEach {
                                        if (it.isValid())
                                            userDataRepository?.insertUserData(it)
                                    }
                                }

                                APIServiceListener.success()
                            }
                        }
                    }
                    else{
                        Log.d("spotify", response.message())
                        Log.d("spotify","Fetching profile failed!")
                        val exception = ApiResponseException(
                            "Fetching profile failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching profile failed!")
                    }
                }
            })
    }
}