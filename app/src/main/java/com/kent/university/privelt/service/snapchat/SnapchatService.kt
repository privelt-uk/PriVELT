package com.kent.university.privelt.service.snapchat

import android.content.Context
import android.util.Log
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import com.snap.corekit.SnapKit
import com.snap.corekit.controller.LoginStateController
import com.snap.loginkit.LoginResultCallback
import com.snap.loginkit.SnapLoginProvider
import com.snap.loginkit.UserDataQuery
import com.snap.loginkit.UserDataResultCallback
import com.snap.loginkit.exceptions.LoginException
import com.snap.loginkit.exceptions.UserDataException
import com.snap.loginkit.models.UserDataResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SnapchatService: Service("Snapchat", false, "", "", "") {

    lateinit var APIServiceListener: APIServiceListener
    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(context: Context) = CoroutineScope(Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }
        systemTime = System.currentTimeMillis()

        val id = insertOrGetService(context)
        getProfile(context, id)
    }
    fun connection(context: Context) {
        //loginListener(context)

        SnapKit.initSDK(context)
        if (checkLoginState(context)) {
            insertServiceAndFetchData(context)
        } else {
            SnapLoginProvider.get(context).startTokenGrant(object : LoginResultCallback {
                override fun onStart() {

                }

                override fun onFailure(p0: LoginException) {
                }

                override fun onSuccess(p0: String) {
                    insertServiceAndFetchData(context)
                }
            })
        }
    }

    private fun checkLoginState(context: Context): Boolean {
        return SnapLoginProvider.get(context).isUserLoggedIn
    }

    private fun loginListener(context: Context) {
        val mLoginStateChangedListener: LoginStateController.OnLoginStateChangedListener =
            object : LoginStateController.OnLoginStateChangedListener {
                override fun onLoginSucceeded() {
                    // Here you could update UI to show login success
                    insertServiceAndFetchData(context)
                    Log.d("LOGIN", "success")
                }

                override fun onLoginFailed() {
                    // Here you could update UI to show login failure
                    Log.d("LOGIN", "fail")
                    val exception = ApiResponseException(
                        "Login failed",
                        "",
                        500)
                    Firebase.crashlytics.recordException(exception)
                    APIServiceListener.error("Login failed")
                }

                override fun onLogout() {
                    // Here you could update UI to reflect logged out state
                    Log.d("LOGIN", "logout")
                    APIServiceListener.error("Logout")
                }
            }

        //SnapLoginProvider.get(context).addLoginStateCallback(mLoginStateChangedListener)
    }

    private fun getProfile(context: Context, serviceId: Long) {
        // Construct the user data query
        val userDataQuery = UserDataQuery.newBuilder() // optional: for 'displayName' resource
            .withDisplayName() // optional: for 'externalID' resource
            .build()

        // Get the Login Api
        val loginApi = SnapLoginProvider.get(context)

        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        // Call the fetch Api
        loginApi.fetchUserData(userDataQuery, object : UserDataResultCallback {
            override fun onSuccess(userDataResult: UserDataResult) {
                // Handle Success
                if (userDataResult.data == null || userDataResult.data?.meData == null) {
                    APIServiceListener.error("Data is null")
                    return
                }
                val meData = userDataResult.data!!.meData!!

                //save username
                CoroutineScope(Dispatchers.IO).launch {
                    val serviceDataRepository =
                        PriVELTDatabase.getInstance(context)?.serviceDao()
                    val service =
                        serviceDataRepository?.getServiceWithName(this@SnapchatService.name)
                    if (meData.displayName != null) {
                        service?.user = meData.displayName!!
                        serviceDataRepository?.updateServices(service)
                    }
                }

                val userData = SnapChatData.createFromMeData(meData).toUserData(serviceId, systemTime)
                Log.d("DEBUG", userData.toString())
                CoroutineScope(Dispatchers.IO).launch {
                    userData.forEach {
                        if (it.isValid())
                            userDataRepository?.insertUserData(it)
                    }
                }
                APIServiceListener.success()
            }

            override fun onFailure(errorCode: UserDataException) {
                // Handle Failure
                val exception = ApiResponseException(
                    "Login failed",
                    "",
                    500)
                Firebase.crashlytics.recordException(exception)
                APIServiceListener.error("Fetch user data failed")
            }
        })
    }
}