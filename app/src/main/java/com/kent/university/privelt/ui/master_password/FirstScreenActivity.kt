package com.kent.university.privelt.ui.master_password

import android.content.ComponentName
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.os.Bundle
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.databinding.ActivityFirstScreenBinding

class FirstScreenActivity : BaseActivity() {

    override val activityLayout: Int
        get() = R.layout.activity_first_screen

    override fun configureViewModel() {
    }

    override fun configureDesign(savedInstanceState: Bundle?) {
        val binding = ActivityFirstScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        with(binding) {
            readMoreCta.paintFlags = readMoreCta.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            readMoreCta.setOnClickListener {
                val url = "https://privelt.ac.uk/"
                var i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    i = Intent.createChooser(i, null).putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, arrayOf(ComponentName(applicationContext, FirstScreenActivity::class.java)))
                startActivity(i)
            }
            continueCta.setOnClickListener {
                startActivity(Intent(this@FirstScreenActivity, MasterPasswordActivity::class.java))
            }
        }

        val masterPasswordAlreadyGiven = identityManager?.isPasswordAlreadySet()
        if (masterPasswordAlreadyGiven == true)
            startActivity(Intent(this@FirstScreenActivity, MasterPasswordActivity::class.java))
    }

}