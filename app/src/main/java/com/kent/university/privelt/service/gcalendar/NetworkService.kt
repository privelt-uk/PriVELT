package com.kent.university.privelt.service.gcalendar

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {

    @FormUrlEncoded
    @POST("token")
    fun refreshToken(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("refresh_token") refreshToken: String
    ): Call<AuthRequestTokenResponse>

    @FormUrlEncoded
    @POST("token")
    fun requestToken(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("code") code: String,
        @Field("redirect_uri") redirectUri: String,
        @Field("code_verifier") codeVerifier: String
    ): Call<AuthRequestTokenResponse>

    @GET("calendar/v3/users/me/calendarList")
    fun requestCalendarList(
        @Header("Authorization") authorize: String
    ): Call<GoogleCalendarList>

    @GET("calendar/v3/calendars/{calendarId}/events")
    fun requestEventsList(
        @Path("calendarId") calendarId: String,
        @Header("Authorization") authorize: String
    ): Call<GoogleEventsList>
}

class RetrofitGcalendarInstance {
    companion object {
        private const val BASE_AUTH_URL: String = "https://oauth2.googleapis.com/"
        private const val BASE_API_URL: String = "https://www.googleapis.com/"

        fun getAuthRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_AUTH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthRequestTokenResponse(val access_token: String,
                                    val token_type: String,
                                    val id_token: String,
                                    val scope: String,
                                    val expires_in: Int,
                                    val refresh_token: String)

data class GoogleCalendarList(val items: List<GoogleCalendarItem>) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "items"
        }
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(
                title = "Google calendar $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(parentId, title))

        }
        val itemsUserData = items.map { it.toUserData(serviceId, timeStamp, parentId) }.flatten()
        return userDataList + itemsUserData
    }
}

data class GoogleCalendarItem(val id: String) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "defaultReminders" && it.name == "notificationSettings" && it.name == "conferenceProperties"
        }
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(
                title = "Google calendar $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(parentId, title))
        }

        return userDataList
    }
}

data class GoogleEventsList(val items: List<GoogleEventItem>) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "defaultReminders" && it.name == "items"
        }
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Google calendar event $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(parentId, title))
        }
        val itemsUserData = items.map { it.toUserData(serviceId, timeStamp, parentId) }.flatten()

        return userDataList + itemsUserData
    }
}

data class GoogleEventItem(val summary: String,
                           val description: String?,
                           val location: String,
                           val organizer: GoogleEventOrganizer?,
                           val start: GoogleEventDate?,
                           val end: GoogleEventDate?,
                           val visibility: String?,
                           val attendees: List<GoogleEventAttendee>?) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val event = UserData(title = "Google calendar event",
            type = DataTypeList.ACTIVITY.title,
            subtype = "Google calendar event",
            value = "",
            serviceId = serviceId,
            date = timeStamp,
            parentId = parentId)
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filter {
            it.name == "summary" && it.name == "description" && it.name == "location" && it.name == "visibility"
        }
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(
                title = "Google calendar $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(event.id, title)
            )
        }
        val organizerUserData = organizer?.toUserData(serviceId, timeStamp, event.id) ?: emptyList()
        val startUserData = start?.toUserData(serviceId, timeStamp, event.id) ?: emptyList()
        val endUserData = end?.toUserData(serviceId, timeStamp, event.id) ?: emptyList()
        val attendeesUserData = attendees?.map { it.toUserData(serviceId, timeStamp, event.id) }?.flatten() ?: emptyList()

        return (listOf(event) + organizerUserData + startUserData + endUserData + attendeesUserData + userDataList)
    }
}

data class GoogleEventOrganizer(val email: String,
                                val displayName: String?) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Google calendar event organizer $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(parentId, title))
        }
        return userDataList
    }
}

data class GoogleEventDate(val date: String?) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Google calendar event $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(parentId, title))
        }
        return userDataList
    }
}

data class GoogleEventAttendee(val email: String,
                               val self: Boolean) {
    fun toUserData(serviceId: Long, timeStamp: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Google calendar event attendee $title",
                type = getType(title),
                subtype = getSubType(title),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = timeStamp,
                parentId = getParentId(parentId, title))
        }
        return userDataList
    }
}

fun getSubType(value: String): String {
    return when (value) {
        "displayName" -> DataSubTypeListProfile.NAME.value
        "email" -> DataSubTypeListProfile.EMAIL.value
        "self" -> DataSubTypeListProfile.OTHER.value
        "location" -> DataSubTypeListLocation.LOCATION.value
        else -> DataSubTypeListActivity.EVENTS.value
    }
}

fun getType(value: String): String {
    return when (value) {
        "displayName" -> DataTypeList.PROFILE.title
        "email" -> DataTypeList.PROFILE.title
        "self" -> DataTypeList.PROFILE.title
        "location" -> DataTypeList.LOCATION.title
        else -> DataTypeList.ACTIVITY.title
    }
}

fun getParentId(parentId: String, value: String): String {
    return when (value) {
        "displayName" -> GcalendarService.mInitialUserDataProfile!!
        "email" -> GcalendarService.mInitialUserDataProfile!!
        "self" -> GcalendarService.mInitialUserDataProfile!!
        "location" -> GcalendarService.mInitialUserDataLocation!!
        else -> parentId
    }
}