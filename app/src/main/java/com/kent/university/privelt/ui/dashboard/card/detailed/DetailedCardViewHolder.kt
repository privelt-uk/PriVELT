/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.card.detailed

import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.databinding.CellDetailedCardBinding
import com.kent.university.privelt.events.LaunchListDataEvent
import com.kent.university.privelt.model.CardItem
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.utils.sentence.SentenceAdapter
import net.neferett.webviewsextractor.model.UserDataTypes
import org.greenrobot.eventbus.EventBus
import java.util.*

class DetailedCardViewHolder internal constructor(private val binding: CellDetailedCardBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(cardItem: CardItem, isService: Boolean) {
        binding.text1.text = SentenceAdapter.adapt(
            binding.text1.context.resources.getString(R.string.data_found),
            cardItem.number
        )
        itemView.setOnClickListener {
            EventBus.getDefault().post(LaunchListDataEvent(cardItem.name))
        }
        if (!isService) {
            val userDataType = UserDataTypes.getUserDataType(cardItem.name.uppercase(Locale.ROOT))
            if (userDataType != null)
                binding.icon.setImageResource(userDataType.res)
            else
                binding.icon.setImageResource(DataTypeList.getResourceFromTitle(cardItem.name))
        } else {
            val priVELTApplication =
                binding.icon.context.applicationContext as PriVELTApplication
            val res = priVELTApplication.serviceHelper!!.getResIdWithName(cardItem.name)
            res?.let {
                binding.icon.setImageResource(res)
            }
        }
    }

}
