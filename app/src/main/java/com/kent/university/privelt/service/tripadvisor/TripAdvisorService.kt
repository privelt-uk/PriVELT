package com.kent.university.privelt.service.tripadvisor

import android.content.Context
import com.facebook.FacebookSdk.getApplicationContext
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.Tripadvisor.TripadvisorProfileRow
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import com.tom_roush.pdfbox.android.PDFBoxResourceLoader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import com.tom_roush.pdfbox.pdmodel.PDDocument
import com.tom_roush.pdfbox.text.PDFTextStripper

class TripAdvisorService : SARService() {
    override var sarRequestURL: String = "https://privacyportal-cdn.onetrust.com/dsarwebform/da774af3-e615-4e8c-8a28-3d13e98367c5/d294c536-2b05-4be5-b3dc-22f11e5c9d19.html"
    override val service = Service("TripAdvisor", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) = CoroutineScope(Dispatchers.IO).launch {
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            PDFBoxResourceLoader.init(getApplicationContext());
            val document = PDDocument.load(inputStream)
            val stripper = PDFTextStripper()
            val lastPage = document.numberOfPages
            stripper.startPage = lastPage
            stripper.endPage = lastPage
            val text = stripper.getText(document)
            val allLines = text.split("\n").filterNot { it == " " || it == "" }
            if (allLines.contains("Member Details: ")) {
                val lines = allLines.drop(2).toMutableList()
                val subscriptionStartIndex = lines.indexOf("Subscriptions [ ")
                val subscriptionEndIndex = lines.indexOf("] ")
                if (subscriptionStartIndex != -1 && subscriptionEndIndex != -1) {
                    val mergedString = lines.subList(subscriptionStartIndex, subscriptionEndIndex + 1).joinToString(separator = " ")
                    lines.subList(subscriptionStartIndex, subscriptionEndIndex + 1).clear()
                    lines.add(subscriptionStartIndex, mergedString)
                }
                val keyValueList = mutableListOf<TripadvisorProfileRow>()
                for (line in lines) {
                    if (line.contains("Subscriptions")) {
                        val key = "Subscriptions"
                        val splitValues = line.split("Subscriptions [").last()
                        val value = splitValues.replace("[\\[\\]]".toRegex(), "").trim()
                        keyValueList.add(TripadvisorProfileRow(key, value))
                    } else {
                        val keyValue = line.split(" ").filterNot { it == "" }
                        val value = keyValue.last().trim()
                        val key = keyValue.filterNot { it == value }.toString()
                            .replace(",", "")
                            .replace("[", "")
                            .replace("]", "")
                            .trim()
                        keyValueList.add(TripadvisorProfileRow(key, value))
                    }
                }
                document.close()

                val date = System.currentTimeMillis()
                val initialUserData = UserData(title = "TripAdvisor Profile",
                    type = DataTypeList.PROFILE.title,
                    subtype = "TripAdvisor Profile",
                    value = "",
                    serviceId = service.id,
                    date = date)
                val allData = mutableListOf<UserData>()
                val userData = keyValueList.map {
                    it.toUserData(0L, date, initialUserData.id)
                }
                allData.add(initialUserData)
                allData.addAll(userData)
                saveUserDataToRepository(context, allData)
                _stateFlow.emit(State.Success)
            } else {
                document.close()
                _stateFlow.emit(State.Error(error = "The selected file is not a SAR file for this service"))
            }
        }
    }

}
