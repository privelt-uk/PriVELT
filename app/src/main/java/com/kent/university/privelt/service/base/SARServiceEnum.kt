package com.kent.university.privelt.service.base

import com.kent.university.privelt.R
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.service.airbnb.AirbnbService
import com.kent.university.privelt.service.booking.BookingService
import com.kent.university.privelt.service.easyjet.EasyjetService
import com.kent.university.privelt.service.expedia.ExpediaService
import com.kent.university.privelt.service.glocation.GoogleLocationService
import com.kent.university.privelt.service.instagram.InstagramSarService
import com.kent.university.privelt.service.pinterest.PinterestService
import com.kent.university.privelt.service.snapchat.SnapchatSarService
import com.kent.university.privelt.service.stagecoach.StagecoachService
import com.kent.university.privelt.service.tiktok.TikTokSarService
import com.kent.university.privelt.service.trainline.TrainlineService
import com.kent.university.privelt.service.tripadvisor.TripAdvisorService
import com.kent.university.privelt.service.twitter.TwitterServiceSar
import com.kent.university.privelt.service.uber.UberService

enum class SARServiceEnum(
    val serService: SARService,
    override val service: Service,
    override val icon: Int,
    override val privacyURL: String
) : ServiceList {

    AIRBNB(
        AirbnbService(),
        AirbnbService().service,
        R.drawable.airbnb,
        "https://www.airbnb.co.uk/privacy"
    ),
    BOOKING(
        BookingService(),
        BookingService().service,
        R.drawable.booking,
        "https://www.booking.com/content/privacy.en-gb.html"
    ),
    EASYJET(
        EasyjetService(),
        EasyjetService().service,
        R.drawable.ic_easyjet_logo,
        "https://www.easyjet.com/en/policy/privacy"
    ),
    EXPEDIA(
        ExpediaService(),
        ExpediaService().service,
        R.drawable.expedia,
        "https://www.expediagroup.com/privacy-policy/"
    ),
    GOOGLE_LOCATION(
        GoogleLocationService(),
        GoogleLocationService().service,
        R.drawable.google_maps,
        "https://policies.google.com/privacy"
    ),
    SNAPCHAT(
        SnapchatSarService(),
        SnapchatSarService().service,
        R.drawable.snapchat,
        "https://values.snap.com/en-GB/privacy/privacy-policy"
    ),
    INSTAGRAM(
        InstagramSarService(),
        InstagramSarService().service,
        R.drawable.instagram,
        "https://help.instagram.com/155833707900388"
    ),
    PINTEREST(
        PinterestService(),
        PinterestService().service,
        R.drawable.google_maps,
        "https://policy.pinterest.com/en-gb/privacy-policy"
    ),
    STAGECOACH(
        StagecoachService(),
        StagecoachService().service,
        R.drawable.stagecoach,
        "https://www.stagecoachbus.com/privacy-policy"
    ),
    TIKTOK(
        TikTokSarService(),
        TikTokSarService().service,
        R.drawable.tiktok,
        "https://www.tiktok.com/legal/page/us/privacy-policy/en"
    ),
    TRAINLINE(
        TrainlineService(),
        TrainlineService().service,
        R.drawable.trainline,
        "https://www.thetrainline.com/terms/privacy"
    ),
    TRIPADVISOR(
        TripAdvisorService(),
        TripAdvisorService().service,
        R.drawable.tripadvisor,
        "https://www.tripadvisor.co.uk/pages/ftl_fk_privacy_policy.html"
    ),
    TWITTER(
        TwitterServiceSar(),
        TwitterServiceSar().service,
        R.drawable.twitter_x_icon,
        "https://twitter.com/en/privacy"
    ),
    UBER(
        UberService(),
        UberService().service,
        R.drawable.uber,
        "https://www.uber.com/legal/en/document/?country=united-states&lang=en&name=privacy-notice"
    );

}
