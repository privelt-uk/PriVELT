package com.kent.university.privelt.model.facebook

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class FacebookLocation (
    @SerializedName("id") val id : Long?,
    @SerializedName("name") val name : String?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val allData = mutableListOf<UserData>()
        val initialUserData = UserData(title = "Facebook Location",
            type = DataTypeList.LOCATION.title,
            subtype = "Facebook Location",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialUserData)
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Facebook Location $title",
                type = DataTypeList.LOCATION.title,
                subtype = DataSubTypeListLocation.LOCATION.value,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = initialUserData.id)
        }
        allData.addAll(userDataList)
        return allData
    }
}