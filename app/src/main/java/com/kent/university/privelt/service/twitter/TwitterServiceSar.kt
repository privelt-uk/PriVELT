package com.kent.university.privelt.service.twitter

import android.content.Context
import android.util.Log
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.twitter.TwitterAccount
import com.kent.university.privelt.model.twitter.TwitterAgeInfo
import com.kent.university.privelt.model.twitter.TwitterConnectedApps
import com.kent.university.privelt.model.twitter.TwitterContact
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.zip.ZipInputStream

class TwitterServiceSar : SARService() {
    override var sarRequestURL: String = "https://help.twitter.com/en/managing-your-account/accessing-your-twitter-data"
    override val service = Service("X (Twitter)", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            val zipInputStream = ZipInputStream(inputStream)
            val pairs = zipInputStream.use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.endsWith(".js") }
                    .filterNot { it.name.startsWith("__") }
                    .map {
                        Pair(it.name, zipStream.reader().readText())
                    }
                    .toList()
            }
            val gson = Gson()
            val serviceId = 0L
            if (pairs.isEmpty()) {
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
                return@launch
            }
            pairs.map {
                if (it.first.contains("account.js")) {
                    val json = it.second.removePrefix("window.YTD.account.part0 = ")
                    val itemType = object : TypeToken<List<TwitterAccount>>() {}.type
                    val initialUserData = UserData(title = "Twitter Account",
                        type = DataTypeList.PROFILE.title,
                        subtype = "Twitter Account",
                        value = "",
                        serviceId = service.id,
                        date = date)
                    val allData = mutableListOf<UserData>()
                    try {
                        val userData = gson.fromJson<List<TwitterAccount>>(json, itemType)
                            .map { it.toUserData(serviceId, date, initialUserData.id) }.flatten()
                        allData.add(initialUserData)
                        allData.addAll(userData)
                        saveUserDataToRepository(context, allData, true)
                    }
                    catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (it.first.contains("connected-application.js")) {
                    val json = it.second.removePrefix("window.YTD.connected_application.part0 = ")
                    val itemType = object : TypeToken<List<TwitterConnectedApps>>() {}.type
                    val initialUserData = UserData(title = "Twitter Connected Apps",
                        type = DataTypeList.PROFILE.title,
                        subtype = "Twitter Connected Apps",
                        value = "",
                        serviceId = service.id,
                        date = date)
                    val allData = mutableListOf<UserData>()
                    val userData = gson.fromJson<List<TwitterConnectedApps>>(json, itemType).map { it.toUserData(serviceId, date, initialUserData.id) }.flatten()
                    allData.add(initialUserData)
                    allData.addAll(userData)
                    saveUserDataToRepository(context, allData)
                } else if (it.first.contains("ageinfo.js")) {
                    val json = it.second.removePrefix("window.YTD.ageinfo.part0 = ")
                    val itemType = object : TypeToken<List<TwitterAgeInfo>>() {}.type
                    val initialUserData = UserData(title = "Twitter Age Info",
                        type = DataTypeList.PROFILE.title,
                        subtype = "Twitter Age Info",
                        value = "",
                        serviceId = service.id,
                        date = date)
                    val allData = mutableListOf<UserData>()
                    allData.add(initialUserData)
                    val userData = gson.fromJson<List<TwitterAgeInfo>>(json, itemType).map { it.toUserData(serviceId, date, initialUserData.id) }.flatten()
                    allData.addAll(userData)
                    saveUserDataToRepository(context, allData)
                } else if (it.first.contains("contact.js")) {
                    val json = it.second.removePrefix("window.YTD.contact.part0 = ")
                    val itemType = object : TypeToken<List<TwitterContact>>() {}.type
                    val initialUserData = UserData(title = "Twitter Contact",
                        type = DataTypeList.FEED.title,
                        subtype = "Twitter Contact",
                        value = "",
                        serviceId = service.id,
                        date = date)
                    val allData = mutableListOf<UserData>()
                    allData.add(initialUserData)
                    val userData = gson.fromJson<List<TwitterContact>>(json, itemType).map { it.toUserData(serviceId, date, initialUserData.id) }.flatten()
                    allData.addAll(userData)
                    saveUserDataToRepository(context, allData)
                }
            }
        }

}