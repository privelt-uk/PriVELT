package com.kent.university.privelt.ui.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.repositories.UserDataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.Serializable

class DataTreeViewModel(private val app: Application, private val stateHandle: SavedStateHandle): AndroidViewModel(app) {

    companion object {
        private const val STATE_KEY = "state"
    }

    private val initialState: State =
        State(
            isLoading = false,
            data = emptyList()
        )

    val liveDataState: LiveData<State> = stateHandle.getLiveData(STATE_KEY, initialState)

    private var state: State
        get() = liveDataState.value ?: initialState
        private set(value) {
            viewModelScope.launch(Dispatchers.Main) { stateHandle[STATE_KEY] = value }
        }

    data class State(val data: List<UserData>, val isLoading: Boolean): Serializable

    fun fetchUserData(serviceName: String, type: String, parentId: String?) = viewModelScope.launch(Dispatchers.IO) {
        val dataDao = PriVELTDatabase.getInstance(app)?.userDataDao()
        val serviceDao = PriVELTDatabase.getInstance(app)?.serviceDao()


        dataDao?.let {
            val userDataRepository = UserDataRepository(it)
            val serviceId = serviceDao?.getServiceWithName(serviceName)?.id!!
            state = state.copy(isLoading = true)
            userDataRepository.getUserDatasForAServiceAndTypeAndParentId(serviceId, type, parentId).asFlow().collect { data ->
                if (state.isLoading)
                    state = state.copy(data = data, isLoading = false)
            }
        }
    }
}