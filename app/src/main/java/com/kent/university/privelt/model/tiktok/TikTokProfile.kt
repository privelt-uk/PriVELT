package com.kent.university.privelt.model.tiktok

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TikTokProfile(
    @SerializedName("Auto Fill")
    val autoFill: AutoFill?,
    @SerializedName("Profile Information")
    val profileInformation: ProfileInformation?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val autoFillData = autoFill?.toUserData(serviceId, date, parentId) ?: emptyList()
        val profileInfoData = profileInformation?.toUserData(serviceId, date, parentId) ?: emptyList()

        return autoFillData + profileInfoData
    }
}

data class AutoFill(
    @SerializedName("PhoneNumber")
    val phoneNumber: String?,
    @SerializedName("Email")
    val email: String?,
    @SerializedName("FirstName")
    val firstName: String?,
    @SerializedName("LastName")
    val lastName: String?,
    @SerializedName("Address")
    val address: String?,
    @SerializedName("ZipCode")
    val zipCode: String?,
    @SerializedName("Unit")
    val unit: String?,
    @SerializedName("City")
    val city: String?,
    @SerializedName("State")
    val state: String?,
    @SerializedName("Country")
    val country: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "TikTok autofill settings: $title",
                type = getType(field.name),
                subtype = getSubType(field.name),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = TikTok.mUserParentId)
        }
        return userDataList
    }
}

data class ProfileInformation(
    @SerializedName("ProfileMap")
    val profileMap: ProfileMap?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        return profileMap?.toUserData(serviceId, date, parentId) ?: emptyList()
    }
}

data class ProfileMap(
    @SerializedName("PlatformInfo")
    val platformInfo: List<Any>?,
    @SerializedName("bioDescription")
    val bioDescription: String?,
    @SerializedName("birthDate")
    val birthDate: String?,
    @SerializedName("emailAddress")
    val emailAddress: String?,
    @SerializedName("telephoneNumber")
    val telephoneNumber: String?,
    @SerializedName("userName")
    val userName: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "TikTok profile Item: $title",
                type = getType(field.name),
                subtype = getSubType(field.name),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = TikTok.mUserParentId)
        }
        return userDataList
    }
}

private fun getSubType(fieldName: String): String {
    return when {
        fieldName.contains("platformInfo") -> DataSubTypeListProfile.OTHER.value
        fieldName.contains("bioDescription") -> DataSubTypeListProfile.OTHER.value
        fieldName.contains("birthDate") -> DataSubTypeListProfile.AGE_INFO.value
        fieldName.contains("emailAddress") -> DataSubTypeListProfile.EMAIL.value
        fieldName.contains("telephoneNumber") -> DataSubTypeListProfile.MOBILE.value
        fieldName.contains("userName") -> DataSubTypeListProfile.NAME.value
        fieldName.contains("phoneNumber") -> DataSubTypeListProfile.MOBILE.value
        fieldName.contains("email") -> DataSubTypeListProfile.EMAIL.value
        fieldName.contains("firstName") -> DataSubTypeListProfile.NAME.value
        fieldName.contains("lastName") -> DataSubTypeListProfile.NAME.value
        fieldName.contains("address") -> DataSubTypeListProfile.ADDRESS.value
        fieldName.contains("zipCode") -> DataSubTypeListProfile.ADDRESS.value
        fieldName.contains("unit") -> DataSubTypeListProfile.ADDRESS.value
        fieldName.contains("city") -> DataSubTypeListProfile.ADDRESS.value
        fieldName.contains("state") -> DataSubTypeListProfile.ADDRESS.value
        fieldName.contains("country") -> DataSubTypeListProfile.ADDRESS.value
        else -> fieldName.replace("_", " ")
    }
}
private fun getType(fieldName: String): String {
    return when {
        fieldName.contains("platformInfo") -> DataTypeList.PROFILE.title
        fieldName.contains("bioDescription") -> DataTypeList.PROFILE.title
        fieldName.contains("birthDate") -> DataTypeList.PROFILE.title
        fieldName.contains("emailAddress") -> DataTypeList.PROFILE.title
        fieldName.contains("telephoneNumber") -> DataTypeList.PROFILE.title
        fieldName.contains("userName") -> DataTypeList.PROFILE.title
        fieldName.contains("phoneNumber") -> DataTypeList.PROFILE.title
        fieldName.contains("email") -> DataTypeList.PROFILE.title
        fieldName.contains("firstName") -> DataTypeList.PROFILE.title
        fieldName.contains("lastName") -> DataTypeList.PROFILE.title
        fieldName.contains("address") -> DataTypeList.PROFILE.title
        fieldName.contains("zipCode") -> DataTypeList.PROFILE.title
        fieldName.contains("unit") -> DataTypeList.PROFILE.title
        fieldName.contains("city") -> DataTypeList.PROFILE.title
        fieldName.contains("state") -> DataTypeList.PROFILE.title
        fieldName.contains("country") -> DataTypeList.PROFILE.title
        else -> fieldName.replace("_", " ")
    }
}