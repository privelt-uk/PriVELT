package com.kent.university.privelt.utils

import java.util.*

fun capitalize(line: String): String {
    return line.split(' ').joinToString(" ") { it ->
        it.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(
                Locale.getDefault()
            ) else it.toString()
        }
    }.replace(" SAR", "")
}
