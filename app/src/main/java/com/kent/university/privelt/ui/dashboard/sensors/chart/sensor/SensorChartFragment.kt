/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.dashboard.sensors.chart.sensor

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseFragment
import com.kent.university.privelt.databinding.FragmentSensorChartBinding
import com.kent.university.privelt.events.CheckedSensorEvent
import com.kent.university.privelt.model.HistoryPermission
import com.kent.university.privelt.model.Sensor
import com.kent.university.privelt.ui.dashboard.sensors.chart.SensorChartAdapter
import com.kent.university.privelt.utils.sensors.SensorHelper.getNumberOfApplicationInstalled
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.text.SimpleDateFormat
import java.util.*
import java.util.Collections.max
import kotlin.math.ceil
import kotlin.reflect.KMutableProperty

class SensorChartFragment : BaseFragment() {

    private lateinit var binding: FragmentSensorChartBinding

    private var adapter: SensorChartAdapter? = null
    private var listPermissionStatus: List<HistoryPermission>? = null
    private var sensorsWithPosition: List<Sensor>? = null
    private var axisMaximum = 1000000f

    companion object {
        fun newInstance(): SensorChartFragment {
            val fragment = SensorChartFragment()
            return fragment
        }
    }

    private var sensorStatusViewModel: SensorChartViewModel? = null

    override val fragmentLayout: Int
        get() = R.layout.fragment_sensor_chart

    override fun configureViewModel() {
        //Can not use it in this activity
    }

    private fun configureRecyclerView() {
        binding.listSensor.layoutManager = GridLayoutManager(context, 3)
        val listSensors = ArrayList<Sensor>()
        sensorsWithPosition?.forEach {
            listSensors.add(it)
        }
        adapter = SensorChartAdapter(listSensors)
        binding.listSensor.adapter = adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun configureDesign(): View {
        binding = FragmentSensorChartBinding.inflate(layoutInflater)
        val view = binding.root

        sensorsWithPosition = Sensor.values().toList()

        sensorStatusViewModel = getViewModel(SensorChartViewModel::class.java)

        sensorStatusViewModel?.init()

        sensorStatusViewModel?.permissionStatus?.observe(this) { list ->
            if (list.isNotEmpty() && adapter?.getSelectedSensors()!!.isNotEmpty())
                setDataPermission(list, adapter?.getSelectedSensors()!!)
            listPermissionStatus = list
        }

        binding.numberOfApplications.text =
            getString(R.string.number_app, getNumberOfApplicationInstalled(context!!))
        binding.chart.description.isEnabled = false

        // enable touch gestures
        binding.chart.setTouchEnabled(true)

        binding.chart.dragDecelerationFrictionCoef = 0.9f

        // enable scaling and dragging
        binding.chart.isDragEnabled = true
        binding.chart.setScaleEnabled(false)
        binding.chart.setDrawGridBackground(false)
        binding.chart.isHighlightPerDragEnabled = false
        binding.chart.isHighlightFullBarEnabled = true

        binding.chart.xAxis.setDrawLimitLinesBehindData(true)

        // set an alternative background color
        binding.chart.setBackgroundColor(Color.WHITE)
        binding.chart.setViewPortOffsets(0f, 0f, 0f, 0f)

        val xAxis = binding.chart.xAxis
        xAxis.position = XAxis.XAxisPosition.TOP_INSIDE
        xAxis.textSize = 10f
        xAxis.textColor = Color.WHITE
        xAxis.setDrawAxisLine(false)
        xAxis.setDrawGridLines(false)
        xAxis.setDrawLabels(true)
        xAxis.textColor = Color.rgb(0, 0, 0)
        xAxis.granularity = 1f
        xAxis.isGranularityEnabled = true
        xAxis.setCenterAxisLabels(true)
        binding.chart.axisLeft.granularity = 1f
        val leftAxis = binding.chart.axisLeft
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
        leftAxis.textColor = Color.rgb(0, 0, 0)
        leftAxis.setDrawGridLines(true)
        leftAxis.isGranularityEnabled = true
        leftAxis.axisMinimum = 0f
        leftAxis.yOffset = -9f

        val rightAxis = binding.chart.axisRight
        rightAxis.isEnabled = false

        val legend: Legend = binding.chart.legend
        legend.isEnabled = false
        configureRecyclerView()
        return view
    }

    private fun getSensorWithPosition(position: Int, pageSize: Int): List<Sensor> {
        val sensorList = ArrayList<Sensor>()
        val test = Sensor.values().sortedBy { it.title.uppercase(Locale.getDefault()) }
        for ((i, sensor) in test.withIndex()) {
            if (i >= (position * pageSize) && i < ((position + 1) * pageSize)) {
                sensorList.add(sensor)
            }
        }
        return sensorList
    }

    private fun getLimitLineAt(xIndex: Int): LimitLine {
        val ll = LimitLine(xIndex.toFloat()) // set where the line should be drawn
        ll.lineColor = Color.BLACK
        ll.enableDashedLine(10f, 10f, 0f)
        ll.lineWidth = 0.2f
        return ll
    }

    private fun arrayListToPrimitiveArrayPermission(array: List<Float>): FloatArray {
        val res = FloatArray(array.size)
        for ((index, value) in array.withIndex())
            res[index] = value
        return res
    }


    @SuppressLint("SimpleDateFormat")
    private fun getDate(milliSeconds: Long): String? {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat("dd/MM")

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    private fun getMaximumAxisSensors(historyPermissions: List<HistoryPermission>): Int {
        val lists = historyPermissions.groupBy { it.date }
        val maximums = lists.map { it.value.sumOf { it.accountsValue + it.activityrecognitionValue + it.bluetoothValue + it.bodysensorsValue + it.calendarValue + it.cameraValue + it.contactsValue + it.locationValue + it.microValue + it.networkstateValue + it.nfcValue + it.phonestateValue + it.playingcontentValue + it.smsValue + it.storageValue + it.wifiValue }}
        return max(maximums)
    }

    private fun setDataPermission(
        permissionStatus: List<HistoryPermission>,
        permissions: List<String>
    ) {

        val values: ArrayList<BarEntry> = ArrayList()
        val dates: ArrayList<String> = ArrayList()

        for ((i, permissionS) in permissionStatus.withIndex()) {
            val tmpStack = ArrayList<Float>()
            for (sensor in sensorsWithPosition!!) {
                if (permissions.contains(sensor.title)) {

                    // Reflection to call each getter avoiding if forest

                    val kClass = Class.forName(permissionS.javaClass.name).kotlin

                    //Remove space and uppercase letters
                    val value = kClass.members.filterIsInstance<KMutableProperty<*>>().firstOrNull {
                        it.name == sensor.title.lowercase(Locale.ROOT).replace(" ", "").replace("-", "") + "Value"
                    }
                    val sensorValue = kClass.members.filterIsInstance<KMutableProperty<*>>()
                        .firstOrNull {
                            it.name == sensor.title.lowercase(Locale.ROOT)
                                .replace(" ", "").replace("-", "") + "Sensor"
                        }

                    val res =
                        (value?.getter?.call(permissionS) as Int) + if (sensor.isSensor && !(sensorValue?.getter?.call(
                                permissionS
                            ) as Boolean)
                        ) 0.5f else 0f
                    tmpStack.add(res)
                }
            }
            val b = BarEntry((i).toFloat(), arrayListToPrimitiveArrayPermission(tmpStack))
            dates.add(getDate(permissionS.date)!!)
            binding.chart.xAxis?.addLimitLine(getLimitLineAt(i))

            values.add(b)
        }


        axisMaximum = getMaximumAxisSensors(permissionStatus).toFloat()
        binding.chart.axisLeft?.axisMaximum = axisMaximum

        val set1 = BarDataSet(values, "Activated sensors")
        set1.setDrawIcons(false)

        val colors = ArrayList<Int>()
        for (sensor in sensorsWithPosition!!)
            if (permissions.contains(sensor.title)) {
                colors.add(ResourcesCompat.getColor(resources, sensor.color, null))
            }
        set1.colors = colors

        drawChart(set1, dates)
    }

    private fun drawChart(set1: IBarDataSet, dates: ArrayList<String>) {
        val dataSets: ArrayList<IBarDataSet> = ArrayList()
        dataSets.add(set1)
        val data = BarData(dataSets)
        binding.chart.data = data
        binding.chart.data?.setValueTextColor(Color.WHITE)
        binding.chart.data?.setDrawValues(true)
        binding.chart.data?.setValueTextSize(12f)
        binding.chart.setDrawValueAboveBar(false)
        binding.chart.data?.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value < (axisMaximum*0.01))
                     ""
                else value.toInt().toString()
            }
        })

        binding.chart.setFitBars(true)
        binding.chart.xAxis?.setLabelCount(7, false)
        binding.chart.moveViewToX(set1.entryCount.toFloat())
        binding.chart.xAxis?.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value.toInt() + 1 < dates.size) dates[value.toInt() + 1] else ""
            }
        }

        binding.chart.data?.isHighlightEnabled = false
        binding.chart.invalidate()
        binding.chart.setVisibleXRangeMaximum(7f)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onCheckedSensor(event: CheckedSensorEvent) {
        if (adapter?.getSelectedSensors()!!.isNotEmpty())
            setDataPermission(listPermissionStatus!!, adapter?.getSelectedSensors()!!)
    }

}
