package com.kent.university.privelt.service.glocation

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.zip.ZipInputStream

class GoogleLocationService : SARService() {

    override val sarRequestURL: String = "https://takeout.google.com/"
    override val service: Service = Service("Google Location", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            val zipInputStream = ZipInputStream(inputStream)
            val locationHistories = zipInputStream.use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.endsWith(".json") }
                    .mapIndexed { index, _ ->
                        if (index < 10) zipStream.reader().readText()
                        else ""
                    }
                    .toList()
            }
            val allData = mutableListOf<UserData>()
            val initialUserData = UserData(title = "Google Location History",
                type = DataTypeList.LOCATION.title,
                subtype = "Google Location History",
                value = "",
                serviceId = 0L,
                date = date)
            allData.add(initialUserData)
            val userDataList = locationHistories.map { ObjectMapper().readTree(it) }
                .flatMap { it.findValuesAsText("name") }
                .distinct()
                .map {
                    UserData(title = "Google location $it",
                            type = DataTypeList.LOCATION.title,
                            subtype = DataSubTypeListLocation.LOCATION.value,
                            value = it,
                            date = date,
                            serviceId = 0L,
                            parentId = initialUserData.id)
                    }
            allData.addAll(userDataList)
            saveUserDataToRepository(context, allData, true)
        }

}
