package com.kent.university.privelt.ui.service

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.databinding.ActivityApiServiceBinding
import com.kent.university.privelt.service.base.APIServiceEnum
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.ServiceController
import com.kent.university.privelt.service.facebook.FacebookService
import com.kent.university.privelt.service.fitbit.FitbitService
import com.kent.university.privelt.service.flickr.FlickrService
import com.kent.university.privelt.service.gcalendar.GcalendarService
import com.kent.university.privelt.service.instagram.InstagramService
import com.kent.university.privelt.service.snapchat.SnapchatService
import com.kent.university.privelt.service.spotify.SpotifyService
import com.kent.university.privelt.utils.PriVELTLog
import com.kent.university.privelt.utils.getSerializableIntent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class APIServiceActivity : AppCompatActivity(), APIServiceListener {

    private lateinit var binding: ActivityApiServiceBinding

    private var callbackManager = CallbackManager.Factory.create()

    lateinit var service: APIServiceEnum

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityApiServiceBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        service = intent.getSerializableIntent(ServiceController.PARAM_SERVICE, APIServiceEnum::class.java)
        PriVELTLog.BUTTON_PRESSED("User tries to extract data from API service ${service.service.name}")
        when (service) {
            APIServiceEnum.INSTAGRAM -> {
                val instagramService = service.service as InstagramService
                instagramService.APIServiceListener = this
                instagramService.connection(binding.webView)
                binding.webView.isVisible = true
            }
            APIServiceEnum.FACEBOOK -> {
                FacebookSdk.sdkInitialize(applicationContext)
                val facebookService = service.service as FacebookService
                facebookService.APIServiceListener = this
                facebookService.connection(callbackManager, binding.loginButton)
                binding.facebookGroup.isVisible = true
            }
            APIServiceEnum.SNAPCHAT -> {
                val snapchatService = service.service as SnapchatService
                snapchatService.APIServiceListener = this
                snapchatService.connection(this)
            }
            APIServiceEnum.SPOTIFY -> {
                val spotifyService = service.service as SpotifyService
                spotifyService.APIServiceListener = this
                spotifyService.connection(this, binding.webView)
            }
            APIServiceEnum.FITBIT -> {
                val fitbitService = service.service as FitbitService
                fitbitService.APIServiceListener = this
                fitbitService.connection(this, binding.webView)
            }
            APIServiceEnum.GOOGLECALENDAR -> {
                val gCalendarService = service.service as GcalendarService
                gCalendarService.APIServiceListener = this
                gCalendarService.connection(this, binding.webView)
            }
            APIServiceEnum.FLICKR -> {
                val gCalendarService = service.service as FlickrService
                gCalendarService.APIServiceListener = this
                gCalendarService.connection(this, binding.webView)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun loading() {
        binding.webView.isVisible = false
        binding.facebookGroup.isVisible = false
        binding.loading.isVisible = true
    }

    override fun success() {
        PriVELTLog.ACTION_RESULT("API serivce ${service.service.name} extraction success")
        CoroutineScope(Dispatchers.IO).launch {
            val serviceDataRepository =
                PriVELTDatabase.getInstance(this@APIServiceActivity)?.serviceDao()

            val userDataRepository =
                PriVELTDatabase.getInstance(this@APIServiceActivity)?.userDataDao()

            val service = serviceDataRepository?.getServiceWithName(service.service.name)

            if (false && service != null) {
                val userData = userDataRepository?.getUserDataForAService(service.id)
                if (userData.isNullOrEmpty())
                    serviceDataRepository.deleteServices(service)
            }
            CoroutineScope(Dispatchers.Main).launch {
                Toast.makeText(this@APIServiceActivity, "The data has been extracted from this service", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    override fun error(error: String) {
        PriVELTLog.ACTION_RESULT("API serivce ${service.service.name} extraction error ($error)")
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(this@APIServiceActivity, error, Toast.LENGTH_LONG).show()
            finish()
        }
    }

}
