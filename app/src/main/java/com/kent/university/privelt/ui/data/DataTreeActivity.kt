package com.kent.university.privelt.ui.data

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.isVisible
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityDataTreeBinding
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.ui.login.LoginActivity
import com.unnamed.b.atv.model.TreeNode
import com.unnamed.b.atv.view.AndroidTreeView

class DataTreeActivity : BaseActivity() {

    private lateinit var binding: ActivityDataTreeBinding
    override val activityLayout: Int
        get() = R.layout.activity_data_tree

    private val viewModel: DataTreeViewModel by viewModels()

    override fun configureViewModel() {
    }

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityDataTreeBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun configureTreView(data: List<UserData>) {
        val root: TreeNode = TreeNode.root()
        val parents = data.filter { it.parentId == null }

        var i = 0
        var j = 0
        parents.forEach { parent ->
            val parentNode = TreeNode(parent).setViewHolder(HeaderHolder(this, parents.areItemsTheSame(), i))
            val childrenNodes = data.filter { it.parentId == parent.id }
            childrenNodes.forEach { children ->
                if (data.any { it.parentId == children.id}) { //hasChildren
                    val node = TreeNode(children).setViewHolder(SubHeaderHolder(this, parents.areItemsTheSame(), j))
                    j++
                    parentNode.addChildren(node)
                    val childrenOfChildren = data.filter { it.parentId == children.id }
                    childrenOfChildren.forEach { child ->
                        val childNode = TreeNode(child).setViewHolder(DataTreeViewHolder(this, ::handleClick))
                        node.addChildren(childNode)
                    }
                } else {
                    val childNode = TreeNode(children).setViewHolder(DataTreeViewHolder(this, ::handleClick))
                    parentNode.addChildren(childNode)
                }
            }

            if (childrenNodes.isNotEmpty()) {
                root.addChildren(parentNode)
                i++
            }
        }
        val tView = AndroidTreeView(this, root)
        binding.containerView.addView(tView.view)
    }

    private fun handleClick(userDataLongClicked: UserData, view: View) {
        val popupMenu = PopupMenu(this@DataTreeActivity, view)

        popupMenu.menuInflater.inflate(R.menu.user_data_long_click_menu, popupMenu.getMenu())
        popupMenu.setOnMenuItemClickListener {
            if (it.itemId == R.id.copy) {
                val text = if (userDataLongClicked.value.isNotEmpty())
                    "${userDataLongClicked.subtype}\n${userDataLongClicked.value}"
                else
                    userDataLongClicked.subtype
                val clipboard: ClipboardManager =
                    getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip: ClipData = ClipData.newPlainText("PriVELT", text)
                clipboard.setPrimaryClip(clip)

            }
            true
        }
        popupMenu.show()
    }

    private fun List<UserData>.areItemsTheSame(): Boolean {
        return !this.any { it.subtype != this[0].subtype } && this.size > 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val serviceName = intent.getStringExtra(LoginActivity.PARAM_SERVICE)
        val type = intent.getStringExtra(DataActivity.PARAM_TYPE)
        viewModel.fetchUserData(serviceName!!, type!!, null)

        viewModel.liveDataState.observe(this) {
            binding.loader.isVisible = it.isLoading

            configureTreView(it.data)
        }
    }
}