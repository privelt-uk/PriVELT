package com.kent.university.privelt.model.trainline

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListFinancials
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TrainlinePayments(val amount: String?,
                             val currencyCode: String?,
                             val paymentMethod: String?,
                             val cardType: String?,
                             val billingName: String?,
                             val expiryMonth: String?,
                             val expiryYear: String?) {

    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()
        val initialUserData = UserData(title = "Trainline Payments",
            type = DataTypeList.FINANCIALS.title,
            subtype = "Trainline Payments",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialUserData)
        val initialProfileUserData = UserData(title = "Trainline Payments Profile Details",
            type = DataTypeList.PROFILE.title,
            subtype = "Trainline Payments Profile Details",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(initialProfileUserData)

        val amountUserData = UserData(
            title = "Trainline Payments Amount",
            type = DataTypeList.FINANCIALS.title,
            subtype = DataSubTypeListFinancials.PAYMENTS.value,
            value = amount ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(amountUserData)
        val currencyCodeUserData = UserData(
            title = "Trainline Payments Currency Code",
            type = DataTypeList.FINANCIALS.title,
            subtype = DataSubTypeListFinancials.PAYMENTS.value,
            value = currencyCode ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(currencyCodeUserData)
        val paymentMethodUserData = UserData(
            title = "Trainline Payments Payment Method",
            type = DataTypeList.FINANCIALS.title,
            subtype = DataSubTypeListFinancials.PAYMENT_METHODS.value,
            value = paymentMethod ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(paymentMethodUserData)
        val cardTypeUserData = UserData(
            title = "Trainline Payments Card Type",
            type = DataTypeList.FINANCIALS.title,
            subtype = DataSubTypeListFinancials.PAYMENT_METHODS.value,
            value = cardType ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(cardTypeUserData)
        val billingNameUserData = UserData(
            title = "Trainline Payments Billing Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = billingName ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialProfileUserData.id
        )
        allData.add(billingNameUserData)
        val expiryMonthUserData = UserData(
            title = "Trainline Payments Expiry Month",
            type = DataTypeList.FINANCIALS.title,
            subtype = DataSubTypeListFinancials.PAYMENT_METHODS.value,
            value = expiryMonth ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(expiryMonthUserData)
        val expiryYearUserData = UserData(
            title = "Trainline Payments Expiry Month",
            type = DataTypeList.FINANCIALS.title,
            subtype = DataSubTypeListFinancials.PAYMENT_METHODS.value,
            value = expiryYear ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id
        )
        allData.add(expiryYearUserData)
        return allData
    }
}
