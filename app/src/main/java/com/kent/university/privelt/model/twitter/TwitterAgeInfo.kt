package com.kent.university.privelt.model.twitter

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TwitterAgeInfo(val ageMeta: TwitterAgeInfoDetails?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        if (ageMeta != null) {
            return ageMeta.toUserData(serviceId, date, parentId)
        }

        return emptyList()
    }
}

data class TwitterAgeInfoDetails(val ageInfo: TwitterAge?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        if (ageInfo != null) {
            return ageInfo.toUserData(serviceId, date, parentId)
        }

        return emptyList()
    }
}

data class TwitterAge(val age: List<String>?,
                      val birthDate: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter $title",
                type = DataTypeList.PROFILE.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}