package com.kent.university.privelt.service.base

import androidx.annotation.DrawableRes
import com.kent.university.privelt.R

enum class DataTypeList(val title: String, @DrawableRes val res: Int) {

    //deprecated
    FEED("feed", R.drawable.ic_feed),
    MEDIA("media", R.drawable.ic_media),
    MUSIC("music", R.drawable.ic_music),
    RESERVATION("reservation", R.drawable.reservation),

    PROFILE("profile", R.drawable.ic_profile),
    LOCATION("location", R.drawable.ic_map_location),
    FINANCIALS("financials", R.drawable.ic_money),
    DEVICE("device", R.drawable.ic_phone),
    ACTIVITY("activity", R.drawable.reservation),
    OTHER("other", R.drawable.ic_add);

    companion object {
        fun getResourceFromTitle(title: String): Int {
            values().forEach {
                if (it.title == title)
                    return it.res
            }
            return 0
        }
    }

}
