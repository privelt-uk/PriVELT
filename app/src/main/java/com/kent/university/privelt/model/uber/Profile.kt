package com.kent.university.privelt.model.uber

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class Profile(
    val firstTime: String,
    val lastName: String,
    val email: String,
    val mobile: String,
    val rating: String,
    val userType: String,
    val country: String,
    val firstPaymentMethodAdded: String
){
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Uber profile Data $title",
                type = DataTypeList.PROFILE.title,
                subtype = getProfileSubType(field.name),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }

    private fun getProfileSubType(fieldName: String): String {
        return when {
            fieldName.contains("lastName") -> DataSubTypeListProfile.NAME.value
            fieldName.contains("email") -> DataSubTypeListProfile.EMAIL.value
            fieldName.contains("mobile") -> DataSubTypeListProfile.MOBILE.value
            fieldName.contains("country") -> DataSubTypeListProfile.ADDRESS.value
            else -> fieldName.replace("_", " ")
        }
    }
}