package com.kent.university.privelt.model.Airbnb

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList

data class AirbnbProfileData (
    var userCountry : String,
    var userBirthday : String,
    var userInitialIp : String,
    var userLanguages : String,
    var userSex : String,
    var userTosVersion : String,
    var userTosAcceptedAt : String,
    var userCreatedAt : String,
    var userFirstName : String,
    var userLastName : String,
    var userNativeCurrency : String,
    var userEmail : String,
    var serProfileInfoHometown : String,
    var userProfileInfoUniversity : String,
    var userProfileInfoEmployer : String,
    var userPreferredLocale : String,
    var userUpdatedAt : String,
    var phoneNumber : String
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val profileUserData = UserData(title = "Airbnb Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Airbnb Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(profileUserData)

        val locationUserData = UserData(title = "Airbnb Locations",
            type = DataTypeList.LOCATION.title,
            subtype = "Airbnb Locations",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(locationUserData)

        val deviceUserData = UserData(title = "Airbnb Devices",
            type = DataTypeList.DEVICE.title,
            subtype = "Airbnb Devices",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(deviceUserData)

        val countryEntry = UserData(title = "Airbnb profile Data user country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = userCountry,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(countryEntry)

        val birthdayEntry = UserData(title = "Airbnb profile Data user birthday",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.AGE_INFO.value,
            value = userBirthday,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(birthdayEntry)

        val ipEntry = UserData(title = "Airbnb profile Data user initial IP address",
            type = DataTypeList.DEVICE.title,
            subtype = DataSubTypeListDevice.IP.value,
            value = userInitialIp,
            serviceId = serviceId,
            date = date,
            parentId = deviceUserData.id)
        allData.add(ipEntry)

        val languagesEntry = UserData(title = "Airbnb profile Data user languages",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user language: $userLanguages",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(languagesEntry)

        val sexEntry = UserData(title = "Airbnb profile Data user sex",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.GENDER.value,
            value = userSex,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(sexEntry)

        val tosVersionEntry = UserData(title = "Airbnb profile Data user tos version",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user TOS version: $userTosVersion",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(tosVersionEntry)

        val tosAcceptedAtEntry = UserData(title = "Airbnb profile Data user tos accepted date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user TOS accepted at: $userTosAcceptedAt",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(tosAcceptedAtEntry)

        val createdEntry = UserData(title = "Airbnb profile Data user created date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user created at: $userCreatedAt",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(createdEntry)

        val universityEntry = UserData(title = "Airbnb profile Data user university",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user university info: $userProfileInfoUniversity",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(universityEntry)

        val employerEntry = UserData(title = "Airbnb profile Data user employer",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.JOB_TITLE.value,
            value = userProfileInfoEmployer,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(employerEntry)

        val localeEntry = UserData(title = "Airbnb profile Data user locale",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = userPreferredLocale,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(localeEntry)

        val emailEntry = UserData(title = "Airbnb profile Data user email",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = userEmail,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(emailEntry)

        val hometownEntry = UserData(title = "Airbnb profile Data user hometown",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = serProfileInfoHometown,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(hometownEntry)

        val firstNameEntry = UserData(title = "Airbnb profile Data user first name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = userFirstName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(firstNameEntry)

        val lastNameEntry = UserData(title = "Airbnb profile Data user last name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = userLastName,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(lastNameEntry)

        val currencyEntry = UserData(title = "Airbnb profile Data user native currency",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user currency: $userNativeCurrency",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(currencyEntry)

        val updatedEntry = UserData(title = "Airbnb profile Data user last updated date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user updated at: $userUpdatedAt",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(updatedEntry)

        val phoneNumberEntry = UserData(title = "Airbnb profile Data user phone number",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.MOBILE.value,
            value = phoneNumber,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(phoneNumberEntry)
        return allData
    }
}