package com.kent.university.privelt.model.uber

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class RiderTripsData(
    val city: String,
    val tripOrOderStatus: String,
    val beginTripLat: String,
    val beginTripLng: String,
    val beginTripAddress: String,
    val dropOffLat: String,
    val dropOffLng: String,
    val dropOffAddress: String
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Uber trips Data $title",
                type = DataTypeList.ACTIVITY.title,
                subtype = DataSubTypeListActivity.BOOKING.value,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}