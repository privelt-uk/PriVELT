package com.kent.university.privelt.service.easyjet

import android.content.Context
import com.facebook.FacebookSdk
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.easyjet.EasyjetBooking
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import com.tom_roush.pdfbox.android.PDFBoxResourceLoader
import com.tom_roush.pdfbox.pdmodel.PDDocument
import com.tom_roush.pdfbox.text.PDFTextStripper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream

class EasyjetService: SARService() {
    override var sarRequestURL: String = "https://www.easyjet.com/en/policy/privacy-promise/request-data-form"
    override val service = Service("Easyjet", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) = CoroutineScope(Dispatchers.IO).launch {
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            PDFBoxResourceLoader.init(FacebookSdk.getApplicationContext());
            val document = PDDocument.load(inputStream)
            val stripper = PDFTextStripper()
            val lastPage = document.numberOfPages
            stripper.startPage = 6
            stripper.endPage = lastPage
            val text = stripper.getText(document)
            val allLines = text.split("\n")
                .filterNot { it == " " || it == "  " || it == "" || it.startsWith("Page ")}
                .filterNot { it.contains("This document") || it.contains("may be additional bookings")}
                .filterNot { it.contains("Also, the bookings") || it.contains("information does not fall")}
                .filterNot { it.contains("For security reasons") || it.contains("longer available the")}

            val indices: MutableList<Int> = ArrayList()
            val items: MutableList<List<String>> = ArrayList()

            allLines.forEachIndexed { index, value ->
                if (value.contains("Booked On :")) {
                    indices.add(index)
                }
            }
            var lastIndex: Int = 0
            indices.drop(1).forEach {
                items.add(allLines.subList(lastIndex, it))
                lastIndex = it
            }
            val date = System.currentTimeMillis()

            val allData = mutableListOf<UserData>()
            items.forEach {
                val userData = UserData(title = "EasyJet Booking",
                    type = DataTypeList.RESERVATION.title,
                    subtype = "EasyJet Booking",
                    value = "",
                    serviceId = service.id,
                    date = date)

                allData.add(userData)
                val endIndexPassengerList: Int = it.indexOf("Flight Details :  ") - 1
                val endIndexFlightDetailsList: Int = it.indexOf("Payments:  ") - 1
                val endIndexPaymentsList: Int = it.indexOf("Booker Details: ") - 1
                val postalCodeIndex: Int = getPostalCodeIndex(it)
                val startIndexCheckInDetailsList: Int = it.indexOf("Check In/Boarding Details :  ") + 1
                val endIndexCheckInDetailsList: Int = it.lastIndex

                val booking = EasyjetBooking(
                    it[0].replace("Booking Booked On : ", ""),
                    it[1].replace("Date of Booking : ", ""),
                    it[2].replace("Booking Reference : ", ""),
                    it[3].replace("Booked By : ", ""),
                    it.subList(5, endIndexPassengerList),
                    it.subList(endIndexPassengerList + 2, endIndexFlightDetailsList),
                    it.subList(endIndexFlightDetailsList + 2, endIndexPaymentsList),
                    it[endIndexPaymentsList + 2].replace("Mailing Name : ", ""),
                    getFullAddress(it),
                    it[postalCodeIndex].replace("Postal Code : ", ""),
                    it[postalCodeIndex + 1].replace("City/Country : ", ""),
                    it[postalCodeIndex + 2].replace("Country : ", ""),
                    it[postalCodeIndex + 3].replace("Email : ", ""),
                    it.subList(startIndexCheckInDetailsList, endIndexCheckInDetailsList)
                )
                val bookings: MutableList<EasyjetBooking> = ArrayList()
                bookings.add(booking)
                allData.addAll(bookings.map {
                    it.toUserData(0L, date, userData.id)
                }.flatten())
            }
            document.close()

            saveUserDataToRepository(context, allData, true)
        }
    }

    private fun getFullAddress(list: List<String>): String {
        val address1Index: Int = list.indexOfFirst {
            it.startsWith("Address 1")
        }
        val address2Index: Int = list.indexOfFirst {
            it.startsWith("Address 2")
        }
        return if (address2Index != -1) {
            val address1: String = list[address1Index].replace("Address 1 : ", "")
            val address2: String = list[address2Index].replace("Address 2 : ", "")
            "$address1 $address2"
        } else {
            list[address1Index].replace("Address 1 : ", "")
        }
    }

    private fun getPostalCodeIndex(list: List<String>): Int {
        return list.indexOfFirst {
            it.startsWith("Postal Code")
        }
    }
}