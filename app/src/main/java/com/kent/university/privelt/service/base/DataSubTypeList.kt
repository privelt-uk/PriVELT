package com.kent.university.privelt.service.base

interface DataSubtypeList {
    val value: String
}

enum class DataSubTypeListProfile(override val value: String): DataSubtypeList {
    TITLE("title"),
    NAME("name"),
    ID("id"),
    EMAIL("email"),
    OTHER("other"),
    MOBILE("Mobile"),
    ADDRESS("Address"),
    AGE_INFO("Age Info"),
    GENDER("Gender"),
    USERNAME("Username"),
    WEBSITE("Website"),
    COMPANY("Company"),
    JOB_TITLE("Job Title"),
    HEIGHT("Height"),
    WEIGHT("Weight"),
    NUMBER_OF_KIDS("Number of Kids"),
    MARRIAGE_STATUS("Marriage Status"),
    PREFERENCES("Preferences"),
    LOGIN_HISTORY("Login History"),
    BROWSING_HISTORY("Browsing History"),
    CONNECTED_APPS("Connected Apps"),
    CONNECTED_ACCOUNTS("Connected Accounts")
}

enum class DataSubTypeListLocation(override val value: String): DataSubtypeList {
    LOCATION("location")
}

enum class DataSubTypeListFinancials(override val value: String): DataSubtypeList {
    PAYMENT_METHODS("Payment Methods"),
    PAYMENTS("Payments"),
    REFUNDS("Refunds")
}

enum class DataSubTypeListDevice(override val value: String): DataSubtypeList {
    IP("ip"),
    MAC("mac"),
    DEVICE_TYPE("device_type"),
    OS("os")
}

enum class DataSubTypeListActivity(override val value: String): DataSubtypeList {
    BOOKING("booking"),
    FOOD("food"),
    EVENTS("events")
}