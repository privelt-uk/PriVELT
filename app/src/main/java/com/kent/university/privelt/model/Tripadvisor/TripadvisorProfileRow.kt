package com.kent.university.privelt.model.Tripadvisor

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList

data class TripadvisorProfileRow(val key: String, val value: String) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): UserData {
        return UserData(title = "Tripadvisor profile Data $key",
            type = DataTypeList.PROFILE.title,
            subtype = getSubtype(),
            value = value,
            serviceId = serviceId,
            date = date,
            parentId = parentId)
    }

    private fun getSubtype(): String {
        return when {
            key.contains("Date") -> DataSubTypeListProfile.LOGIN_HISTORY.value
            key.contains("Google") -> DataSubTypeListProfile.CONNECTED_ACCOUNTS.value
            key.contains("Facebook") -> DataSubTypeListProfile.CONNECTED_ACCOUNTS.value
            key.contains("Email") -> DataSubTypeListProfile.EMAIL.value
            key.contains("Locale") -> DataSubTypeListProfile.ADDRESS.value
            key.contains("Name") -> DataSubTypeListProfile.NAME.value
            else -> key
        }
    }
}