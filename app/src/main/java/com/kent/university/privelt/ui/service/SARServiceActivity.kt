package com.kent.university.privelt.ui.service

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.kent.university.privelt.databinding.ActivitySarServiceBinding
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.SARServiceEnum
import com.kent.university.privelt.service.base.ServiceController
import com.kent.university.privelt.service.base.State
import com.kent.university.privelt.utils.PriVELTLog
import com.kent.university.privelt.utils.getSerializableIntent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SARServiceActivity : AppCompatActivity() {

    lateinit var binding: ActivitySarServiceBinding

    private lateinit var sarService: SARService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySarServiceBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val sarServiceEnum = intent.getSerializableIntent(
            ServiceController.PARAM_SERVICE,
            SARServiceEnum::class.java
        )
        sarService = sarServiceEnum.serService
        PriVELTLog.BUTTON_PRESSED("User tries to extract data from SAR service ${sarService.service.name}")
        binding.sarServiceName.text = sarService.name
        binding.sarServiceLogo.setImageResource(sarServiceEnum.icon)
        binding.sarInformationButton.setOnClickListener {
            startSarInformationActivity(sarServiceEnum)
        }
        binding.sarServiceRequestButton.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(sarService.sarRequestURL)))
        }
        observeServiceState(sarService)
        binding.sarServiceUploadButton.setOnClickListener { openSARFile() }
    }

    private fun observeServiceState(sarService: SARService) =
        CoroutineScope(Dispatchers.Main).launch {
            sarService.stateFlow.collect { state ->
                when (state) {
                    is State.Error -> {
                        PriVELTLog.ACTION_RESULT("SAR serivce ${sarService.service.name} extraction error ($state.error)")
                        Toast.makeText(
                            this@SARServiceActivity,
                            state.error,
                            Toast.LENGTH_LONG
                         ).show()
                        finish()
                    }
                    State.Loading -> binding.loading.isVisible = true
                    State.Success -> {
                        PriVELTLog.ACTION_RESULT("SAR serivce ${sarService.service.name} extraction success")
                        Toast.makeText(this@SARServiceActivity, "The data has been extracted from this service", Toast.LENGTH_LONG).show()
                        finish()
                    }
                }
            }
        }

    private fun startSarInformationActivity(sarServiceEnum: SARServiceEnum) {
        val intent = Intent(this, SARInformationActivity::class.java)
        val identifierName = sarServiceEnum.service.name.replace(" ", "_") + "_Information"
        val identifier = resources.getIdentifier(identifierName, "string", packageName)
        intent.putExtra(SAR_INFORMATION, getString(identifier))
        startActivity(intent)
    }

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) parseSARFile(data.data!!)
            }
        }

    private fun openSARFile() {
        intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        resultLauncher.launch(intent)
    }

    private fun parseSARFile(uri: Uri) {
        contentResolver.query(uri, null, null, null, null)?.use { cursor ->
            val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            cursor.moveToFirst()
        }
        val inputStream = contentResolver.openInputStream(uri)
        inputStream?.let { sarService.parseSAR(this, it) }
    }

    companion object {
        internal const val SAR_INFORMATION = "SarInformation"
    }

}
