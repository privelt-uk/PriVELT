package com.kent.university.privelt.service.uber

import android.content.Context
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.uber.Profile
import com.kent.university.privelt.model.uber.PaymentMethods
import com.kent.university.privelt.model.uber.RestaurantNames
import com.kent.university.privelt.model.uber.RiderAppAnalytics
import com.kent.university.privelt.model.uber.RiderEaterSavedLocations
import com.kent.university.privelt.model.uber.RiderTripsData
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.*
import java.util.zip.ZipInputStream

class UberService : SARService() {

    data class UberFile(val name: String, val content: String)

    override var sarRequestURL: String = "https://myprivacy.uber.com/privacy/exploreyourdata/download"
    override val service = Service("Uber", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) = CoroutineScope(Dispatchers.IO).launch {
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            ZipInputStream(inputStream).use { zipInputStream ->
                val uberFiles = generateSequence { zipInputStream.nextEntry }
                    .filterNot { it.isDirectory }
                    .filterNot { it.name.startsWith("__") }
                    .filter { it.name.endsWith(".csv") }
                    .map { UberFile(name = it.name, content = zipInputStream.reader().readText()) }
                    .toList()

                if (uberFiles.isEmpty()) {
                    _stateFlow.emit(State.Error("No CSV files found in the SAR"))
                    return@launch
                }
                uberFiles.forEach {
                    if (it.name.contains("payment_methods")) {
                        val userData = readPaymentMethodsCSVFile(it.content, date, context)
                        saveUserDataToRepository(context, userData, false)
                    } else if (it.name.contains("profile_data")) {
                        val userData = readProfileCSVFile(it.content, date, context)
                        saveUserDataToRepository(context, userData, true)
                    } else if (it.name.contains("rider_eater_saved_locations")) {
                        val userData = readRiderEaterSavedLocationsCSVFile(it.content, date, context)
                        saveUserDataToRepository(context, userData, false)
                    } else if (it.name.contains("eats_restaurant_names")) {
                        val userData = readEatsRestaurantNamesCSVFile(it.content, date, context)
                        saveUserDataToRepository(context, userData, false)
                    } else if (it.name.contains("rider_app_analytics")) {
                        val userData = readRiderAppAnalyticsCSVFile(it.content, date, context)
                        saveUserDataToRepository(context, userData, false)
                    } else if (it.name.contains("trips_data")) {
                        val userData = readRiderTripDataCSVFile(it.content, date, context)
                        saveUserDataToRepository(context, userData, false)
                    }
                }
            }
        }
    }

    fun readPaymentMethodsCSVFile(content: String, date: Long, context: Context): List<UserData> {
        var lines = content.split("\n")
        var paymentMethodsList: MutableList<PaymentMethods> = mutableListOf<PaymentMethods>()
        for (line in lines) {
            if (line.isNotEmpty()) {
                // process each line of the CSV file
                val fields = line.split(",")
                val profileType = fields[0]
                val bankOrIssuerName = fields[1]
                val paymentMethodBrand = fields[2]
                val paymentMethodType = fields[3]
                val usedForCollection = fields[4]
                val usedForDisbursements = fields[5]
                val isTheLastVersion = fields[6]
                val notPayable = fields[7]
                val country = fields[8]
                val region = fields[9]
                val deletedAt = fields[10]
                // create a data class instance from the CSV data
                val data = PaymentMethods(profileType, bankOrIssuerName, paymentMethodBrand,
                    paymentMethodType, usedForCollection, usedForDisbursements, isTheLastVersion,
                    notPayable, country, region, deletedAt)
                // do something with the data, such as adding it to a list
                // or inserting it into a database
                paymentMethodsList.add(data)
            }
        }
        val initialUserData = UserData(title = "Uber Payment Methods",
            type = DataTypeList.FINANCIALS.title,
            subtype = "Uber Payment Methods",
            value = "",
            serviceId = service.id,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val list = paymentMethodsList.flatMap {
            val subData = UserData(title = "Uber Payment Method",
                type = DataTypeList.FINANCIALS.title,
                subtype = "Uber Payment Method",
                value = "",
                serviceId = service.id,
                date = date,
                parentId = initialUserData.id)
            allData.add(subData)
            it.toUserData(0L, date, subData.id)
        }
        allData.addAll(list)
        return allData
    }

    fun readProfileCSVFile(content: String, date: Long, context: Context): List<UserData> {
        var lines = content.split("\n")
        var profileList: MutableList<Profile> = mutableListOf<Profile>()
        for (line in lines) {
            if (line.isNotEmpty()) {
                // process each line of the CSV file
                val fields = line.split(",")
                val firstName = fields[0]
                val lastName = fields[1]
                val email = fields[2]
                val mobile = fields[3]
                val rating = fields[4]
                val userType = fields[5]
                val country = fields[6]
                val firstPaymentMethodAdded = fields[7]
                // create a data class instance from the CSV data
                val data = Profile(
                    firstName, lastName, email,
                    mobile, rating, userType, country,
                    firstPaymentMethodAdded
                )
                profileList.add(data)
            }
        }
        val initialUserData = UserData(title = "Uber Profiles",
            type = DataTypeList.PROFILE.title,
            subtype = "Uber Profiles",
            value = "",
            serviceId = service.id,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val list = profileList.flatMap {
            val subData = UserData(title = "Uber Profile",
                type = DataTypeList.PROFILE.title,
                subtype = "Uber Profile",
                value = "",
                serviceId = service.id,
                date = date,
                parentId = initialUserData.id)
            allData.add(subData)
            it.toUserData(0L, date, subData.id)
        }
        allData.addAll(list)
        return allData
    }

    fun readRiderEaterSavedLocationsCSVFile(content: String, date: Long, context: Context): List<UserData> {
        var lines = content.split("\n")
        var savedLocationsList: MutableList<RiderEaterSavedLocations> = mutableListOf<RiderEaterSavedLocations>()
        for (line in lines) {
            if (line.isNotEmpty()) {
                // process each line of the CSV file
                val fields = line.split(",")
                val label = fields[0]
                val houseNumber = fields[1]
                val streetName = fields[2]
                val city = fields[3]
                val state = fields[4]
                val postalCode = fields[5]
                val countryCode = fields[6]
                val latitude = fields[7]
                val longitude = fields[8]
                // create a data class instance from the CSV data
                val data = RiderEaterSavedLocations(label, houseNumber, streetName,
                    city, state, postalCode, countryCode,
                    latitude, longitude)
                // do something with the data, such as adding it to a list
                // or inserting it into a database
                savedLocationsList.add(data)
            }
        }
        val initialUserData = UserData(title = "Uber Saved Location",
            type = DataTypeList.LOCATION.title,
            subtype = "Uber Saved Location",
            value = "",
            serviceId = service.id,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val list =  savedLocationsList.flatMap {
            val subData = UserData(title = "Uber Saved Location",
                type = DataTypeList.LOCATION.title,
                subtype = "Uber Saved Location",
                value = "",
                serviceId = service.id,
                date = date,
                parentId = initialUserData.id)
            allData.add(subData)
            it.toUserData(0L, date, subData.id)
        }
        allData.addAll(list)
        return allData
    }

    fun readEatsRestaurantNamesCSVFile(content: String, date: Long, context: Context): List<UserData> {
        var lines = content.split("\n")
        var restaurantNamesList: MutableList<RestaurantNames> = mutableListOf<RestaurantNames>()
        for (line in lines) {
            if (line.isNotEmpty()) {
                // process each line of the CSV file
                val fields = line.split(",")
                val city = fields[0]
                val restaurantId = fields[1]
                val restaurantName = fields[2]
                // create a data class instance from the CSV data
                val data = RestaurantNames(city, restaurantId, restaurantName)
                // do something with the data, such as adding it to a list
                // or inserting it into a database
                restaurantNamesList.add(data)
            }
        }
        val initialUserData = UserData(title = "Uber Eats Restaurants",
            type = DataTypeList.ACTIVITY.title,
            subtype = "Uber Eats Restaurants",
            value = "",
            serviceId = service.id,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val list = restaurantNamesList.flatMap {
            val subData = UserData(title = "Uber Eats Restaurant",
                type = DataTypeList.ACTIVITY.title,
                subtype = "Uber Eats Restaurant",
                value = "",
                serviceId = service.id,
                date = date,
                parentId = initialUserData.id)
            allData.add(subData)
            it.toUserData(0L, date, subData.id)
        }
        allData.addAll(list)
        return allData
    }

    fun readRiderAppAnalyticsCSVFile(content: String, date: Long, context: Context): List<UserData> {
        var lines = content.split("\n")
        var appAnalyticsList: MutableList<RiderAppAnalytics> = mutableListOf<RiderAppAnalytics>()
        for (line in lines) {
            if (line.isNotEmpty()) {
                val fields = line.split(",")
                val latitude = fields[3]
                val longitude = fields[4]
                val city = fields[6]
                val cellularCarrier = fields[7]
                val carrierMcc = fields[8]
                val carrierMnc = fields[9]
                val ipAddress = fields[12]
                val deviceLanguage = fields[13]
                val deviceModel = fields[14]
                val deviceOs = fields[15]
                val deviceOsVersion = fields[16]
                val region = fields[17]
                // create a data class instance from the CSV data
                val data = RiderAppAnalytics(latitude, longitude, city,
                    cellularCarrier, carrierMcc, carrierMnc, ipAddress, deviceLanguage,
                    deviceModel, deviceOs, deviceOsVersion, region)
                // do something with the data, such as adding it to a list
                // or inserting it into a database
                appAnalyticsList.add(data)
            }
        }
        val initialUserData = UserData(title = "Uber App Analytics",
            type = DataTypeList.DEVICE.title,
            subtype = "Uber App Analytics",
            value = "",
            serviceId = service.id,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val list = appAnalyticsList.flatMap {
            val subData = UserData(title = "Uber App Analytics",
                type = DataTypeList.DEVICE.title,
                subtype = "Uber App Analytics",
                value = "",
                serviceId = service.id,
                date = date,
                parentId = initialUserData.id)
            allData.add(subData)
            it.toUserData(0L, date, subData.id)
        }
        allData.addAll(list)
        return allData
    }

    fun readRiderTripDataCSVFile(content: String, date: Long, context: Context): List<UserData> {
        var lines = content.split("\n")
        var tripsDataList: MutableList<RiderTripsData> = mutableListOf<RiderTripsData>()
        for (line in lines) {
            if (line.isNotEmpty()) {
                val fields = line.split(",")
                val city = fields[0]
                val tripOrOderStatus = fields[2]
                val beginTripLat = fields[5]
                val beginTripLng = fields[6]
                val beginTripAddress = fields[7]
                val dropOffLat = fields[9]
                val dropOffLng = fields[10]
                val dropOffAddress = fields[11]
                // create a data class instance from the CSV data
                val data = RiderTripsData( city, tripOrOderStatus, beginTripLat, beginTripLng,
                    beginTripAddress, dropOffLat, dropOffLng, dropOffAddress
                )
                // do something with the data, such as adding it to a list
                // or inserting it into a database
                tripsDataList.add(data)
            }
        }
        val initialUserData = UserData(title = "Uber Trips Data",
            type = DataTypeList.ACTIVITY.title,
            subtype = "Uber Trips Data",
            value = "",
            serviceId = service.id,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val list = tripsDataList.flatMap {
            val subData = UserData(title = "Uber Trip Data",
                type = DataTypeList.ACTIVITY.title,
                subtype = "Uber Trip Data",
                value = "",
                serviceId = service.id,
                date = date,
                parentId = initialUserData.id)
            allData.add(subData)
            it.toUserData(0L, date, subData.id)
        }
        allData.addAll(list)
        return allData
    }
}