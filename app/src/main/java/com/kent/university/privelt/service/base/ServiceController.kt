package com.kent.university.privelt.service.base

class ServiceController {

    companion object {
        const val PARAM_SERVICE = "param_service"

        val serviceList = mutableListOf<ServiceList>().plus(APIServiceEnum.values())
            .plus(SARServiceEnum.values()).sortedBy { it.service.name }

        val sarServiceList = mutableListOf<ServiceList>().plus(SARServiceEnum.values()).sortedBy { it.service.name }

        val apiServiceList = mutableListOf<ServiceList>().plus(APIServiceEnum.values()).sortedBy { it.service.name }

        fun getServiceItem(name: String): ServiceList {
            return serviceList.find { it.service.name.contains(name) }!!
        }

        fun getServiceItemOrNull(name: String): ServiceList? {
            return serviceList.find { it.service.name.contains(name) }
        }

        fun isAPIService(service: ServiceList): Boolean {
            return service is APIServiceEnum
        }

        fun isSARSService(service: ServiceList): Boolean {
            return service is SARServiceEnum
        }
    }

}
