package com.kent.university.privelt.model.twitter

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TwitterConnectedApps(val connectedApplication: TwitterConnectedAppDetails?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        if (connectedApplication != null) {
            return connectedApplication.toUserData(serviceId, date, parentId)
        }

        return emptyList()
    }
}

data class TwitterConnectedAppDetails(val organization: TwitterConnectedAppOrganization?,
                                      val name: String?,
                                      val permissions: List<String>,
                                      val approvedAt: String?,
                                      val id: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.filterNot { it.name == "organization"}.map{ field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter App Organization Details $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }

        val orgUserData = organization?.toUserData(serviceId, date, parentId)
        if (orgUserData != null) {
            return  orgUserData + userDataList
        }
        return userDataList
    }
}

data class TwitterConnectedAppOrganization(val name: String?,
                                           val url: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Twitter App Organization Details $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}
