package com.kent.university.privelt.model.Booking

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class BookingReservationData (
    val checkInDate : String,
    val bookerFirstName : String,
    val bookerLastName : String,
    val bookerEmail : String,
    val bookerAddress : String,
    val bookerCity : String,
    val bookerZip : String,
    val bookerCompany : String,
    val bookerPhone : String,
    val bookerCountryCode : String,
    val bookerIp : String,
    val guestName : String,
    val stayerEmail : String,
    val smokingPreferences : String,
    val numberOfGuests : String,
    val travelPurpose : String
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = splitCamelCase(field.name)
            val value = field.getter.call(this).toString()
            UserData(title = "Booking Reservation Data $title",
                type = DataTypeList.ACTIVITY.title,
                subtype = DataSubTypeListActivity.BOOKING.value,
                value = "$title: $value",
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }

    private fun splitCamelCase(text: String): String {
        return text.replace("(?=[A-Z]+)".toRegex(), " ").trim { it <= ' ' }
    }
}