package com.kent.university.privelt.service

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.kent.university.privelt.api.model.DatabaseDump
import com.kent.university.privelt.api.model.DatabaseDumpAnonymized
import com.kent.university.privelt.api.model.toServiceAnonymized
import com.kent.university.privelt.api.model.toUserDataAnonymized
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListFinancials
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.OutputStreamWriter
import java.text.SimpleDateFormat
import java.util.*

object FirebaseSavingHelper {

    fun pushToFirebaseStorage(context: Context, loadingView: View, prolificId: String) = CoroutineScope(Dispatchers.IO).launch {
        CoroutineScope(Dispatchers.Main).launch {
            loadingView.isVisible = true
        }

        val content = getContent(context)
        val storage = FirebaseStorage.getInstance()
        val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
        val fileName = "anonymised_data_${sdf.format(Date())}.json"
        val storageRef = storage.reference.child("Users_Data/$prolificId/$fileName")
        val file = File(context.filesDir, fileName)

        try {
            val outputStreamWriter = OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE))
            outputStreamWriter.write(content)
            outputStreamWriter.close()
        } catch (e: Exception) {
            Log.e("TAG", "Error writing file", e)
        }

        val fileUri = FileProvider.getUriForFile(context, "${context.packageName}.provider", file)

        storageRef.putFile(fileUri)
            .addOnSuccessListener {
                CoroutineScope(Dispatchers.Main).launch {
                    loadingView.isVisible = false
                    Toast.makeText(context, "File uploaded successfully", Toast.LENGTH_SHORT).show()
                }
                // Delete the local file after upload
                file.delete()
            }
            .addOnFailureListener { exception ->
                CoroutineScope(Dispatchers.Main).launch {
                    loadingView.isVisible = false
                    Toast.makeText(context, "File uploaded failed", Toast.LENGTH_SHORT).show()
                }
                // Delete the local file on upload failure
                file.delete()
            }
    }

    private fun getContent(context: Context): String? {

        val serviceDataRepository = PriVELTDatabase.getInstance(context)?.serviceDao()
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()
        val historyRepository = PriVELTDatabase.getInstance(context)?.historyPermissionDao()
        val userEventRepository = PriVELTDatabase.getInstance(context)?.userEventDao()
        val validSubTypes = DataSubTypeListProfile.values().map { it.value } + DataSubTypeListLocation.values().map { it.value } + DataSubTypeListFinancials.values().map { it.value } + DataSubTypeListDevice.values().map { it.value } + DataSubTypeListActivity.values().map { it.value }
        val userData = userDataRepository?.syncUserData?.filter { validSubTypes.contains(it.subtype) } ?.map { it.toUserDataAnonymized() }
        val serviceList = serviceDataRepository?.allServices?.map { it.toServiceAnonymized() }
        val historyPermissionList = historyRepository?.allHistoryPermission
        val userEvent = userEventRepository?.allUserEvent

        serviceList?.forEach {
            it.userData = userData?.filter { userData -> userData.serviceId == it.id }
        }
        val databaseDump = DatabaseDumpAnonymized(service = serviceList, historyPermissionList = historyPermissionList, userEvent = userEvent)

        val inputString = Gson().toJson(databaseDump)
        val pattern1 = "\\b\\d{10}\\b".toRegex()
        val result1 = inputString.replace(pattern1) { match ->
            val timestamp = match.value.toLong()
            val date = SimpleDateFormat("EEEE, d MMMM yyyy HH:mm:ss", Locale.ENGLISH)
                .format(Date(timestamp * 1000))
            "\"$date\""
        }
        val pattern2 = "\\b\\d{13}\\b".toRegex()
        val result2 = result1.replace(pattern2) { match ->
            val timestamp = match.value.toLong()
            val date = SimpleDateFormat("EEEE, d MMMM yyyy HH:mm:ss", Locale.ENGLISH)
                .format(Date(timestamp))
            "\"$date\""
        }
        return result2
    }
}