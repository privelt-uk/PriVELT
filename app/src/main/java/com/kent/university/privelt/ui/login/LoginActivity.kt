/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.login

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.api.DataExtraction.extractData
import com.kent.university.privelt.api.ServiceHelper
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.databinding.ActivityLoginBinding
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.utils.EyePassword
import com.kent.university.privelt.utils.getSerializableIntent
import kotlinx.coroutines.*
import net.neferett.webviewsextractor.DataExtractor
import net.neferett.webviewsinjector.response.ResponseCallback
import net.neferett.webviewsinjector.response.ResponseEnum
import net.neferett.webviewsinjector.services.LoginService

class LoginActivity : BaseActivity() {

    private lateinit var binding: ActivityLoginBinding

    private var service: Service? = null
    private var isEdit = false

    private var loginService: LoginService? = null
    private var alertDialog: AlertDialog? = null
    private var adapter: ScriptsAdapter? = null

    override val activityLayout: Int
        get() = R.layout.activity_login

    override fun configureViewModel() {}

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.testConnection.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                if (isValidInput) {
                    val res = processLogin()
                    if (res == null) {
                        Toast.makeText(
                            this@LoginActivity,
                            "Technical error occurred, please try again later",
                            Toast.LENGTH_LONG
                        ).show()
                        binding.progressCircular.visibility = View.GONE
                        binding.testConnection.isEnabled = true
                    }
                }
            }
        }
        if (intent != null) {
            service = intent.getSerializableIntent(PARAM_SERVICE, Service::class.java)
            isEdit = intent.getBooleanExtra(PARAM_EDIT, false)
        } else if (savedInstanceState != null) {
            service = savedInstanceState.getSerializable(PARAM_SERVICE, Service::class.java)
            isEdit = savedInstanceState.getBoolean(PARAM_EDIT)
        }
        if (isEdit)
            binding.addService.text = "Continue"
        assert(service != null)
        title = service!!.name
        EyePassword.configureEye(binding.eyePassword, binding.password)
        val serviceHelper = ServiceHelper(this)
        loginService = serviceHelper.getServiceWithName(service!!.name)
        binding.email.setText(service!!.user)
        binding.password.setText(service!!.password)
        binding.rememberPassword.isChecked = service!!.isPasswordSaved
        configureRecyclerView()
        binding.email.doOnTextChanged { text, start, before, count ->
            binding.addService.visibility = View.INVISIBLE
        }
        binding.password.doOnTextChanged { text, start, before, count ->
            binding.addService.visibility = View.INVISIBLE
        }
        binding.magicLink.doOnTextChanged { text, start, before, count ->
            binding.addService.visibility = View.INVISIBLE
        }
        binding.addService.setOnClickListener {
            val intent = Intent()
            intent.putExtra(PARAM_USER, binding.email.text.toString())
            intent.putExtra(PARAM_PASSWORD, binding.password.text.toString())
            service!!.isPasswordSaved = binding.rememberPassword.isChecked
            service!!.concatenatedScripts = adapter!!.concatenatedScriptsChecked
            intent.putExtra(PARAM_SERVICE, service)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        if (loginService?.name?.contains("Booking") == true) {
            binding.magicLink.isVisible = true
            binding.email.isVisible = false
            binding.password.isVisible = false
            binding.eyePassword.isVisible = false
        }
    }

    private fun configureRecyclerView() {
        binding.scripts.layoutManager = LinearLayoutManager(this)
        val dataExtractor = DataExtractor(loginService)
        adapter = ScriptsAdapter(
            dataExtractor.displayNameScripts,
            listOf(*service!!.unConcatenatedScripts)
        )
        binding.scripts.adapter = adapter
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(PARAM_SERVICE, service)
        outState.putBoolean(PARAM_EDIT, isEdit)
    }

    private suspend fun processLogin() = suspendCoroutineWithTimeout(15000) { continuation ->
        binding.progressCircular.visibility = View.VISIBLE
        binding.testConnection.isEnabled = false

        val callback = object : ResponseCallback() {
            override fun getResponse(responseEnum: ResponseEnum, data: String) {
                if (responseEnum != ResponseEnum.SUCCESS) {
                    binding.testConnection.isEnabled = true
                    showAlertDebug()
                    Toast.makeText(this@LoginActivity, responseEnum.getName(), Toast.LENGTH_LONG)
                        .show()
                    if (continuation.isActive)
                        continuation.resume(ConnectionResult.ConnectionFailure) {}
                } else {
                    Toast.makeText(this@LoginActivity, R.string.login_success, Toast.LENGTH_LONG)
                        .show()
                    binding.addService.visibility = View.VISIBLE

                    if (loginService?.name?.contains("Booking") == true) {
                        CoroutineScope(Dispatchers.IO).launch {
                            PriVELTDatabase.getInstance(applicationContext)?.serviceDao()
                                ?.insertServices(service)
                            val service =
                                PriVELTDatabase.getInstance(applicationContext)?.serviceDao()
                                    ?.getServiceWithName("Booking(R)")
                            extractData(
                                loginService!!,
                                this@LoginActivity.application as PriVELTApplication,
                                service!!
                            )
                        }
                    }

                    if (continuation.isActive)
                        continuation.resume(ConnectionResult.ConnectionSuccess) {}
                }
                binding.progressCircular.visibility = View.GONE
                binding.testConnection.isEnabled = true
            }
        }

        if (binding.magicLink.text.isNotEmpty())
            loginService!!.autoLogin(binding.magicLink.text.toString(), callback)
        else
            loginService!!.autoLogin(
                binding.email.text.toString(),
                binding.password.text.toString(),
                callback
            )
    }

    private fun showAlertDebug() {
        if (loginService!!.webview.parent != null) {
            (loginService!!.webview.parent as ViewGroup).removeView(loginService!!.webview)
        }
        if (alertDialog == null || !alertDialog!!.isShowing) {
            val dialogBuilder = AlertDialog.Builder(this@LoginActivity)
            dialogBuilder.setView(loginService!!.webview)
            alertDialog = dialogBuilder.create()
            alertDialog?.setCanceledOnTouchOutside(true)
            alertDialog?.show()
        }
    }

    private val isValidInput: Boolean
        get() {
            if (loginService?.name?.contains("Booking") != true && binding.email.text.toString()
                    .isEmpty() && binding.password.text.toString().isEmpty()
            ) {
                Toast.makeText(
                    this@LoginActivity,
                    getString(R.string.missing_credentials),
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            if (loginService?.name?.contains("Booking") != true && binding.email.text.toString()
                    .isEmpty()
            ) {
                Toast.makeText(
                    this@LoginActivity,
                    getString(R.string.missing_email),
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            if (loginService?.name?.contains("Booking") == true && binding.magicLink.text.toString()
                    .isEmpty()
            ) {
                Toast.makeText(
                    this@LoginActivity,
                    "Please enter your magic link",
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            if (loginService?.name?.contains("Booking") != true && binding.password.text.toString()
                    .isEmpty()
            ) {
                Toast.makeText(
                    this@LoginActivity,
                    getString(R.string.password_missing),
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            return true
        }

    companion object {
        const val PARAM_USER = "PARAM_USER"
        const val PARAM_PASSWORD = "PARAM_PASSWORD"
        const val PARAM_SERVICE = "PARAM_SERVICE"
        const val PARAM_EDIT = "PARAM_EDIT"
    }

    private suspend inline fun <T> suspendCoroutineWithTimeout(
        timeout: Long,
        crossinline block: (CancellableContinuation<T>) -> Unit
    ): T? {
        var finalValue: T? = null
        withTimeoutOrNull(timeout) {
            finalValue = suspendCancellableCoroutine(block = block)
        }
        return finalValue
    }

    sealed class ConnectionResult {
        object ConnectionSuccess : ConnectionResult()
        object ConnectionFailure : ConnectionResult()
    }

}
