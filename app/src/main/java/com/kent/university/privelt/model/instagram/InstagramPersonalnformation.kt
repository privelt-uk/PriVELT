package com.kent.university.privelt.model.instagram

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class InstagramPersonalInformation(
    @SerializedName("profile_user")
    val profileUser: List<InstagramPersonalInformationData>
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        return profileUser.flatMap { it.toUserData(serviceId, date) }
    }
}

data class InstagramPersonalInformationData(
    @SerializedName("title")
    val title: String?,
    @SerializedName("media_map_data")
    val mediaMapData: InstagramPersonalInformationProfilePhotoData?,
    @SerializedName("string_map_data")
    val stringMapData: InstagramPersonalInformationMapData?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Instagram Personal Information Details",
            type = DataTypeList.PROFILE.title,
            subtype = "Instagram Personal Information Details",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val titleUserData = UserData(title = "Instagram Profile Title Value",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.TITLE.value,
            value = title.toString(),
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(titleUserData)
        val stringMapData = stringMapData?.toUserData(serviceId, date, initialUserData.id) ?: emptyList()
        allData.addAll(stringMapData)
        val mediaMapData = mediaMapData?.toUserData(serviceId, date, initialUserData.id) ?: emptyList()
        allData.addAll(mediaMapData)
        return  allData
    }
}

data class InstagramPersonalInformationProfilePhotoData(
    @SerializedName("Profile photo")
    val profilePhoto: InstagramPersonalInformationProfilePhotoDetails?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        return profilePhoto?.toUserData(serviceId, date, parentId) ?: emptyList()
    }
}

data class InstagramPersonalInformationProfilePhotoDetails(
    @SerializedName("uri")
    val uri: String?,
    @SerializedName("creation_timestamp")
    val creationTimestamp: String?,
    @SerializedName("title")
    val title: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Instagram Profile Photo Data $title",
                type = DataTypeList.PROFILE.title,
                subtype = DataSubTypeListProfile.PREFERENCES.value,
                value = "$title: ${field.getter.call(this).toString()}",
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class InstagramPersonalInformationMapData(
    @SerializedName("Email address")
    val emailAddress: InstagramInnerGeneric?,
    @SerializedName("Phone number")
    val phoneNumber: InstagramInnerGeneric?,
    @SerializedName("Phone number confirmed")
    val phoneNumberConfirmed: InstagramInnerGeneric?,
    @SerializedName("Phone confirmation method")
    val phoneConfirmationMethod: InstagramInnerGeneric?,
    @SerializedName("Username")
    val username: InstagramInnerGeneric?,
    @SerializedName("Name")
    val name: InstagramInnerGeneric?,
    @SerializedName("Bio")
    val bio: InstagramInnerGeneric?,
    @SerializedName("Gender")
    val gender: InstagramInnerGeneric?,
    @SerializedName("Private account")
    val privateAccount: InstagramInnerGeneric?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val emailAddressUserData = UserData(title = "Instagram Profile Email Address",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = emailAddress?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(emailAddressUserData)
        val phoneNumberUserData = UserData(title = "Instagram Profile Phone Number",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = phoneNumber?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(phoneNumberUserData)
        val phoneNumberConfirmedUserData = UserData(title = "Instagram Profile Phone Number Confirmed",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Phone number confirmed: ${phoneNumber?.value ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(phoneNumberConfirmedUserData)
        val phoneConfirmationMethodUserData = UserData(title = "Instagram Profile Phone Number Confirmation method",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Phone number confirmation method: ${phoneConfirmationMethod?.value ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(phoneConfirmationMethodUserData)
        val usernameUserData = UserData(title = "Instagram Profile Username",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.USERNAME.value,
            value = username?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(usernameUserData)
        val nameUserData = UserData(title = "Instagram Profile Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = name?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(nameUserData)
        val bioUserData = UserData(title = "Instagram Profile Bio",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Bio: ${bio?.value ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(bioUserData)
        val genderUserData = UserData(title = "Instagram Profile Gender",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.GENDER.value,
            value = gender?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(genderUserData)
        val privateAccountUserData = UserData(title = "Instagram Profile Private Account",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Private account: ${privateAccount?.value ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(privateAccountUserData)

        return allData
    }
}