package com.kent.university.privelt.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_event")
data class UserEvent(val tag: String, val message: String, val date: Long) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
