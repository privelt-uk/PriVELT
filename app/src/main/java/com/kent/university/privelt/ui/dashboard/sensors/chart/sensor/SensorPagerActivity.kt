/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.dashboard.sensors.chart.sensor

import android.os.Bundle
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivitySensorChartBinding


class SensorPagerActivity : BaseActivity() {

    private var mSensorPagerAdapter: SensorPagerAdapter? = null

    override val activityLayout: Int
        get() = R.layout.activity_sensor_chart

    override fun configureViewModel() {

    }

    override fun configureDesign(savedInstanceState: Bundle?) {
        val binding = ActivitySensorChartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSensorPagerAdapter = SensorPagerAdapter(supportFragmentManager)
        binding.sensorPager.adapter = mSensorPagerAdapter

        binding.sensorPager.setCurrentItem(0, false)
    }

}
