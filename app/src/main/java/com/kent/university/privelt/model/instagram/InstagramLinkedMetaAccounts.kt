package com.kent.university.privelt.model.instagram

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class InstagramLinkedMetaAccounts(
    @SerializedName("profile_linked_meta_accounts")
    val profileLinkedMetaAccounts: List<InstagramLinkedMetaAccountsData>
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        return profileLinkedMetaAccounts.map { it.toUserData(serviceId, date) }.flatten()
    }
}

data class InstagramLinkedMetaAccountsData(
    @SerializedName("title")
    val title: String?,
    @SerializedName("media_map_data")
    val mediaMapData: Map<String, Any>?,
    @SerializedName("string_map_data")
    val stringMapData: InstagramLinkedMetaAccountDetails?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Instagram Meta Accounts Details",
            type = DataTypeList.PROFILE.title,
            subtype = "Instagram Meta Accounts Details",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)
        val connectedAccountTitleUserData = UserData(title = "Instagram Meta Account Title",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = title ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(connectedAccountTitleUserData)
        val mediaUserData = UserData(title = "Instagram Meta Account media data",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = (mediaMapData ?: emptyMap()).toString(),
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(mediaUserData)
        val detailsUserData = stringMapData?.toUserData(serviceId, date, initialUserData.id) ?: emptyList()
        allData.addAll(detailsUserData)
        return allData
    }
}

data class InstagramLinkedMetaAccountDetails(
    @SerializedName("Account type")
    val accountType: InstagramInnerGeneric?,
    @SerializedName("User name")
    val username: InstagramInnerGeneric?,
    @SerializedName("Identifier")
    val identifier: InstagramInnerGeneric?,
    @SerializedName("Email address")
    val emailAddress: InstagramInnerGeneric?,
    @SerializedName("Phone number")
    val phoneNumber: InstagramInnerGeneric?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val accountTypeUserData = UserData(title = "Instagram Connected Account Account Type",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = accountType?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(accountTypeUserData)
        val usernameUserData = UserData(title = "Instagram Connected Account Username",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = username?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(usernameUserData)
        val identifierUserData = UserData(title = "Instagram Connected Account identifier",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = identifier?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(identifierUserData)
        val emailAddressUserData = UserData(title = "Instagram Connected Account Email Address",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = emailAddress?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(emailAddressUserData)
        val phoneNumberUserData = UserData(title = "Instagram Connected Account Phone number",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = phoneNumber?.value ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(phoneNumberUserData)

        return allData
    }
}