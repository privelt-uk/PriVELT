/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.master_password

import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.databinding.CellPdaBinding
import com.kent.university.privelt.model.ServicePDA

class ImportDataViewHolder(private val binding: CellPdaBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(servicePDA: ServicePDA) {
        binding.title.text = servicePDA.title
        binding.logo.setImageResource(servicePDA.image)
    }

}
