/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.base

import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.DialogInterface
import android.content.Intent
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.android.gms.tasks.Task
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.R
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.utils.DriveServiceHelper
import com.kent.university.privelt.utils.hat.AccountManager
import com.kent.university.privelt.utils.hat.DeleteFileTask
import com.kent.university.privelt.utils.hat.DownloadFileTask
import com.kent.university.privelt.utils.hat.UploadFileTask
import com.privelt.pda.dataplatform.DataPlatformFactory
import com.privelt.pda.dataplatform.DataPlatformType
import com.privelt.pda.dataplatform.generic.Credentials
import com.privelt.pda.dataplatform.generic.DataPlatformClient
import com.privelt.pda.dataplatform.hat.HatClient
import com.privelt.pda.dataplatform.hat.files.HatFileDetails
import com.privelt.pda.dataplatform.hat.response.HatAuthenticationResponse
import java.io.File
import java.net.MalformedURLException
import java.net.URL

abstract class PDAFragment : BaseFragment() {

    protected var mDriveServiceHelper: DriveServiceHelper? = null
    protected var listener: PDAListener? = null

    private fun getDeepLink(): String {
        return String.format("%s://%s", DEEP_LINK_SCHEME, DEEP_LINK_DOMAIN)
    }

    private var credentials: Credentials? = null
    private val dataPlatformType: DataPlatformType? = null
    private var dataPlatformClient: DataPlatformClient? = null
    private var hatLogin = false
    private var username = ""
    private var token = ""

    abstract fun hatLogged()

    abstract fun googleLogged()

    protected fun googleDriveConnection() {
        val account = GoogleSignIn.getLastSignedInAccount(activity!!)
        if (account == null) {
            requestSignIn()
        } else {
            val credential =
                GoogleAccountCredential.usingOAuth2(activity, listOf(DriveScopes.DRIVE_FILE))
            credential.selectedAccount = account.account
            val googleDriveService = Drive.Builder(
                AndroidHttp.newCompatibleTransport(),
                GsonFactory(),
                credential
            )
                .setApplicationName(getString(R.string.app_name))
                .build()
            mDriveServiceHelper = DriveServiceHelper(googleDriveService)
            googleLogged()
        }
    }

    fun downloadFile() {
        googleDriveConnection()
        mDriveServiceHelper!!.downloadFile(
            activity!!.applicationContext.filesDir.path
        )
            .addOnSuccessListener {
                Log.i("Privelt", "onDownloadSuccess")
                listener!!.onDownloadSuccess()
            }
            .addOnFailureListener {
                Log.i("Privelt", it.stackTraceToString())
                listener!!.onDownloadFailure()
            }
    }

    companion object {
        private const val REQUEST_CODE_SIGN_IN = 1
        private const val DEEP_LINK_SCHEME = "https"
        private const val DEEP_LINK_DOMAIN = "pdaapi.app"
    }

    private fun requestSignIn() {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestScopes(Scope(DriveScopes.DRIVE_FILE))
            .build()
        val client = GoogleSignIn.getClient(activity!!, signInOptions)
        startActivityForResult(client.signInIntent, REQUEST_CODE_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val googleAccount = completedTask.getResult(ApiException::class.java)
            val credential =
                GoogleAccountCredential.usingOAuth2(activity!!, listOf(DriveScopes.DRIVE_FILE))
            credential.selectedAccount = googleAccount.account
            val googleDriveService =
                Drive.Builder(AndroidHttp.newCompatibleTransport(), GsonFactory(), credential)
                    .setApplicationName("Drive API Migration").build()
            mDriveServiceHelper = DriveServiceHelper(googleDriveService)
           // if (listener != null) downloadFile()
            listener!!.onConnectionSuccess()
            googleLogged()
        } catch (e: ApiException) {
            Firebase.crashlytics.recordException(e)
            Log.e(TAG, "signInResult:failed code=" + e.statusCode)
        }
    }

    //HAT

    private fun parseHatLoginCallback(url: String): Boolean {
        try {
            val aURL = URL(url)
            if (aURL.protocol.equals(DEEP_LINK_SCHEME, ignoreCase = true)) {
                val host = aURL.host
                if (host != null) {
                    if (host.contains("hubat.net")) {
                        val hatUsername = host.substring(0, host.indexOf('.'))
                        username = hatUsername
                        credentials?.username = hatUsername
                    } else if (host.equals(DEEP_LINK_DOMAIN, ignoreCase = true)) {
                        val query = aURL.query
                        if (query.startsWith("token=")) {
                            val appToken = query.replace("token=", "")
                            token = appToken
                            credentials?.token = appToken
                        }
                        return true
                    }
                }
            }
        } catch (e: MalformedURLException) {
            Firebase.crashlytics.recordException(e)
            Log.e("PriVELT-APP", "onItemSelected", e)
        }
        return false
    }

    private fun createWebView(): WebView {
        val wv = WebView(activity!!)
        val webSettings = wv.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.setGeolocationEnabled(true)
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webSettings.builtInZoomControls = true
        webSettings.displayZoomControls = false
        webSettings.setSupportZoom(true)
        webSettings.defaultTextEncodingName = "utf-8"
        wv.requestFocus(View.FOCUS_DOWN)
        return wv
    }

    private fun createDataPlatform() {
        credentials = null
        if (dataPlatformClient == null) {
            credentials = Credentials(username, "password")
            credentials?.token = token
            val hatClient = HatClient(credentials)
            dataPlatformClient = hatClient
        }

        // Add Hat App name:
        val appName = "app-112-dev"
        if (dataPlatformClient is HatClient) {
            (dataPlatformClient as HatClient).appName = appName
        }
        val dataPlatform =
            if (dataPlatformClient == null)
                DataPlatformFactory.getDataPlatform(dataPlatformType, credentials)
            else
                DataPlatformFactory.getDataPlatform(dataPlatformClient)
        AccountManager.getInstance().dataPlatform = dataPlatform
    }

    fun setHatAuthenticationResult(hatAuthenticationResponse: HatAuthenticationResponse?) {
        if (hatAuthenticationResponse == null) {
            //hatSwitch.visibility = View.INVISIBLE
            return
        }
        token = hatAuthenticationResponse.accessToken
        //hatSwitch.visibility = View.VISIBLE
    }

    fun showMessage(message: String?) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    fun setHatLoginResult() {
        if (!hatLogin) {
            return
        }
        token = credentials!!.token
        //hatSwitch.visibility = View.VISIBLE
    }

    private fun createHatFileObject(filePath: String, file: File): HatFileDetails {
        val hatFilesDetails = HatFileDetails(file.name, filePath, listOf(""))
        hatFilesDetails.size = file.length()
        return hatFilesDetails
    }

    fun uploadDatabaseWithHAT() {
        createDataPlatform()

        // Request File Upload:
        val file = activity!!.getDatabasePath(PriVELTDatabase.PriVELTDatabaseName)
        val hatFilesDetails: HatFileDetails = createHatFileObject(file.absolutePath, file)
        UploadFileTask(
            AccountManager.getInstance().dataPlatformController,
            hatFilesDetails,
            listener
        ).execute()
    }

    fun deleteFileOnHAT(fileID: String) {
        DeleteFileTask(AccountManager.getInstance().dataPlatformController, fileID).execute()
    }

    fun downloadFileWithHAT(fileId: String) {
        createDataPlatform()
        DownloadFileTask(
            AccountManager.getInstance().dataPlatformController,
            fileId,
            activity!!.getDatabasePath(PriVELTDatabase.PriVELTDatabaseName).path,
            listener
        ).execute()
    }

    fun loginHAT(email: String) {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(activity, "Please enter a valid email address", Toast.LENGTH_LONG).show()
            return
        }
        // Reset objects:
        hatLogin = false
        credentials = Credentials()
        val alert = AlertDialog.Builder(activity!!)
        alert.setTitle("HAT Signin/Signup")
        alert.setCancelable(true)
        alert.setNegativeButton("Close") { dialog: DialogInterface, id: Int -> dialog.dismiss() }
        val alertDialog = alert.create()
        val wv: WebView = createWebView()
        val hatAppName = "app-112-dev"
        val signupURL = HatClient.getSignupURL(email, hatAppName, getDeepLink())
        wv.loadUrl(signupURL)
        wv.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.i("WebView-Tag", "onPageFinished: $url")
                if (parseHatLoginCallback(url)) {
                    alertDialog.dismiss()
                    setHatLoginResult()
                    hatLogin = !hatLogin
                    if (hatLogin)
                        hatLogged()
                }
                Log.i("PriVELT-APP", credentials.toString())
            }
        }
        val wrapper = LinearLayout(activity)

        val keyboardHack = EditText(activity)

        keyboardHack.visibility = View.GONE
        wrapper.orientation = LinearLayout.VERTICAL
        wrapper.addView(
            wv,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        wrapper.addView(
            keyboardHack,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        alertDialog.setView(wrapper)
        alertDialog.show()
    }

}
