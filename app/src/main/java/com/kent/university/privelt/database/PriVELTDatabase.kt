/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.kent.university.privelt.database.dao.*
import com.kent.university.privelt.model.*

@Database(entities = [UserData::class, Service::class, CurrentUser::class, Settings::class, HistoryPermission::class, UserEvent::class], version = 20, exportSchema = false)
abstract class PriVELTDatabase : RoomDatabase() {
    abstract fun userDataDao(): UserDataDao?
    abstract fun serviceDao(): ServiceDao?
    abstract fun currentUserDao(): CurrentUserDao?
    abstract fun settingsDao(): SettingsDao?
    abstract fun historyPermissionDao(): HistoryPermissionDao?
    abstract fun userEventDao(): UserEventDao?

    companion object {
        @Volatile
        private var INSTANCE: PriVELTDatabase? = null
        const val PriVELTDatabaseName = "PriVELTDatabase.db"

        fun getInstance(context: Context): PriVELTDatabase? {
            if (INSTANCE == null) {
                synchronized(PriVELTDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                PriVELTDatabase::class.java,
                                PriVELTDatabaseName)
                                .addMigrations(MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6, MIGRATION_6_7, MIGRATION_7_8, MIGRATION_8_9, MIGRATION_9_10, MIGRATION_10_11, MIGRATION_11_12, MIGRATION_12_13, MIGRATION_13_14, MIGRATION_14_15, MIGRATION_15_16, MIGRATION_16_17, MIGRATION_17_18, MIGRATION_18_19, MIGRATION_19_20)
                                .build()
                    }
                }
            }
            return INSTANCE
        }

        private val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `settings` (`id` INTEGER, `googleDriveAutoSave` INTEGER, `googleDriveFileID` TEXT, PRIMARY KEY(`id`))")
            }
        }

        private val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `sensor_status` (`id` INTEGER, `sensorName` TEXT, `date` INTEGER, `wereActivated` INTEGER , PRIMARY KEY(`id`))")
            }
        }

        private val MIGRATION_5_6: Migration = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `user_data` ADD COLUMN `date` INTEGER default 0 NOT NULL")
            }
        }

        private val MIGRATION_6_7: Migration = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `permission_status` (`id` INTEGER NOT NULL, `permissionName` TEXT NOT NULL, `date` INTEGER NOT NULL, `wereActivated` INTEGER NOT NULL, `applicationPackage` TEXT NOT NULL, PRIMARY KEY(`id`))")
            }
        }

        private val MIGRATION_7_8: Migration = object : Migration(7, 8) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `history_permission` (`id` INTEGER NOT NULL, `date` INTEGER NOT NULL, `locationSensor` INTEGER NOT NULL,`bluetoothSensor` INTEGER NOT NULL, `nfcSensor` INTEGER NOT NULL, `wifiSensor` INTEGER NOT NULL,`locationValue` INTEGER NOT NULL, `contactsValue` INTEGER NOT NULL, `bluetoothValue` INTEGER NOT NULL,`storageValue` INTEGER NOT NULL,`wifiValue` INTEGER NOT NULL,`nfcValue` INTEGER NOT NULL,`calendarValue` INTEGER NOT NULL,`smsValue` INTEGER NOT NULL,PRIMARY KEY(`id`))")
            }
        }

        private val MIGRATION_8_9: Migration = object : Migration(8, 9) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `microValue` INTEGER default 0 NOT NULL")
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `cameraValue` INTEGER default 0 NOT NULL")
            }
        }

        private val MIGRATION_9_10: Migration = object : Migration(9, 10) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `accountsValue` INTEGER default 0 NOT NULL")
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `phonestateValue` INTEGER default 0 NOT NULL")
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `playingcontentValue` INTEGER default 0 NOT NULL")
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `activityrecognitionValue` INTEGER default 0 NOT NULL")
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `networkstateValue` INTEGER default 0 NOT NULL")
                database.execSQL("ALTER TABLE `history_permission` ADD COLUMN `bodysensorsValue` INTEGER default 0 NOT NULL")
            }
        }

        private val MIGRATION_10_11: Migration = object : Migration(10, 11) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE permission_status")
                database.execSQL("DROP TABLE sensor_status")
            }
        }

        private val MIGRATION_11_12: Migration = object : Migration(11, 12) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `settings` ADD COLUMN `hatFileID` TEXT default 0 NOT NULL")
            }
        }

        private val MIGRATION_12_13: Migration = object : Migration(12, 13) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `settings` ADD COLUMN `hatFileID2` TEXT")
                database.execSQL("ALTER TABLE `settings` ADD COLUMN `googleDriveFileID2` TEXT")
            }
        }

        private val MIGRATION_13_14: Migration = object : Migration(13, 14) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `service` ADD COLUMN `createdAt` INTEGER default 0 NOT NULL")
            }
        }

        private val MIGRATION_14_15: Migration = object : Migration(14, 15) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `user_event` (`id` INTEGER NOT NULL, `date` INTEGER NOT NULL, `tag` TEXT NOT NULL,`message` TEXT NOT NULL, PRIMARY KEY(`id`))")
            }
        }

        private val MIGRATION_15_16: Migration = object : Migration(15, 16) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `user_data` ADD COLUMN `parentId` TEXT")
                database.execSQL("ALTER TABLE `user_data` ADD COLUMN `id_string` TEXT NOT NULL DEFAULT ''")
            }
        }

        private val MIGRATION_16_17: Migration = object : Migration(16, 17) {
            override fun migrate(database: SupportSQLiteDatabase) {
            }
        }

        private val MIGRATION_17_18: Migration = object : Migration(17, 18) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "CREATE TABLE user_data_new (" +
                            "id_string TEXT PRIMARY KEY NOT NULL," +
                            "title TEXT NOT NULL," +
                            "type TEXT NOT NULL," +
                            "value TEXT NOT NULL," +
                            "concatenatedData TEXT NOT NULL," +
                            "service_id INTEGER NOT NULL," +
                            "date INTEGER NOT NULL," +
                            "parentId TEXT," +
                            "FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE" +
                            ")"
                )

                database.execSQL("CREATE INDEX index_user_data_service_id2 ON user_data_new(service_id)")
                database.execSQL(
                    "INSERT INTO user_data_new (id_string, title, type, value, concatenatedData, service_id, date, parentId) " +
                            "SELECT id, title, type, value, concatenatedData, service_id, date, parentId FROM user_data"
                )
                database.execSQL("DROP TABLE user_data")
                database.execSQL("ALTER TABLE user_data_new RENAME TO user_data")
            }
        }

        private val MIGRATION_18_19: Migration = object : Migration(18, 19) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE user_data RENAME COLUMN id_string TO id")
            }
        }

        private val MIGRATION_19_20: Migration = object : Migration(19, 20) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("UPDATE service SET name = 'X (Twitter)' WHERE name = 'Twitter SAR'")
            }
        }

        fun nullDatabase() {
            synchronized(PriVELTDatabase::class.java) { INSTANCE = null }
        }
    }
}