/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.sensors

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseFragment
import com.kent.university.privelt.databinding.FragmentSensorsBinding
import com.kent.university.privelt.events.LaunchDetailedSensorEvent
import com.kent.university.privelt.model.Sensor
import com.kent.university.privelt.ui.dashboard.sensors.chart.global.RadarChartSensorActivity
import com.kent.university.privelt.ui.dashboard.sensors.chart.sensor.SensorPagerActivity
import com.kent.university.privelt.ui.dashboard.sensors.detailed.DetailedSensorActivity
import com.kent.university.privelt.utils.privacy_scoring.PermissionScoring
import com.kent.university.privelt.utils.privacy_scoring.PermissionScoring.computeGlobalScore
import com.kent.university.privelt.utils.sensors.SensorHelper
import com.kent.university.privelt.utils.sentence.SentenceAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class SensorFragment : BaseFragment() {

    private lateinit var binding: FragmentSensorsBinding

    override val fragmentLayout: Int
        get() = R.layout.fragment_sensors

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun configureViewModel() {}

    override fun configureDesign(): View {
        binding = FragmentSensorsBinding.inflate(layoutInflater)
        val view = binding.root
        binding.loader.visibility = View.VISIBLE
        CoroutineScope(Dispatchers.IO).launch {
            val sensorsList = SensorHelper.getSensorsInformation(context!!).sortedByDescending {
                PermissionScoring.computeScoreForPermission(
                    it,
                    SensorHelper.getNumberOfApplicationInstalled(context!!)
                ).toInt()
            }
            CoroutineScope(Dispatchers.Main).launch {
                setUpRecyclerView(sensorsList)
                updateOverallRiskValue()
                binding.loader.visibility = View.GONE
            }
        }
        return view
    }

    private fun updateOverallRiskValue() {

        val numberOfApplicationInstalled = SensorHelper.getNumberOfApplicationInstalled(context!!).toString()
        binding.header.applicationsInstalled.text = "$numberOfApplicationInstalled applications installed"
        binding.header.applicationsInstalled.isVisible = true
        var riskValue = computeGlobalScore(context!!)

        if (riskValue > 100) riskValue = 100
        when {
            riskValue == 0 -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value),
                    "None"
                )
                binding.header.privacyValue.setTextColor(Color.GRAY)
            }
            riskValue < 20 -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value),
                    "Low"
                )
                binding.header.privacyValue.setTextColor(Color.GREEN)
            }
            riskValue < 60 -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value),
                    "Medium"
                )
                binding.header.privacyValue.setTextColor(Color.parseColor("#FFBF00"))
            }
            else -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value),
                    "High"
                )
                binding.header.privacyValue.setTextColor(Color.RED)
            }
        }
        binding.header.progressBar.progress = riskValue

        binding.header.globalChartSpider.setOnClickListener {
            startActivity(Intent(activity, RadarChartSensorActivity::class.java))
        }

        binding.header.globalChartBar.setOnClickListener {
            startActivity(Intent(activity, SensorPagerActivity::class.java))
        }

    }

    private fun setUpRecyclerView(sensorsList: List<Sensor>) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        binding.sensors.layoutManager = layoutManager
        val cardAdapter = SensorAdapter(sensorsList)
        binding.sensors.adapter = cardAdapter
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.risk) {
            binding.loader.visibility = View.VISIBLE
            CoroutineScope(Dispatchers.IO).launch {
                val sensorsList = SensorHelper.getSensorsInformation(context!!).sortedByDescending {
                    PermissionScoring.computeScoreForPermission(
                        it,
                        SensorHelper.getNumberOfApplicationInstalled(context!!)
                    ).toInt()
                }
                CoroutineScope(Dispatchers.Main).launch {
                    setUpRecyclerView(sensorsList)
                    binding.loader.visibility = View.GONE
                }
            }
            return true
        } else if (item.itemId == R.id.status) {
            binding.loader.visibility = View.VISIBLE
            CoroutineScope(Dispatchers.IO).launch {
                val sensorsList = SensorHelper.getSensorsInformation(context!!).sortedWith(compareBy<Sensor> { !it.isEnabled(context!!) }.thenBy { !it.isSensor }.thenBy { it.title.uppercase(Locale.getDefault()) })
                CoroutineScope(Dispatchers.Main).launch {
                    setUpRecyclerView(sensorsList)
                    binding.loader.visibility = View.GONE
                }
            }
            return true
        } else if (item.itemId == R.id.name) {
            binding.loader.visibility = View.VISIBLE
            CoroutineScope(Dispatchers.IO).launch {
                val sensorsList = SensorHelper.getSensorsInformation(context!!).sortedBy { it.title.uppercase(Locale.getDefault()) }
                CoroutineScope(Dispatchers.Main).launch {
                    setUpRecyclerView(sensorsList)
                    binding.loader.visibility = View.GONE
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    @Subscribe
    fun onDetailedSensorEvent(event: LaunchDetailedSensorEvent) {
        val intent = Intent(activity, DetailedSensorActivity::class.java)
        intent.putExtra(PARAM_SENSOR, event.sensor)
        startActivity(intent)
    }

    companion object {
        const val PARAM_SENSOR = "PARAM_SENSOR"
    }

}
