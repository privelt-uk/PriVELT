/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.card

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.kent.university.privelt.R
import com.kent.university.privelt.api.DataExtraction.processDataExtraction
import com.kent.university.privelt.api.ServiceHelper
import com.kent.university.privelt.base.BaseFragment
import com.kent.university.privelt.databinding.FragmentServiceBinding
import com.kent.university.privelt.events.ChangeWatchListStatusEvent
import com.kent.university.privelt.events.ClickedDateExtractionDateEvent
import com.kent.university.privelt.events.ClickedRefreshEvent
import com.kent.university.privelt.events.DetailedCardEvent
import com.kent.university.privelt.events.RadarClickedEvent
import com.kent.university.privelt.events.UpdateCredentialsEvent
import com.kent.university.privelt.model.Card
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.APIServiceEnum
import com.kent.university.privelt.service.base.SARServiceEnum
import com.kent.university.privelt.service.base.ServiceController
import com.kent.university.privelt.ui.dashboard.DashboardActivity
import com.kent.university.privelt.ui.dashboard.card.detailed.DetailedCardActivity
import com.kent.university.privelt.ui.login.LoginActivity
import com.kent.university.privelt.ui.risk_value.RiskValueActivity
import com.kent.university.privelt.ui.risk_value.RiskValueActivity.Companion.PARAM_IS_DATA_CENTRIC
import com.kent.university.privelt.ui.risk_value.RiskValueActivity.Companion.PARAM_SUB_TYPE_PARENT
import com.kent.university.privelt.ui.service.APIServiceActivity
import com.kent.university.privelt.ui.service.SARServiceActivity
import com.kent.university.privelt.utils.CardManager
import com.kent.university.privelt.utils.PriVELTLog
import com.kent.university.privelt.utils.WatchListHelper
import com.kent.university.privelt.utils.firebase.RemoteConfigHelper
import com.kent.university.privelt.utils.getSerializableIntent
import com.kent.university.privelt.utils.sentence.SentenceAdapter
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*
import java.util.concurrent.Executors

class CardFragment : BaseFragment(), AddCardDialogFragment.AddCardDialogListener {

    private lateinit var binding: FragmentServiceBinding

    private var cardViewModel: CardViewModel? = null
    private var subscribedServices: ArrayList<Service>? = null
    private var extractionDates: List<Long> = emptyList()
    private var userData: ArrayList<UserData>? = null
    private var watchListHelper: WatchListHelper? = null
    private var cardAdapter: CardAdapter? = null
    private lateinit var filters: BooleanArray
    private var newFragment: AddCardDialogFragment? = null
    private var isFavorite = false
    set(value) {
        field = value
        val activity = activity as? DashboardActivity ?: return
        activity.watchlist?.setIcon(if (value) R.drawable.ic_eye_white else R.drawable.ic_eye_white_disabled)
    }
    private var sorting = SORTING.DATE

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        watchListHelper = WatchListHelper(
            activity!!.getSharedPreferences(
                FilterAlertDialog.KEY_SHARED, Context.MODE_PRIVATE
            )
        )
    }

    private fun getUserData() {
        cardViewModel!!.userData?.observe(this) { userData: List<UserData> ->
            updateUserData(
                userData
            )
        }
    }

    private fun updateUserData(userData: List<UserData>) {
        this.userData = ArrayList(userData)
        updateRecyclerView()
        updateOverallRiskValue()
    }

    private val services: Unit
        get() {
            cardViewModel!!.services!!.observe(this) { services: List<Service> ->
                updateServices(
                    services
                )
            }
        }

    private fun updateServices(services: List<Service>) {
        subscribedServices = ArrayList(services)
        updateRecyclerView()
        updateOverallRiskValue()
    }

    private fun setLastDateToEveryService() {
        for (service in subscribedServices!!) {
            val date = userData?.filter { it.serviceId == service.id }?.maxByOrNull { it.date }?.date ?: 0
            if (service.dateSelected == 0L)
                service.dateSelected = date
            else {
                val isPresent = userData?.any { it.date == service.dateSelected } == true
                if (!isPresent)
                    service.dateSelected = date
            }
        }
    }

    private fun updateRecyclerView() {

        setLastDateToEveryService()

        if (subscribedServices!!.size == 0) {
            binding.noServices.visibility = View.VISIBLE
            cardAdapter!!.updateCards(ArrayList(0))
        } else {
            binding.noServices.visibility = View.GONE
            var list = CardManager.cardsFilter(
                userData, subscribedServices!!, filters, watchListHelper!!.watchList
            )
            if (sorting == SORTING.RISK)
                list = list.sortedByDescending {
                    var value = 0
                    for (i in it.metrics) value += i.number
                    value
                }
            else if (sorting == SORTING.NAME)
                list = list.sortedBy { it.title.uppercase(Locale.getDefault()) }
            else if (sorting == SORTING.DATE)
                list = list.sortedBy {
                    it.date
                }

            cardAdapter!!.updateCards(list)
        }
        cardAdapter!!.notifyDataSetChanged()
    }

    private fun updateOverallRiskValue() {
        var riskValue = 0
        if (userData == null)
            return
        if (isFavorite) {
            for (data in userData!!) {
                for (service in subscribedServices!!)
                    if (data.serviceId == service.id && watchListHelper!!.watchList.contains(service.name))
                        if (data.date == service.dateSelected)
                            riskValue++
            }
        } else {
            for (data in userData!!)
                for (service in subscribedServices!!)
                    if (data.date == service.dateSelected)
                        riskValue++
        }
        if (riskValue > 100) riskValue = 100
        binding.header.root.isVisible = riskValue != 0
        binding.header.privacyConst.text = "Data sharing profile : "
        when {
            riskValue == 0 -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value), "None"
                )
                binding.header.privacyValue.setTextColor(Color.GRAY)
            }
            riskValue < 20 -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value), "Low"
                )
                binding.header.privacyValue.setTextColor(Color.GREEN)
            }
            riskValue < 60 -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value),
                    "Medium"
                )
                binding.header.privacyValue.setTextColor(Color.parseColor("#FFBF00"))
            }
            else -> {
                binding.header.privacyValue.text = SentenceAdapter.adapt(
                    binding.root.context.resources.getString(R.string.global_privacy_value), "High"
                )
                binding.header.privacyValue.setTextColor(Color.RED)
            }
        }
        binding.header.progressBar.progress = riskValue
    }

    private fun setupAddButton() {
        binding.addService.setOnClickListener {

            var dialog: ChooseServiceTypeDialogFragment? = null
            dialog = ChooseServiceTypeDialogFragment { position ->
                if (position == 0)
                    displayServicesChoiceList(false)
                else
                    displayServicesChoiceList(true)
                dialog?.dismiss()
            }
            dialog.show(activity?.supportFragmentManager!!, "missiles")
        }
    }

    private fun displayServicesChoiceList(isSAR: Boolean) {
        var services: MutableList<String> = mutableListOf()
        val list = if (isSAR) ServiceController.sarServiceList else ServiceController.apiServiceList

        list.forEach { serviceListItem ->
            if (subscribedServices!!.find { it.name == serviceListItem.service.name } == null) services.add(
                serviceListItem.service.name
            )
        }

        val options = RemoteConfigHelper.getServicesOptions()
        options?.let { serviceOptions ->
            services.removeIf { name ->
                serviceOptions.servicesOptions.any { it.name == name && !it.visibility }
            }
            services = services.sortedBy { name ->
                var indexCommputed = 0
                serviceOptions.servicesOptions.filterIndexed { index, serviceOption ->
                    if (serviceOption.name == name)
                        indexCommputed = index
                    true
                }
                indexCommputed
            }.toMutableList()
        }

        if (services.isEmpty()) {
            Toast.makeText(
                this@CardFragment.context, R.string.already_added_all, Toast.LENGTH_LONG
            ).show()
            return
        }
        newFragment = AddCardDialogFragment(services, this)
        newFragment!!.show(activity?.supportFragmentManager!!, "missiles")
    }

    private fun editCredentials(service: Service, requestCode: Int, isEdit: Boolean) {
        val intent = Intent(context, LoginActivity::class.java)
        intent.putExtra(LoginActivity.PARAM_SERVICE, service)
        intent.putExtra(LoginActivity.PARAM_EDIT, isEdit)
        startActivityForResult(intent, requestCode)
    }

    private fun setUpRecyclerView(view: View) {
        subscribedServices = ArrayList()
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(view.context)
        binding.servicesList.layoutManager = layoutManager
        cardAdapter = CardAdapter()
        binding.servicesList.adapter = cardAdapter
    }

    override val fragmentLayout: Int
        get() = R.layout.fragment_service

    override fun configureViewModel() {
        cardViewModel = getViewModel(CardViewModel::class.java)
        cardViewModel?.init()
    }

    override fun configureDesign(): View {
        binding = FragmentServiceBinding.inflate(layoutInflater)
        val view = binding.root

        setupAddButton()
        services
        getUserData()
        binding.header.globalChart.setOnClickListener {
            val intent = Intent(
                activity, RiskValueActivity::class.java
            )
            intent.putExtra(PARAM_IS_DATA_CENTRIC, binding.tabLayout.selectedTabPosition == 1)
            startActivity(
                intent
            )
        }
        enableSwipeToDeleteAndUndo()
        binding.tabLayout.addTab(
            binding.tabLayout.newTab().setText("Services")
        )
        binding.tabLayout.addTab(
            binding.tabLayout.newTab().setText("Data")
        )

        filters = booleanArrayOf(
            false, true, false
        )
        setUpRecyclerView(view)

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                filters = booleanArrayOf(
                    tab?.text == "Data",
                    tab?.text == "Services",
                    isFavorite
                )
                updateRecyclerView()
                binding.addService.visibility =
                    if (tab?.text == "Services") View.VISIBLE else View.GONE
                updateOverallRiskValue()

                when (tab?.text) {
                    "Data" -> RemoteConfigHelper.nextScreenFragment("2.2.0", "fragment_service")
                    "Services" -> RemoteConfigHelper.nextScreenFragment("2.0.0", "fragment_service")
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

        })
        return view
    }

    @SuppressLint("StaticFieldLeak")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == REQUEST_LOGIN || requestCode == REQUEST_EDIT_LOGIN) && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val service =
                    data.getSerializableIntent(LoginActivity.PARAM_SERVICE, Service::class.java)
                val user = data.getStringExtra(LoginActivity.PARAM_USER)
                val password = data.getStringExtra(LoginActivity.PARAM_PASSWORD)
                service.user = user!!
                if (service.isPasswordSaved) {
                    service.password = password!!
                }
                object : AsyncTask<Void?, Void?, Service?>() {
                    override fun doInBackground(vararg voids: Void?): Service {
                        if (requestCode == REQUEST_LOGIN) {
                            if (service.name != "Booking(R)") cardViewModel!!.insertService(service)
                        } else {
                            cardViewModel!!.updateService(service)
                        }
                        return cardViewModel!!.getServiceWithName(serviceName = service.name)
                    }

                    override fun onPostExecute(serviceP: Service?) {
                        super.onPostExecute(serviceP)
                        val ex = Executors.newSingleThreadExecutor()
                        ex.execute {
                            processDataExtraction(
                                ServiceHelper(context),
                                serviceP!!,
                                user,
                                password,
                                context!!.applicationContext
                            )
                        }
                    }
                }.execute()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEditCredentials(event: UpdateCredentialsEvent) {
        for (service in subscribedServices!!) if (service.name == event.service) editCredentials(
            service, REQUEST_EDIT_LOGIN, true
        )
    }

    @Subscribe
    fun onDetailedCard(event: DetailedCardEvent) {
        val intent = Intent(context, DetailedCardActivity::class.java)
        intent.putExtra(DetailedCardActivity.PARAM_CARD, event.card)
        startActivity(intent)
    }

    @Subscribe
    fun onRadarClicked(event: RadarClickedEvent) {
        val intent = Intent(context, RiskValueActivity::class.java)
        intent.putExtra(PARAM_IS_DATA_CENTRIC, false)
        intent.putExtra(PARAM_SUB_TYPE_PARENT, event.card.title)
        startActivity(intent)
    }

    @Subscribe
    fun onRefreshClickedFromAdapter(event: ClickedRefreshEvent) {
        showRefreshDialog {
            val cardTitle = event.cardName
            val serviceItem = ServiceController.getServiceItem(cardTitle)
            if (ServiceController.isAPIService(serviceItem)) {
                val intent = Intent(context, APIServiceActivity::class.java)
                intent.putExtra(ServiceController.PARAM_SERVICE, serviceItem as APIServiceEnum)
                startActivity(intent)
            } else if (ServiceController.isSARSService(serviceItem)) {
                val intent = Intent(context, SARServiceActivity::class.java)
                intent.putExtra(ServiceController.PARAM_SERVICE, serviceItem as SARServiceEnum)
                startActivity(intent)
            }
        }
    }

    @Subscribe
    fun onCardAddedToWatchList(event: ChangeWatchListStatusEvent) {
        watchListHelper!!.changeWatchListStatus(event.cardName)
        updateRecyclerView()
        updateOverallRiskValue()
    }

    @Subscribe
    fun onExtractionDateClicked(event: ClickedDateExtractionDateEvent) {
        showDialog(event.card)
    }

    private fun showDialog(card: Card) {
        val fragmentManager = requireFragmentManager()
        val newFragment = ExtractionDateAlertDialog.newInstance(card)
        newFragment.onDateChosen = { date ->
            val service = subscribedServices?.find { it.name == card.title }
            service?.dateSelected = date
            updateRecyclerView()
            updateOverallRiskValue()
        }
        newFragment.show(fragmentManager, "dialog")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private fun enableSwipeToDeleteAndUndo() {
        val swipeToDeleteCallback: SwipeToDeleteCallback =
            object : SwipeToDeleteCallback(context!!) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
                    val position = viewHolder.adapterPosition
                    val item: Card = cardAdapter?.getData()?.get(position)!!
                    val tmpService: Service? = subscribedServices?.last { it.name == item.title }

                    AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.unsubscribe)
                        .setMessage(R.string.unsubscribe_confirmation)
                        .setPositiveButton(R.string.yes) { _, _ ->
                            run {
                                cardViewModel?.deleteService(
                                    tmpService
                                )
                                PriVELTLog.BUTTON_PRESSED("User deletes ${tmpService?.name}")
                                Toast.makeText(
                                    context,
                                    "The service has been removed from your list",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }.setNegativeButton(R.string.no) { _: DialogInterface, _: Int ->
                            run {
                                cardAdapter?.removeItem(position)
                                cardAdapter?.restoreItem(item, position)
                            }
                        }.setOnCancelListener {
                            run {
                                cardAdapter?.removeItem(position)
                                cardAdapter?.restoreItem(item, position)
                            }
                        }.show()
                }
            }
        val itemTouchhelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchhelper.attachToRecyclerView(binding.servicesList)
    }

    companion object {
        private const val REQUEST_LOGIN = 765
        private const val REQUEST_EDIT_LOGIN = 7654
    }

    override fun onAddServiceClick(service: String) {
        newFragment?.dismiss()
        if (service.contains("(R)")) editCredentials(
            Service(service, false, "", "", ""), REQUEST_LOGIN, false
        )
        val serviceItem = ServiceController.getServiceItem(service)
        if (ServiceController.isAPIService(serviceItem)) {
            val intent = Intent(context, APIServiceActivity::class.java)
            intent.putExtra(ServiceController.PARAM_SERVICE, serviceItem as APIServiceEnum)
            startActivity(intent)
        } else if (ServiceController.isSARSService(serviceItem)) {
            val intent = Intent(context, SARServiceActivity::class.java)
            intent.putExtra(ServiceController.PARAM_SERVICE, serviceItem as SARServiceEnum)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.date -> {
                sorting = SORTING.DATE
                updateRecyclerView()
                return true
            }
            R.id.risk -> {
                sorting = SORTING.RISK
                updateRecyclerView()
                return true
            }
            R.id.name -> {
                sorting = SORTING.NAME
                updateRecyclerView()
                return true
            }
            R.id.watchlist -> {
                isFavorite = !isFavorite
                if (binding.addService.visibility == View.GONE) {
                    filters = booleanArrayOf(
                        true,
                        false,
                        isFavorite
                    )
                    updateRecyclerView()
                    updateOverallRiskValue()
                    if (isFavorite)
                        RemoteConfigHelper.nextScreenFragment("2.3.0", "fragment_service")
                    else
                        RemoteConfigHelper.nextScreenFragment("2.2.0", "fragment_service")
                } else {
                    filters = booleanArrayOf(
                        false,
                        true,
                        isFavorite
                    )
                    updateRecyclerView()
                    updateOverallRiskValue()
                    if (isFavorite)
                        RemoteConfigHelper.nextScreenFragment("2.3.0", "fragment_service")
                    else
                        RemoteConfigHelper.nextScreenFragment("2.0.0", "fragment_service")
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    enum class SORTING {
        NAME, RISK, DATE
    }

    private fun showRefreshDialog(onItemSelected: () -> Unit) {

        val builder = androidx.appcompat.app.AlertDialog.Builder(context!!)
        builder.setTitle("Do you want to re-fetch a new snapshot of this service?")
        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }
        builder.setPositiveButton("Confirm") { dialog, _ ->
            dialog.dismiss()
            onItemSelected()
        }
        val dialog = builder.create()
        dialog.show()
    }
}
