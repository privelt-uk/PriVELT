package com.kent.university.privelt.service.instagram

import android.content.Context
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InstagramService : Service("Instagram", false, "", "", "") {

    lateinit var APIServiceListener: APIServiceListener

    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(context: Context, token: String) = CoroutineScope(Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }

        systemTime = System.currentTimeMillis()
        val id = insertOrGetService(context)
        fetchProfile(token, id, context)
    }

    fun connection(webView: WebView) {
        webView.isVisible = true
        val redirectUri = "https://privelt.ac.uk/"
        val loadUrl = "https://api.instagram.com/oauth/authorize?client_id=367040501960376&response_type=code&redirect_uri=$redirectUri&scope=user_profile,user_media"
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(loadUrl)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectUri)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.getQueryParameter("code")?.let { code ->
                            // Got it!
                            Log.d("OAuth", "Here is the authorization code! $code")
                            exchangeToken(code, webView.context)
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            APIServiceListener.error("Authorization code not received :(")

                            val exception = ApiResponseException(
                                "Failed to fetch media",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                        }
                    }
                }
                return super.shouldOverrideUrlLoading(view, request)
            }
        }
    }

    private fun exchangeToken(code: String, context: Context){
        val retIn = RetrofitInstance.getApiRetrofitInstance()
        val clientSecret = "edb1b8a99c91b0c7b983d4871d8037e8"
        val clientId = "367040501960376"
        val grantType = "authorization_code"
        val redirect = "https://privelt.ac.uk/"
        retIn.exchangeCodeForToken(clientSecret, clientId, grantType, redirect, code)
            .enqueue(object: Callback<AuthTokenExchangeResponse> {
                override fun onFailure(call: Call<AuthTokenExchangeResponse>, t: Throwable) {
                    Log.e("instagram", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<AuthTokenExchangeResponse>, response: Response<AuthTokenExchangeResponse>) {
                    if (response.code() == 200) {
                        Log.d("instagram","Generated token!")
                        response.body()?.access_token?.let {
                            insertServiceAndFetchData(context, it)
                        }
                    }
                    else{
                        Log.d("instagram","Token exchange failed!")
                        val exception = ApiResponseException(
                            "Token exchange failed",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Token exchange failed!")
                    }
                }
            })
    }

    private fun fetchProfile(token: String, id: Long, context: Context) {
        val retIn = RetrofitInstance.getGraphRetrofitInstance()
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        retIn.profile("id,username,media_count,account_type", token).enqueue(object:
            Callback<Profile> {
            override fun onFailure(call: Call<Profile>, t: Throwable) {
                Log.d("instagram",t.message.toString())
                Firebase.crashlytics.recordException(t)
                APIServiceListener.error(t.message.toString())
            }
            override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                if (response.code() == 200) {
                    Log.d("instagram","Fetched profile!")
                    val allData = mutableListOf<UserData>()
                    if (response.body() != null) {
                        val initialUserData = UserData(title = "Instagram Profile",
                        type = DataTypeList.PROFILE.title,
                        subtype = "Instagram Profile",
                        value = "",
                        serviceId = id,
                        date = systemTime)
                        allData.add(initialUserData)
                        val test = response.body()!!.toUserData(id, systemTime, initialUserData.id)
                        allData.addAll(test)
                    }
                    //save username
                    CoroutineScope(Dispatchers.IO).launch {
                        val serviceDataRepository =
                            PriVELTDatabase.getInstance(context)?.serviceDao()
                        val service =
                            serviceDataRepository?.getServiceWithName(this@InstagramService.name)
                        service?.user = response.body()?.username ?: ""
                        serviceDataRepository?.updateServices(service)

                        allData.forEach {
                            if (it.isValid())
                                userDataRepository?.insertUserData(it)
                        }
                    }
                    Log.d(allData.toString(), allData.toString())
                    fetchMedia(token, id, context)
                }
                else {
                    Log.d("instagram","Failed to fetch profile!")
                    val exception = ApiResponseException(
                        "Failed to fetch profile",
                        response.message(),
                        response.code())
                    Firebase.crashlytics.recordException(exception)
                    APIServiceListener.error("Failed to fetch profile")
                }
            }
        })
    }

    private fun fetchMedia(token: String, id: Long, context: Context) {
        val retIn = RetrofitInstance.getGraphRetrofitInstance()
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        retIn.media("id,caption,media_type,media_url,permalink,thumbnail_url,timestamp,username", token).enqueue(object:
            Callback<Media> {
            override fun onFailure(call: Call<Media>, t: Throwable) {
                Log.d("instagram",t.message.toString())
                Firebase.crashlytics.recordException(t)
                APIServiceListener.error(t.message.toString())
            }
            override fun onResponse(call: Call<Media>, response: Response<Media>) {
                if (response.code() == 200) {
                    Log.d("instagram","Fetched Media!")
                    val allData = mutableListOf<UserData>()
                    if (response.body() != null) {
                        val initialUserData = UserData(title = "Instagram Media",
                            type = DataTypeList.MEDIA.title,
                            subtype = "Instagram Media",
                            value = "",
                            serviceId = id,
                            date = systemTime)
                        allData.add(initialUserData)
                        val test = response.body()!!.toUserData(id, systemTime, initialUserData.id)
                        allData.addAll(test)
                    }

                    CoroutineScope(Dispatchers.IO).launch {
                        allData.forEach {
                            if (it.isValid())
                                userDataRepository?.insertUserData(it)
                        }
                    }
                    Log.d(allData.toString(), allData.toString())
                    APIServiceListener.success()
                }
                else {
                    Log.d("instagram","Failed to fetch media!")
                    val exception = ApiResponseException(
                        "Failed to fetch media",
                        response.message(),
                        response.code())
                    Firebase.crashlytics.recordException(exception)
                    APIServiceListener.error("Failed to fetch media")
                }
            }
        })
    }
}