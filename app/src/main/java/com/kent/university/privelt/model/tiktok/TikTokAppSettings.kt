package com.kent.university.privelt.model.tiktok

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TikTokAppSettings(
    @SerializedName("Settings")
    val settings: TikTokSettings?
) {
    fun toUserData(
        serviceId: Long,
        date: Long,
        parentId: String
    ): List<UserData> {
        val settingsData = settings?.toUserData(serviceId, date, parentId) ?: emptyList()

        return settingsData
    }
}

data class TikTokSettings(
    @SerializedName("SettingsMap")
    val settingsMap: TikTokSettingsMap?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        return settingsMap?.toUserData(serviceId, date, parentId) ?: emptyList()
    }
}

data class TikTokSettingsMap(
    @SerializedName("Ads Based on Data Received from Partners")
    val adsBasedOnDataReceivedFromPartners: String?,
    @SerializedName("Allow Others to Find Me")
    val allowOthersToFindMe: String?,
    @SerializedName("App Language")
    val appLanguage: String?,
    @SerializedName("Interests")
    val interests: String?,
    @SerializedName("Personalized Ads")
    val personalizedAds: String?,
    @SerializedName("Private Account")
    val privateAccount: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "TikTok settings: $title",
                type = DataTypeList.PROFILE.title,
                subtype = DataSubTypeListProfile.PREFERENCES.value,
                value = title + " " + field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }

        return userDataList
    }
}
