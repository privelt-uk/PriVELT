package com.kent.university.privelt.model.facebook

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class FacebookHometown (
    @SerializedName("id") val id : Long,
    @SerializedName("name") val name : String
){
    fun toUserData(serviceId: Long): List<UserData> {
        val unixTime = System.currentTimeMillis() / 1000
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Facebook Hometown $title",
                type = DataTypeList.PROFILE.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = unixTime)
        }
        return userDataList
    }
}