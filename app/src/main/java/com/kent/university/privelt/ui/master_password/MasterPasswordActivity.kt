/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.master_password

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityMainPasswordBinding
import com.kent.university.privelt.ui.dashboard.DashboardActivity
import com.kent.university.privelt.ui.settings.SettingsFragment
import com.kent.university.privelt.ui.settings.SettingsFragment.Companion.PARAM_DISABLE_FINGERPRINT
import com.kent.university.privelt.utils.EyePassword
import com.kent.university.privelt.utils.PasswordChecker
import com.kent.university.privelt.utils.biometric.BiometricPromptTinkManager
import com.kent.university.privelt.utils.firebase.RemoteConfigHelper
import com.nulabinc.zxcvbn.Zxcvbn

class MasterPasswordActivity : BaseActivity(), View.OnClickListener, TextWatcher {

    private lateinit var binding: ActivityMainPasswordBinding

    private var zxcvbn: Zxcvbn? = null
    private var changePassword = false
    private lateinit var biometricPromptManager: BiometricPromptTinkManager

    private var masterPasswordAlreadyGiven = false

    override val activityLayout: Int
        get() = R.layout.activity_main_password

    override fun configureViewModel() {

    }

    private fun configureLoginScreen() {
        RemoteConfigHelper.nextScreenFragment("1.0.2", "activity_master_password")
        binding.passwordMeter.visibility = View.GONE
        binding.hint.visibility = View.GONE
        binding.choosePassword.text = getString(R.string.login_label)
        binding.confirmPassword.visibility = View.GONE
        binding.eyeConfirmPassword.visibility = View.GONE
        binding.progressCircular.visibility = View.GONE
        binding.start.isEnabled = true
        if (biometricPromptManager.checkIfPreviousEncryptedData() && biometricPromptManager.isFingerPrintAvailable())
            binding.fingerprint.visibility = View.VISIBLE
        else
            binding.fingerprint.visibility = View.GONE
    }

    private fun configureNewPasswordScreen() {
        RemoteConfigHelper.nextScreenFragment("1.0.1", "activity_master_password")
        binding.progressCircular.visibility = View.GONE
        binding.choosePassword.text = getString(R.string.title_master_password)
        binding.start.isEnabled = true
        binding.passwordMeter.visibility = View.VISIBLE
        binding.hint.visibility = View.VISIBLE
        binding.confirmPassword.visibility = View.VISIBLE
        binding.eyeConfirmPassword.visibility = View.VISIBLE
        binding.fingerprint.visibility = View.GONE
    }

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityMainPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        biometricPromptManager = BiometricPromptTinkManager(this)

        binding.start.setOnClickListener(this)
        binding.password.addTextChangedListener(this)
        zxcvbn = Zxcvbn()
        if (intent != null) {
            changePassword = intent.getBooleanExtra(SettingsFragment.ARG_CHANGE_PASSWORD, false)
        } else if (savedInstanceState != null) {
            changePassword =
                savedInstanceState.getBoolean(SettingsFragment.ARG_CHANGE_PASSWORD, false)
        }
        title = ""
        if (changePassword) {
            binding.start.text = getString(R.string.change_master_password)
        } else supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        masterPasswordAlreadyGiven = identityManager?.isPasswordAlreadySet() == true
        if (!masterPasswordAlreadyGiven || changePassword) {
            configureNewPasswordScreen()
        } else {
            configureLoginScreen()
        }
        EyePassword.configureEye(binding.eyePassword, binding.password)
        EyePassword.configureEye(binding.eyeConfirmPassword, binding.confirmPassword)
        binding.password.transformationMethod = PasswordTransformationMethod()
        binding.confirmPassword.transformationMethod = PasswordTransformationMethod()

        binding.fingerprint.setOnClickListener {
            decryptPrompt()
        }

        if (!changePassword && biometricPromptManager.checkIfPreviousEncryptedData() && biometricPromptManager.isFingerPrintAvailable() && !intent.getBooleanExtra(PARAM_DISABLE_FINGERPRINT, false))
            decryptPrompt()
    }

    private fun decryptPrompt() {
        biometricPromptManager.decryptPrompt(
            failedAction = { },
            successAction = {
                val password = String(it)
                launchDashboard(password)
            }
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(SettingsFragment.ARG_CHANGE_PASSWORD, changePassword)
    }

    @SuppressLint("StaticFieldLeak")
    override fun onClick(view: View) {
        if (binding.confirmPassword.visibility == View.VISIBLE && binding.password.text.toString() != binding.confirmPassword.text.toString()) {
            Toast.makeText(
                this@MasterPasswordActivity,
                R.string.different_password,
                Toast.LENGTH_LONG
            ).show()
            return
        } else if (masterPasswordAlreadyGiven && binding.password.text.toString().isEmpty()) {
            Toast.makeText(
                this@MasterPasswordActivity,
                getString(R.string.empty_pass),
                Toast.LENGTH_LONG
            ).show()
            return
        } else if (!PasswordChecker.checkPassword(zxcvbn!!, binding.password.text.toString())) {
            val errMessage =
                if (changePassword || !masterPasswordAlreadyGiven) R.string.not_respect_policy else R.string.wrong_password
            Toast.makeText(this@MasterPasswordActivity, errMessage, Toast.LENGTH_LONG).show()
            return
        }
        binding.start.isEnabled = false
        binding.progressCircular.visibility = View.VISIBLE
        val password = binding.password.text.toString()
        object : AsyncTask<Void?, Void?, Pair<Boolean, String>>() {
            override fun doInBackground(vararg v: Void?): Pair<Boolean, String> {
                if (changePassword || !masterPasswordAlreadyGiven) {
                    masterPasswordAlreadyGiven = true
                    identityManager?.setPassword(password)
                    return Pair(true, password)
                } else {
                    val check = identityManager?.checkPassword(password) == true
                    return Pair(check, password)
                }
            }

            override fun onPostExecute(res: Pair<Boolean, String>) {
                super.onPostExecute(res)
                binding.start.isEnabled = true
                binding.progressCircular.visibility = View.GONE
                if (res.first) {
                    if (!biometricPromptManager.checkIfPreviousEncryptedData() && biometricPromptManager.isFingerPrintAvailable())
                        AlertDialog.Builder(this@MasterPasswordActivity, R.style.AlertTheme)
                            .setTitle("Fingerprint Login")
                            .setMessage("Do you want to enable fingerprint login?")
                            .setPositiveButton(
                                "Yes"
                            ) { _, _ ->
                                biometricPromptManager.encryptPrompt(
                                    data = res.second.toByteArray(),
                                    failedAction = { },
                                    successAction = {
                                        launchDashboard(password)
                                    }
                                )
                            }
                            .setNegativeButton("No") { _, _ ->
                                launchDashboard(password)
                            }.show()
                    else
                        launchDashboard(password)
                } else {
                    val errMessage =
                        if (changePassword) R.string.same_password else R.string.wrong_password
                    Toast.makeText(this@MasterPasswordActivity, errMessage, Toast.LENGTH_LONG)
                        .show()
                }
            }
        }.execute()
    }

    fun launchDashboard(password: String) {
        if (!changePassword) {
            startActivity(Intent(this@MasterPasswordActivity, DashboardActivity::class.java))
        } else {
            //this@MasterPasswordActivity.identityManager?.changePassword(password)
        }
        finish()
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

    override fun afterTextChanged(editable: Editable) {
        updatePasswordStrengthView(editable.toString())
    }

    private fun updatePasswordStrengthView(password: String) {
        if (TextView.VISIBLE != binding.passwordStrength.visibility) return
        if (password.isEmpty()) {
            binding.passwordStrength.text = ""
            binding.progressPassword.progress = 0
            return
        }
        val strength = zxcvbn!!.measure(password)
        val ps = PasswordStrength.values()[strength.score]
        binding.progressPassword.progress = ps.progress
        binding.passwordStrength.setText(ps.resId)
        binding.passwordStrength.setTextColor(ps.color)
        binding.progressPassword.progressDrawable.colorFilter =
            BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                ps.color,
                BlendModeCompat.SRC_IN
            )
    }

}
