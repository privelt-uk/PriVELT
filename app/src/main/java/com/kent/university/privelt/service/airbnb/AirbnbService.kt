package com.kent.university.privelt.service.airbnb

import android.content.Context
import com.kent.university.privelt.model.Airbnb.AirbnbProfileData
import com.kent.university.privelt.model.Airbnb.AirbnbReservationsData
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.InputStream
import java.util.zip.ZipInputStream

class AirbnbService : SARService() {

    override var sarRequestURL: String = "https://www.airbnb.co.uk/account-settings/privacy-and-sharing/data"
    override val service: Service = Service("Airbnb", false, "", "", "")

    private fun extractProfile(context: Context, inputStream: InputStream, workbook: Workbook, date: Long) =
        CoroutineScope(Dispatchers.IO).launch {
            val profileSheet = workbook.getSheet("Profile Information")
            if (profileSheet != null) {
                val profileRowIterator = profileSheet.rowIterator()
                val profile = AirbnbProfileData(
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                )
                while (profileRowIterator.hasNext()) {
                    val next = profileRowIterator.next()
                    when (next.getCell(0).stringCellValue) {
                        "user.country" -> {
                            profile.userCountry = next.getCell(1).stringCellValue
                        }
                        "user.lastName" -> {
                            profile.userLastName = next.getCell(1).stringCellValue
                        }
                        "user.birthdate" -> {
                            profile.userBirthday = next.getCell(1).stringCellValue
                        }
                        "user.initialIp" -> {
                            profile.userInitialIp = next.getCell(1).stringCellValue
                        }
                        "user.languages" -> {
                            profile.userLanguages = next.getCell(1).stringCellValue
                        }
                        "user.sex" -> {
                            profile.userSex = next.getCell(1).stringCellValue
                        }
                        "user.tosVersion" -> {
                            profile.userTosVersion = next.getCell(1).stringCellValue
                        }
                        "user.tosAcceptedAt" -> {
                            profile.userTosAcceptedAt = next.getCell(1).stringCellValue
                        }
                        "user.createdAt" -> {
                            profile.userCreatedAt = next.getCell(1).stringCellValue
                        }
                        "user.firstName" -> {
                            profile.userFirstName = next.getCell(1).stringCellValue
                        }
                        "user.nativeCurrency" -> {
                            profile.userNativeCurrency = next.getCell(1).stringCellValue
                        }
                        "user.email" -> {
                            profile.userEmail = next.getCell(1).stringCellValue
                        }
                        "user.userProfileInfo.employer" -> {
                            profile.userProfileInfoEmployer = next.getCell(1).stringCellValue
                        }
                        "user.userProfileInfo.university" -> {
                            profile.userProfileInfoUniversity = next.getCell(1).stringCellValue
                        }
                        "user.preferredLocale" -> {
                            profile.userPreferredLocale = next.getCell(1).stringCellValue
                        }
                        "user.updatedAt" -> {
                            profile.userUpdatedAt = next.getCell(1).stringCellValue
                        }
                        "phoneNumbers[0].number" -> {
                            profile.phoneNumber = next.getCell(1).stringCellValue
                        }
                        "user.user.userProfileInfo.hometown" -> {
                            profile.serProfileInfoHometown = next.getCell(1).stringCellValue
                        }
                    }
                }
                val allData = mutableListOf<UserData>()
                val userData = profile.toUserData(service.id, date)
                allData.addAll(userData)
                saveUserDataToRepository(context, allData, true)
            } else
                _stateFlow.emit(State.Error("Profile information not found"))
        }
    private fun extractReservations(context: Context, workbook: Workbook, date: Long) =
        CoroutineScope(Dispatchers.IO).launch {
            val reservationsSheet = workbook.getSheet("Reservations")
            if (reservationsSheet != null) {
                val reservationsRowIterator = reservationsSheet.rowIterator()
                var counter = 0
                val reservation = AirbnbReservationsData(
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                )
                val allData = mutableListOf<UserData>()
                while (reservationsRowIterator.hasNext() && counter <= 50) {
                    val next = reservationsRowIterator.next()
                    val column = next.getCell(0).stringCellValue
                    if (!column.contains(counter.toString())) {
                        val reservationsUserData = UserData(title = "Airbnb Reservations",
                            type = DataTypeList.ACTIVITY.title,
                            subtype = DataSubTypeListActivity.BOOKING.value,
                            value = "",
                            serviceId = service.id,
                            date = date)
                        allData.add(reservationsUserData)
                        allData.addAll(reservation.toUserData(service.id, date, reservationsUserData.id))
                        counter++
                    }
                    val value = next.getCell(0).stringCellValue
                    if (column.contains("guestCurrency")) {
                        reservation.guestCurrency = value
                    } else if (value.contains("numberOfGuests")) {
                        reservation.numberOfGuests = value
                    } else if (value.contains("numberOfInfants")) {
                        reservation.numberOrInfants = value
                    } else if (value.contains("hostingUrl")) {
                        reservation.hostingUrl = value
                    } else if (value.contains("guestVatCountry")) {
                        reservation.guestVatCountry = value
                    } else if (value.contains("numberOfChildren")) {
                        reservation.numberOfChildren = value
                    } else if (value.contains("numberOfAdults")) {
                        reservation.numberOfAdults = value
                    }
                }
                val reservationsUserData = UserData(title = "Airbnb Reservations",
                    type = DataTypeList.ACTIVITY.title,
                    subtype = DataSubTypeListActivity.BOOKING.value,
                    value = "",
                    serviceId = service.id,
                    date = date)
                allData.add(reservationsUserData)
                allData.addAll(reservation.toUserData(service.id, date, reservationsUserData.id))
                saveUserDataToRepository(context, allData, true)
            }
        }

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val workbook = ZipInputStream(inputStream).use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.equals("user_data.xlsx") }
                    .map { WorkbookFactory.create(zipStream) }
                    .reduceOrNull() { first, _ -> first }
            }
            val unixTime = System.currentTimeMillis()
            if (workbook != null) {
                extractProfile(context, inputStream, workbook, unixTime)
                extractReservations(context, workbook, unixTime)
            } else
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
        }

}
