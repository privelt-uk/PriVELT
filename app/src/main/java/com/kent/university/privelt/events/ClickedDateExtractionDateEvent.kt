package com.kent.university.privelt.events

import com.kent.university.privelt.model.Card

data class ClickedDateExtractionDateEvent(var card: Card)
