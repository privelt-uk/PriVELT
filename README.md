# PriVELT

Android application to centralise known data from different services. The project is funded by PriVELT (https://privelt.ac.uk/).
PriVELT aims to make travellers aware of the risks of the Internet. The application collect all private data known by the different services (see Services supported), to inform users and help them to reduce  privacy risks. PriVELT also monitor local installed applications by checking all the permissions they are asking for, and which permissions are granted by the users. 
PriVELT does not have a remote server, or any other other centralised record of user data. All the information is held locally on devices / personal storages owned by the users.
The local database is secured with a password  known and chosen only by the user. PriVELT also provides scores and charts to help users to understand the privacy issues they are facing.

## Screenshots

<img src="https://raw.githubusercontent.com/LeBenki/PriVELT/dev/screenshots/1.jpg" width="200" hspace="20" title="Master password"/> <img src="https://raw.githubusercontent.com/LeBenki/PriVELT/dev/screenshots/2.jpg" width="200" hspace="20" title="Service and data cards"/> <img src="https://raw.githubusercontent.com/LeBenki/PriVELT/dev/screenshots/3.jpg" width="200" hspace="20" title="Permissions cards"/>

<img src="https://raw.githubusercontent.com/LeBenki/PriVELT/dev/screenshots/4.jpg" width="200" hspace="20" title="Save database in another service"/> <img src="https://raw.githubusercontent.com/LeBenki/PriVELT/dev/screenshots/5.jpg" width="200" hspace="20" title="Granted permissions"/><img src="https://raw.githubusercontent.com/LeBenki/PriVELT/dev/screenshots/6.jpg" width="200" hspace="20" title="Details of permission type"/>

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@github.com:LeBenki/PriVELT.git
```

## Services supported

Location history:
  - Google.com (Maps)

Travelling Websites:
  - Hotels.com
  - Booking.com
  - Expedia.com
  - Agoda

Online Social Networks:
  - Twitter
  - Facebook
  - Instagram
  - Pinterest

## Maintainers

The project is maintained by:
* [Lucas Benkemoun](http://github.com/LeBenki)

## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -m 'Add some feature')
4. Push your branch (git push origin my-new-feature)
5. Create a new Pull Request

## License

MPL-2.0

# Attribution

Some icons in this app has been designed using images from Flaticon.com
